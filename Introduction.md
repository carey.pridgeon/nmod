![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)

# The nMod Toolkit User Manual #


---


### [History](http://code.google.com/p/nmod/wiki/history) ###

A little about the origins of nMod.

### [Announcements](http://code.google.com/p/nmod/wiki/announce) ###

News about releases, intermediate changes to source code, planned updates, and changes to the website.


---


### Section One: n-Body Modelling Guide ###
This is introductory material, intended for those who are new to the subject of n-Body modelling

### [Lesson One](http://code.google.com/p/nmod/wiki/basics1) ###
**_What is an n-Body model?_**

### [Lesson Two](http://code.google.com/p/nmod/wiki/basics2) ###
**_Gravitation_**

### [Lesson Three](http://code.google.com/p/nmod/wiki/basics3) ###
**_The Particle-Particle Method_**


---

### Section Two: The Design and Implementation of nMod's n-Body Model ###
This section describes nMod's n-Body model, and the file formats it utilises.
### [The nMod n-Body Model Technical details: Part 1](http://code.google.com/p/nmod/wiki/internals1) ###

**_Calculating Motion: How the nMod n-Body model works_**

### [The nMod n-Body Model Technical details: Part 2](http://code.google.com/p/nmod/wiki/internals2) ###

**_Preparation and Loading of Projects into nMod: the nmp file_**

### [The nMod n-Body Model Technical details: Part 3](http://code.google.com/p/nmod/wiki/internals3) ###
**_Recording of n-Body time series data: the nbo file_**


### [The nMod n-Body Model Technical details: Part 4](http://code.google.com/p/nmod/wiki/internals4) ###

**_Additional Properties of the nMod n-Body Model_**

---

### Section Three: Creating new nmp project files ###

This section explains how to make new project files for the nMod toolkit

### [Creating new NMP files](http://code.google.com/p/nmod/wiki/nmp1) ###
**_Using the JPL Horizons database to build models of the real Solar System_**


---

### Section Four: Integration Functions ###

This section describes the two integration functions available in the nMod n-Body model

### [The Integration Methods in nMod: Part 1](http://code.google.com/p/nmod/wiki/int1) ###

_**Introduction**_

### [The Integration Methods in nMod: Part 2](http://code.google.com/p/nmod/wiki/int2) ###

**_The Midpoint Integration Method_**

### [The Integration Methods in nMod: Part 3](http://code.google.com/p/nmod/wiki/int3) ###

**_The Runge-Kutta Fourth Order Integration Method_**


---

### Section Five: Compiling the nMod Toolkit ###

This section covers compilation of the nMod toolkit under Linux and Windows.

### [Compiling the nMod Toolkit: Part 1](http://code.google.com/p/nmod/wiki/compile1) ###

**_Compilation under Linux_**

### [Compiling the nMod Toolkit: Part 2](http://code.google.com/p/nmod/wiki/compile2) ###

**_Compilation under Windows_**

### [Compiling the nMod Toolkit: Part 3](http://code.google.com/p/nmod/wiki/compile3) ###

**_Setting up Mingw in Windows_**

### [Compiling the nMod Toolkit: Part 4](http://code.google.com/p/nmod/wiki/compile4) ###

**_Compilation of the nMod toolkit using Visual Studio_**



---

### Section Six: Using the nMod Toolkit ###

This section covers using the reference build of the nMod toolkit in Linux and Windows.

### [Using the nMod Toolkit: Part 1](http://code.google.com/p/nmod/wiki/usage1) ###

**_Using the nMod Toolkit n-Body model under Linux_**

### [Using the nMod Toolkit: Part 2](http://code.google.com/p/nmod/wiki/usage2) ###

**_Using the nMod Toolkit n-Body model under Windows_**

### [Using the nMod Toolkit: Part 3](http://code.google.com/p/nmod/wiki/usage3) ###

**_Using the nMod Toolkit nbo viewer nBview under Linux_**


### [Using the nMod Toolkit: Part 4](http://code.google.com/p/nmod/wiki/usage4) ###

**_Using the nMod Toolkit nbo viewer nBview under Windows_**

### [Using the nMod Toolkit: Part 5](http://code.google.com/p/nmod/wiki/usage5) ###

**_nbo2txt - making nbo files human readable_**

### [Using the nMod Toolkit: Part 6](http://code.google.com/p/nmod/wiki/usage6) ###

**_nbojoin - combining existing nbo files_**

### [Using the nMod Toolkit: Part 7](http://code.google.com/p/nmod/wiki/usage7) ###

**_nbofix, repairing broken nbo files_**



---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com