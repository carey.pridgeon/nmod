![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)

# The nMod n-Body Modelling Toolkit #


---


| ![http://nmod.googlecode.com/svn/trunk/images/frontpage.png](http://nmod.googlecode.com/svn/trunk/images/frontpage.png) |
|:------------------------------------------------------------------------------------------------------------------------|


---

**_ANNOUNCEMENT_**

07/2010

Development of nMod has ceased, as it has been superseded by my latest project, a C++ concurrent n-body model development framework called Moody. Moody is also cross platform, is released under an open source licence and has Celestial and Orbital Mechanics capabilities which exceed those available in nMod.

Moodys homepage can be found on github

https://github.com/rucs-hack/moody/wiki



I'm leaving nMod online mainly for sentimental reasons, I put a lot of effort into it, and its always possible someone might still find it useful.

[Proceed to the nMod tutorials and usage guide](http://code.google.com/p/nmod/wiki/Introduction)

---

Dr Carey Pridgeon 2008

carey.pridgeon@gmail.com