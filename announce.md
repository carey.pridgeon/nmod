![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)

# Announcements #


---

Active development of nMod has ceased. For my latest n-body modelling software project, Moody, a parallel n-body model development framework, go [here](http://moody.politespider.com/index.php)

---


Previous announcements removed.


---


**Links**

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)

---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com