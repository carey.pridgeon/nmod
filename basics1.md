![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)

# n-Body Modelling: Lesson One #

**_What is an n-Body model?_**


---


## What does an n-Body model actually do? ##

The fundamental task of n-Body modelling is defined as follows:

**Given a set of gravitationally interacting particles at time _t_ find out at what position those particles will be at time _t+1_**

So simple to say, and you probably read that elsewhere, but what does it actually mean?

Well, the time between **_t_** and **_t+1_**is referred to as a step. This is a period of time during which the particles are moved. The force acting on them which defines that movement is gravity, and you use the n-Body model itself to translate the gravitational influences into motion.

## Gravity, and its application in n-Body models ##

Sooner or later I'm going to have to mention Newton, so we might as well get right on that.

Isaac Newtons Law of Gravitational Motion states that:

**_Every point mass attracts every other point mass by a force pointing along the line intersecting both points. The force is proportional to the product of the two masses and inversely proportional to the square of the distance between the point masses._**

That's quite a statement, and if you're new to this, it might make no sense whatsoever, so lets break it down.

A Point Mass is, simply speaking, the central point of any object with mass. This might be a speck of dust, or a Star. Scale is irrelevant.

Now I'm sure you learned at school that a point has no size. This is true, so why am I saying that a Star can be a point mass? You may have observed that they tend towards being on the barking huge size.

The answer is simple (luckily). Newtons equations don't care one whit about the radius of any object to which it is applied, only the mass. All that matters is the central point of the object in question. Any mass that a Star has is considered as being at this single point, and the same is true of any object, even our Solar System or the Galaxy itself, when considered from a distance. What the equation cares about is that there be a single point which is central to a body of mass being used in the equation.

So, we have two Point Masses, which we'll think of as a couple of stars for now. We work out what the gravitational force is between these two stars by multiplying the total mass of the stars together, and dividing the result by the straight line distance between the absolute centres of both stars.

So you know how it works, its time to look at the equation itself.

![http://nmod.googlecode.com/svn/trunk/images/Newtons_eq.png](http://nmod.googlecode.com/svn/trunk/images/Newtons_eq.png)

Ok then, we can just plug this Equation into a model, set it going and we're away, yes?

No.

Unfortunately for us, while Newtons equation describes the real world rather well, it does not work right off the bat in an n-Body model, or at least, not a good one.  First we have to do a little light hacking to get everything working just right.

So what's the problem?  Co-ordinate systems, that's what.

## The effect of Co-ordinate systems on n-Body Modelling ##

We are required to use a three dimensional Cartesian coordinate system (quite a mouthful, lets use the term World Co-ordinate System, and abbreviate it to WCS). This is because we aren't stuffing a perfect model of the universe into a computer, we are building a mathematical approximation, or simulation, of the real universe.

As a result of this we have this rather annoying problem of particle positions being described in terms of positive and negative real valued numbers, relative to some reference point `[0,0,0]`, also known as the _origin_. I was going to draw a pretty three dimensional graph thing here, but I'm not that good at drawing, so lets move on.

Because of this, we encounter a problem of how to apply forces to a particle in order to achieve the desired result. To explain this further we'll consider the two dimensional case, paying attention to the X axis only. The result applies to all axis, so this is just fine.

We'll create a couple of problems first, then talk about how to solve them, and in so doing, explain how to apply the force equation to an n-Body model.

**Situation one: Both Particles have positive values for position in X.**

![http://nmod.googlecode.com/svn/trunk/images/x_y_both_pos.png](http://nmod.googlecode.com/svn/trunk/images/x_y_both_pos.png)

The particle with the greater value for X (the top one) has the force subtracted from its velocity along the X axis. That means when the next step of the system of particles occurs it will move down, towards the lower particle. Great, all is working as planned, the force is attracting the particles together.

But if we then go ahead and subtract the force from the velocity of the other particle along the X axis, we get something rather annoying, it also moves down along X on its next step after the force is applied. The net effect is it moves away from the other particle.

**Situation two: One Particle has a negative value for position in X.**

![http://nmod.googlecode.com/svn/trunk/images/x_y_one_neg.png](http://nmod.googlecode.com/svn/trunk/images/x_y_one_neg.png)

Ok, a different situation here. The particle with a positive value for its position in X is going to move down when the force is subtracted from its velocity. All well and good, we have the correct motion.

So now we move to the particle with a negative positional value in X, and yay!, subtracting the force, because its position is a negative value, becomes an addition, and it too moves towards the other particle. So far so good.

But what happens when it moves up so much that its position becomes positive? The pull becomes a push, and it flies off in a most annoying fashion, or at best, wobbles around the origin of X.

The inevitable conclusion is that we can't always just subtract force, sometimes we need to add it. Suddenly it's not so simple, we need to add some decision making mechanism into our n-Body model.

So what are our options?

Well, I know two. There are doubtless more solutions, but you can investigate them yourself, we only need the two here.

**Solution One**

We can use a single fixed central point mass, set at origin, and no matter how many particles we have in the system, we only calculate the gravitational interaction between them and the point mass at origin, disregarding all other interactions between particles in the system.

This works, and for a solar system model, you'd get some nice looking orbits for the planets.

However, the model would be horrendously inaccurate, because the planets obviously do interact gravitationally. For example, in the reference Solar System model I provide Jupiter has a pretty significant effect which we can't just ignore. Some people still do it this way though, heaven knows why.

There's another problem with this approach, no moons. How can you have a moon gravitationally bound to a planet if your model only allows for gravitational interaction between each particle and the central Point Mass? (which in this case would likely be a Stellar Mass). The answer is, you can't, sorry.

Early on, when I was first writing nMod, I read in an apparently authoritative paper that stated it was _impossible_ to model moons 'live' in an n-Body model, and we must instead model the moon externally in another, two particle system (where the planet being orbited is a point mass at origin again), and translate the resultant co-ordinates back into the main model. This means you end up with a massless point representing you moon.

This struck me as singularly useless, but I tried it, and yes, it works, but it is most unsatisfactory. Certainly it has no place in accurate n-Body modelling. You might want to apply it in a game, where detail is less important, but we aren't interested in that here.

Since we don't want any of that silly massless point moons nonsense, and we want each particle in the system to interact gravitationally, we require a different approach, which I happen to have.

**Solution Two**

Recapping force briefly: application of force to a particle means a change in the velocity of a particle, which translates directly to change in the position of that particle.

In this second solution we include a decision making process by which we can discover whether force needs to be subtracted from, or added to, the velocity of a particle in order to achieve the correct motion. We also require that gravitational interactions are calculated between every particle in the system, not just each particle and the point mass at origin, so both of the methods described are applied to every unique pairing of particles in the system (We'll get into that later).

There are two ways to do this, which I shall label methods A and B

**Method A**

Build a set of rules which represent all the possible directions of force application in the WCS between two particles. Examine these rules sequentially to discover which matches the current state of the two particles in question, and apply that rule.

The result of the rule applied should be force application which will result in the distance between the particles reducing when the model is next iterated.

**Method B**

Write a function which works out, say, 10% of the distance between the particles, and alternately adds and subtracts it from the position of each particle.

Whichever of these operators causes the particle being examined to move closer to the other particle is the operator we should use.

The result is again that the distance between the particles should reduce when the model is next iterated.

---


Method A is by far the most complex, since it involves creating a long list of rules. Nor is it particularly easy to work out all those rules.

I tried for quite some time, and getting all possible rules right eluded me. Not just me thankfully, other people had a go too.

Method B proved much, much easier, since it takes the decision process out of your hands. There is no large set of rules to craft, just a simple instruction _'always apply force to two gravitationally interacting particles in such a way that the particles move towards each other as a result'_.

For future reference, nMod uses Solution 2, Method B.


[Lesson Two](http://code.google.com/p/nmod/wiki/basics2)


---


**Links**

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com