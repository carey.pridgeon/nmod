![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# n-Body Modelling: Lesson Two #
**_Gravitation_**

---

## Why are we only using Newtons method, why not Einstein's? ##

Relativistic effects are required to accurately model the motion of Mercury, since that is the only way to describe the odd precession of that planet.

However, to do so requires a far more complex model then we need to explain the motion of any other body, so its serious overkill unless you are specifically interested in that problem.

In the Newtonian system gravity travels instantaneously. We know now that gravity moves at the speed of light, so obviously there is some loss of accuracy with the Newtonian system. However this loss of accuracy turns out to be trivial on the scale of our Solar System, and unimportant for almost all issues at that level.  This makes the Newtonian method ideal for n-Body modelling.

### Since the Solar System contains more than two bodies, why are we only dealing with pairs? ###

Simple, we only need to deal with pairs. To do any more in one calculation is beyond current mathematics (it's called the three body problem, and there is no solution at present), and as you will see, we don't have to anyway. All we need to do is calculate the effect of every particle in our model against every other particle, and we do this by working out every possible pairing, and doing these one at a time inbetween steps. I'll explain how this is done later.

## Gravitational attraction, falling with style ##

Why is it, if particles are always falling towards each other, don't they all clump together?

Not so obvious this one, at least not to some people I've met, and not to me, first time I heard it, so I shall explain.

The result of _any_ gravitational interaction is that the particles involved in that interaction will move towards each other. The distance between them will reduce.

However, considering the case of planets in stable orbits, they are already moving relative to each other, so, this fall does not result in collision (Not every object in the Solar System are in such stable orbits, but we are using the stable case for now).

The easiest way to demonstrate this is to show what happens when this falling towards each other is not happening, and when it is. For this we will use the model of the Solar System in the reference project.

### Scenario one: The planets are not falling towards each other ###

![http://nmod.googlecode.com/svn/trunk/images/gravity_fail.png](http://nmod.googlecode.com/svn/trunk/images/gravity_fail.png)

### Scenario two: The planets are falling, and they can't get up. I mean, the planets are constantly falling towards each other. ###

![http://nmod.googlecode.com/svn/trunk/images/gravity_working.png](http://nmod.googlecode.com/svn/trunk/images/gravity_working.png)

In scenario one we saw that the planets are flying off in a straight line. I have turned off the effect of gravity for this example, so this line is representative of their exact velocity and direction of travel at the point when gravity was removed. Without gravity, the velocity and direction of travel which the force exerted by gravity must overcome is revealed.

In scenario two, we see that the planets are in nice orbits, behaving themselves and not doing anything silly.

The _fall_ towards each other has been restored, since gravity is back on, but the tendency to travel in the straight lines seen in scenario one remains at all times. Therefore the force exerted by gravity is countering this straight line movement, drawing the planets and asteroids into their stable orbits.

If we had no motion other than that resulting from gravity, meaning each planet and asteroid were stationary relative to each other and the collapsor (new word, the point mass representing the dominant gravitational influence is called a collapsor), they would fall towards the collapsor in straight lines. The lines wouldn't be perfectly straight in fact, because the planets pull on each other, but pretty straight nonetheless.

## Collapsors ##

Collapsor, as I said above is a term used to describe a particle in an n-Body model which dominates the other particles (is much heavier than). There is no requirement that there be only one collapsor in a model, but this is the case when modelling a single star planatery system, such as our own. You needn't have one at all in fact, but generally, they are used.

A collapsor need not be at origin, except for some particular classes of problem where the main attracting particle must be held fixed. One such occasion is when you are modelling the Solar System, as you will find the reference build of nMod does when you use the  Solar System project provided. I have left out the exact orbital data for Sol in the Solar System project provided with the toolkit, this is because I prefer to hold Sol at the Solar System Barycentre, which is at co-ordinates [0,0,0]. I have tested the reference project with Sol allowed to move, and while the Solar System retains the correct orbits for all particles, Sol wanders away, taking the planets with it, which I don't want.

Here we have a sequence of images showing a globular cluster being impacted by two stars (collapsors), each of whose mass is each equal to the combined mass of all the  bodies in the globular cluster. (If you want a copy of the file that contains this time series, it's [here](http://www.politespider.com/nbo/cluster_smash.zip). I'll get to how you view it in the 'using nbview' section.)

### The beginning of the simulation, with the globular cluster unaffected ###
![http://nmod.googlecode.com/svn/trunk/images/cluster_at_state_1.png](http://nmod.googlecode.com/svn/trunk/images/cluster_at_state_1.png)

### The two particles are approaching the globular cluster, causing it to distort ###
![http://nmod.googlecode.com/svn/trunk/images/cluster_at_state_94.png](http://nmod.googlecode.com/svn/trunk/images/cluster_at_state_94.png)

### The two particles are inside the globular cluster, which is now being torn apart ###
![http://nmod.googlecode.com/svn/trunk/images/cluster_at_state_188.png](http://nmod.googlecode.com/svn/trunk/images/cluster_at_state_188.png)

Lets take a look at both collapsors once they've left the vicinity of the collision.

Collapsor one has a small number of orbiting particles in stable orbits.

![http://nmod.googlecode.com/svn/trunk/images/collapsor_1.png](http://nmod.googlecode.com/svn/trunk/images/collapsor_1.png)

Collapsor two on the other hand, has ripped out a large group of bodies from the cluster, all of which have remained in the vicinity of collapsor two for some time, and have also adopted stable orbits. The orbits are less clear in this image because a lot of unrelated particles pass by this group, leaving their trails in the image. With so many particles in orbit I would expect further disruption in the system at some point, but this particular simulation halted at the point shown.

![http://nmod.googlecode.com/svn/trunk/images/collapsor_2.png](http://nmod.googlecode.com/svn/trunk/images/collapsor_2.png)

## Different types of n-Body model ##

There are quite a few different ways to model the gravitational interaction of particles over time. I'm not going to cover them in detail, only the method nMod uses, but I'll list some so you can go look the up yourself.

  1. Barnes-Hut
  1. Tree Particle Mesh
  1. Fast Multipole Method
  1. Particle-Particle


Plus many systems created for specific problems, like stellar collision modelling. Most of these methods usually run on some seriously big iron, certainly well beyond my budget.

Of the above, Particle-Particle (PP), the method employed by nMod, is the slowest, and can handle the least number of particles. This is because it is concerned with the accuracy of individual particles, whereas the other methods are for large scale models, where the behaviour of particles in groups is of greater importance, and accuracy for an individual particle less so.

nMod is specifically concerned with accurate modelling of individual particles, so it needs to use PP.

Ok then, now we're at the point where we need to go into some more detail regarding the PP approach, so lets move on to the next lesson.


[Lesson Three](http://code.google.com/p/nmod/wiki/basics3)


---


**Links**

[Lesson One](http://code.google.com/p/nmod/wiki/basics1)

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com