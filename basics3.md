![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# n-Body Modelling: Lesson Three #
**_The Particle-Particle Method_**

---


## The Method ##

Given a set of particles, calculate the gravitational interaction between all particles, such that the precise effect of each particle on every other particle is known. Then move the particles in accordance with the gravitational effects found.

Thus for each of **_n_** particles, **_n-1_** interactions must be calculated when the model is iterated through a single step.

For example, consider a model with **_n = 4_** particles. We have the following pairs:

|0|1|
|:|:|
|0 |2 |
|0 |3 |
|1 |2 |
|1 |3 |
|2 |3 |

Each of these must be calculated (the gravitational interactions between the particles in the pair obtained) on each step of the n-Body model. Note that in these pairings **_(0,3)_** is considered to be identical to **_(3,0)_**, so only **_(0,3)_** appears.

All Particle-Particle (PP) needs is the starting conditions: mass, position and velocity, of all particles in the model, and the gravitational constant to apply.
With this it can calculate the revised positions and velocities of all particles at the end of the required step.

Since PP works for a single step only, it requires an additional function called a _driver_. This function passes the current state of the set of particles into to the PP function, retrieves the revised state, and feeds that back into the PP function once again.

This continues until some required number of steps has been completed.

## Accuracy, and its consequences ##

PP is the simplest of all n-Body modelling methods (which isn't to say its all that simple itself, I should be wary of using that word). It is also the most accurate in terms of calculating the precise position and velocity of all particles in an n-Body model.

However, while it is the most accurate, it is rarely, if ever employed in large scale n-Body modelling, such as galaxy, or stellar collision modelling.
The reason for this is that the accuracy of PP comes at a high computational cost, as the following graph demonstrates.


![http://nmod.googlecode.com/svn/trunk/images/pairs_increase.png](http://nmod.googlecode.com/svn/trunk/images/pairs_increase.png)

As we approach 1000 particles, the number of pairs is approaching 50,000. That takes quite a long time to work through for each step, even on a relatively fast PC.

As a result of this computational load, most methods which are examining larger numbers of particles use some sort of grouping method, where particles are considered in groups, rather than individually. This reduces the computational load significantly, since groups of particles replace the original pairs, and there is less work to do per step. As a consequence of this there is a loss of individual particle accuracy. However, when modelling galaxies for example, it is the behaviour of the entire system which is of interest, not the behaviour of individual particles, so this is an acceptable trade off.

nMod, which uses PP, is not meant for such large scale modelling. It is designed specifically for modelling systems where per particle accuracy is more important than speed. The default dataset has less than 60 particles in it, representing a model of the Solar System. In this situation, PP is ideal. The default dataset will grow to hundreds of particles eventually, as the detail in the Solar System it contains is improved, and that's still well within the reasonable range for PP where accuracy can be maintained.

Actually, it _is_ possible to model systems of several thousand particles with PP. This requires making the step size larger. For instance the default dataset works out the positions of particles every 60 seconds. That's quite accurate, and it can go as low as every ten seconds. You can go up to step sizes of hours, days, or longer if you want. You'll lose accuracy, but it means nMod can be used for smaller galaxy simulations, which can be fun.

Now we move onto detailing how nMod's n-Body model is implemented

[The nMod n-Body Model Technical details: Part 1](http://code.google.com/p/nmod/wiki/internals1)


---


**Links**

[Lesson Two](http://code.google.com/p/nmod/wiki/basics2)

[Lesson One](http://code.google.com/p/nmod/wiki/basics1)

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com