![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# Compiling the nMod Toolkit: Part 1 #
**_Compilation under Linux_**

---

The reference verion of the nMod Toolkit has pre compiled binaries for 32 bit Linux, but you may wish to compile your own version, either to make alterations to the reference versions, or to recompile for a specific architecture.

Each utility in the toolkit comes with a Makefile. Thus in Linux it is a matter of unpacking each utility into it's own folder, then executing make in a console window.

Once each binary is compiled, place them all in the same folder, as I have already done in the reference release. This makes it easy to run the programs in sequence, such as running nMod to get the time series, then launching nBview to display the result.

While you can place all the programs in /usr/bin/, I suggest leaving them in a sub directory of your home directory, since that makes installation much simpler, and does not require root privileges.

nBview requires glut development libraries to be installed. Installing freeglut-devel should cover it nicely.


For those of you who want to use the nMod toolkit in windows, the next tutorial covers this

[Compilation under Windows](http://code.google.com/p/nmod/wiki/compile2)


---


**Links**


[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com