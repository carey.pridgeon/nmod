![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# Compiling the nMod Toolkit: Part 2 #
**_Compilation under Windows_**

---


The primary source for the windows version of nMod is an installer program, and that includes compiled versions of all programs in the toolkit. However, if you wish to program using the nMod toolkit, you'll need to know how to compile it, hence this guide.


---


Currently I'm developing the windows version of the toolkit using the mingw-32 compiler for windows. In fact any win32 C compiler that produces win32 native binaries will do.

Download mingw from the [Mingw website](http://www.mingw.org/)

If  you are recompiling nbview you also need to have the glut32 development libraries installed in the lib/include folders of mingw. These can be downloaded [here](http://nmod.googlecode.com/files/glut_libs.zip), or obtained from the dependencies sub folder in SVN.

the dll's from each need to be in the same folder as your nbview binary in order to run it, or in windows\system32.

As with Linux, all programs in the nMod toolkit are compiled using Makefiles. To use them you need to open a console window at the folder you have the source in, and type mingw32-make.

If you do not have the windows addon installed that allows you to right click on a folder and open a command line, you can get it [here](http://download.microsoft.com/download/whistler/Install/2/WXP/EN-US/CmdHerePowertoySetup.exe). Vista users do not need this, just hold down shift as you right click on a folder, and the command line here option will appear.

Once you have compiled all the programs inn the nMod toolkit, its a good idea to move them all into the same folder, as this makes them easier to use.


The next tutorial in this section covers setting up mingw properly so it is on your system path.


[How to set up Mingw](http://code.google.com/p/nmod/wiki/compile3)





---


**Links**

[Compilation under Linux](http://code.google.com/p/nmod/wiki/compile1)

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com