![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# Compiling the nMod Toolkit: Part 3 #
**_Setting up Mingw in Windows_**

---



First download mingw from the [Mingw website](http://www.mingw.org/)

This tutorial is aimed at users of the nMod toolkit, but applies to anyone wishing to set up gcc under windows. It does not cover setting up mingw in Vista, but I have done it, and it's not too much different.

## choosing a location ##

Install it in:

C:\MinGW

Which is the default location.

## Setting up your path ##

The nMod toolkit uses Makefiles, not project files that would be tied to a particular programming IDE.
Therefore we need to tell windows where to find the files we need (gcc.exe and so on) when we use the Makefiles.

Right click on the My Computer icon, either on your desktop if its there, or on the start menu entry. You will see this


![http://nmod.googlecode.com/svn/trunk/images/system_properties.png](http://nmod.googlecode.com/svn/trunk/images/system_properties.png)

Now select the 'Advanced tab, and choose 'Environment Variables'. You should see something like this

![http://nmod.googlecode.com/svn/trunk/images/environment_variables.png](http://nmod.googlecode.com/svn/trunk/images/environment_variables.png)

Now we are in one of those places in windows that I like to describe as 'mess about here and your computer will die horribly' places. Be very careful just to change what I say....

If you installed mingw studio in the default location, then copy the following text

;C:\MinGW\include;C:\MinGW\bin;C:\MinGW\lib;

exactly as it is here onto the  **end** of the entry named **Path**.

Don't worry if you end up with two semicolons at the start because one was already there, its just a separator and will be ignored.

Click ok, and we're done here, close it all off. Any changes just made will not effect any consoles that are already open, so 'close em if you got em'.

You can create seperate LIB BIN and INCLUDE variables, and not use Path alone, but the above works, and it's simpler. I have done this myself before, but I advise you use the described simpler method unless you already have these environment variables defined (for instance if you have visual studio already installed).

## Setting up Glut ##

If you want to compile nbview, you need to add Glut to mingw studio. To do this, download the right build of glut, conveniently located [here](http://nmod.googlecode.com/files/glut_libs.zip), which needs to go in the nbview source directory after you have unzipped it, along with a copy of glut.dll from the glut zip file. Note that these are the files you need to have in the same folder as the Binary (sorry, this is windows, I mean 'executable') or it won't work.

Anyway, drop glut.h into

C:\MinGW\include\GL

and glut.lib and glut32.dll into

C:\MinGW\lib

And you're set, nbview will now compile against opengl.

## Using the console ##

Now mingw can be used via the console to compile all of the nMod Toolkit. To do this requires that you use either a console window, or the make and clean batch files that will compile, an clean up a previous compilation (delete object files and the executable). Actually, even if you use a console I recommend you avail yourself of these batch files, they are a lot simpler then typing mingw32-make and mingw32-make clean, which you would otherwise have to do.

I find navigating around drives and such in the windows console quite tedious. I recommend you install [this](http://download.microsoft.com/download/whistler/Install/2/WXP/EN-US/CmdHerePowertoySetup.exe) utility from [Microsoft's Powertoys Site](http://www.microsoft.com/windowsxp/downloads/powertoys/xppowertoys.mspx) that will allow you to right click on a folder and open a console window for that folder. Its much neater.


## Conclusion ##

That should be about it, everything will now run when you use the Makefiles.

If you decide that Makefiles are too 1970's for you, you could always try one of the IDEs for C in windows, like [Zeus](http://www.zeusedit.com/ccpp.html), or [Dev-C++](http://www.bloodshed.net/devcpp.html).

If the above mingw setup hasn't worked for you, then drop me a line and I'll try and sort out what's gone wrong.

Some people like to use Visual Studio. So I have some help with that too.

[Compilation of the nMod toolkit using Visual Studio](http://code.google.com/p/nmod/wiki/compile4)



---


**Links**

[Compilation under Windows](http://code.google.com/p/nmod/wiki/compile2)

[Compilation under Linux](http://code.google.com/p/nmod/wiki/compile1)

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com