![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# Compiling the nMod Toolkit: Part 4 #
**_Compilation of the nMod toolkit using Visual Studio_**

---

**This section of the user manual will be revised soon. I've just got a shiny new copy of Visual Studio 2008, so I'll be able to work out how to compile the entire toolkit with it now.**

---


If you want to use any of the nMod Toolkit Makefiles with Visual Studio, edit whatever Makefile you are using, replace 'gcc' with 'cl', and add /nologo to the end of the LDFLAGS line. I don't know if cl makes use of the -O3 compiler directive, perhaps someone can enlighten me on this.

Visual Studio does not seem to allow for the use of Makefiles in 'solutions', I have no idea why this would be the case, seems darn silly to me, but there we are.

Compiling with my Makefiles using Visual Studio requires that the Visual Studio bin/lib/include folders are on your path. There is a batch file provided with visual studio that does this automatically which can be found at:

**Visual Studio\VC98\bin\VCVARS32.BAT**

If that fails, I have no idea what you can do, sorry.

Anyway, to compile at a command prompt using the Makefile, type:

**_nmake_**

at the command prompt.

Or you could just create a new project in Visual Studio and import the code. That might work too.

I will admit to ignorance regarding the compilation of nBview with visual studio, as I haven't ever tried.


---


**Links**

[Setting up Mingw in Windows](http://code.google.com/p/nmod/wiki/compile3)

[Compilation under Windows](http://code.google.com/p/nmod/wiki/compile2)

[Compilation under Linux](http://code.google.com/p/nmod/wiki/compile1)

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com