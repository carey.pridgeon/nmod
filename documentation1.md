![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# nMod-ea Documentation #

**_nMod-ea technical details_**


---


## The problem that nMod-ea is designed to solve ##

nMod-ea is an extension of the nMod n-Body model with additional functionality that allows experiments to be performed with Evolutionary Algorithms (EA) on particles within the n-Body model. It is assumed that the subject of this mutation would be a spacecraft whose orbit you are trying to create and refine, but it need not be. For the purposes of this tutorial I am assuming you are trying to create and refine the orbit of a spacecraft.

## Differentiating the n-Body model code from the code designed to run the EA experiments ##

Not all the code provided with nMod-ea needs to be edited when you are designing an experiment. Any function that begins with an underscore can be ignored. Essentially the n-Body model runs as a black box so far as the EA code is concerned. You put together the input for the n-Body model, and analyse the output, but what happens inside the n-Body model need not concern you. If you are interested in it, read the nMod Toolkit user manual.

Every other function can be edited. In general function names (and their filename, most are in a C file of the same name) have a prefix that reveals their purpose, such as _mutation_ (is a mutation operator),or _runtime\_reset_ (allows you to alter parameters provided in the project file while the program is running).


First a little note. When referring to a particle I will sometimes use the word chromosome. This is when I am referring to the particle as something subject to mutation. All chromosomes are particles, but not all particles are chromosomes. For example, in the projects provided, Earth is present as a particle, but never subject to mutation, therefore it is not a chromosome. The spacecraft _is_ subject to mutation, so is referred to as a chromosome.

## Core Concepts ##

nMod-ea revolves around three core elements. These are the _environment population_, the _EA population_, and the _temp particle_.

These all reside within the ea\_project data structure. Everything used in a single experiment is contained within this structure, so assume everything mentioned in terms of variables and arrays means 'within the ea\_project structure'.This is of type `project`, which can be found in the nmod\_structures.h file.

### The Environment Population ###

This is the set of particles that the n-Body model works on. In the example projects provided this population is composed of Sol, Earth, the Asteroids Apophis and Eros, and the spacecraft. This is the population within which your evolving chromosomes (particles) will be placed for testing. The spacecraft particle can be considered a placeholder particle. In order to test a chromosome you reset the environment particles (via provided functions that will be covered shortly), and move your chromosome into the environment population to be tested.

### The EA Population ###

This is the set of particles which form your population of chromosomes. This population is never seen by the n-Body model, only the EA code acts on it, deciding which members survive, which get over-written and so on. This population is used even if you are using a Hillclimber approach, at which point the population has one member.
Many functions exist to interact with this population, so its a good idea to use it if you don't want to re-code your own population management functions.

### The Temp Particle ###

This particle is your point of communication between the environment population and the EA population. When a particle is selected for mutation and testing from the EA population, it gets copied into the temp particle, using the function `copy_particle_from_population_to_temp_particle`. All copy operations are named for their task, which should make locating the one you need easier.

All mutation operations are performed on the temp particle. All the example mutation operators provided are set up to do so, so reading any one will show you how this is done.

After the temp particle has been prepared, you copy it into the environment population, again using a function written specifically to perform this task, called `copy_temp_particle_to_environment`.

Once the n-Body model has completed its run, you check to see how well it performed (using either the provided fitness checking functions, or your own). If the temp particle, which we mutated before testing in the n-Body model, performed better then the original chromosome which was its parent, and gets to survive, then it needs to be copied back into the EA population.

To do this we _do not_ copy the spacecraft back out from the environment population. That spacecraft has used all its fuel, and has a changed velocity and position. Instead we copy the temp particle (which still holds that superior flight plan, full fuel load, and starting conditions), back into the EA population.

There is a copy operation specially written to perform this task, called `copy_temp_particle_to_population`.
You need to preserve the improved score of the temp particle as well, so that gets written to the scores array. A function to perform this task has been provided, called `copy_temp_particle_score_to_scores_array`

Usually you would seek to locate the worst chromosome in the population and over-write that, and its score in the scores array. How this is done will be covered when we walk through an EA example.

## How the EA code is used ##

In order to explain this we will go through the way in which nMod-ea performs a steady state EA experiment using the provided code.

### Loading the Data ###

This stage loads in the project (nmp) file which is passed to the program as a parameter. The function which handles this is `load_project_into_nmod`. It takes the name of the project file as a perameter, plus an integer 'rval'. This is set to 1 if the project filename provided is invalid, or the project file can't be opened. This is primarily for debugging. The function returns a filled project data structure, which is passed to the global project variable 'ea\_project' by default.
An in depth description of what `load_project_into_nmod` does will be provided later, as this manual improves, but suffice it to say all the arrays required by nMod-ea are created and initialised in this function. The function is fairly well documented in comments, so you could read it for some idea of how it works.

If you make changes to the nmp file, it is this function that you need to change if you want your changes loaded in, plus of course add code elsewhere to use your new parameters.

### Preparing the environment population ###

Open up the file 'EA\_main.c' and find the line

`save_out_environment_particles_starting_state(ea_project);`

This function call saves out a copy of the environment populations starting conditions. This is required because each time we test a chromosome in the n-Body model, the particles in the environment change end up in new positions, with different velocities. We need to be able to reset the environment after each test, and this function stores the starting conditions we need.

The starting conditions are held in the array

`    particle *state_reset_particles;`

inside the ea\_project data structure. You don't need to interact with this directly, the function to reset the environment already exists. It's called `swap_particles_starting_state_in`, and is used thus

`swap_particles_starting_state_in (ea_project);`

Notice that the parameter of this function is the project data structure ea\_project. This is true of many functions. This is meant to simplify the process of interacting with the project data structure. Often you don't really need to know which members of the project data structure are being used, you only need to know what function to call to achieve the desired result.

In addition it means that you could have more than one project resident in memory, but we aren't handling that case here.


## The pre-evolution group ##

This particular aspect of nMod-ea resulted from the realisation that simply generating a population of random particles might well (and in early tests, frequently did) produce a population so poor that finding a decent solution proved extremely difficult. Therefore we introduced the concept of a pre-evolution group.

The pre-evolution group is a set of particles which are generated and tested _for each member of the final population_. What happens is for each index _j_ of the EA population, a succession of putative chromosomes are created, get a burn plan assigned, get place into the environment, and have their position/velocities set(This can't always be done beforehand, as position may be relative to some particle in the environment), at which point, and before the n-Body model is started, the chromosome is copied from the environment into the pre-evolution group chromosome array, where it waits for all the chromosomes in the group to be tested. We need to make a copy before it is tested because the process of testing uses all its fuel, and changes its position and velocity.

Once the required number of pre-evolution chromosomes has been generated, stored and tested, the best one found 'wins' and gets copied into the EA population as the actual population member for that index.

So, if you select an EA population size of 100, and a pre-evolution group of ten, the for each of those 100 chromosomes, ten potential chromosomes will be tested, and the best of each ten selected to be in the final population of 100.

Pre-evolution group chromosomes are tested using exactly the same fitness function you will use during evolution, and (if you have turned that option on), checked to see if they collide with anything, and rejected if they do.

The result of all this is that instead of having 100 entirely random and untried chromosomes, you have 100 randomly generated chromosomes which were each the best out of ten attempts. Still random and unoptimised, but at least you can be reasonably sure you haven't got a population full of spacecraft that nosedive into a planet.


---


Now for a more exact description for those of you who want to manipulate the pre-evolution group code. Note that if you don't, all you need to do is activate it (its on by default in all example experiments) and make sure the fitness function is the same as the one you intend to use in the actual experiment.


---


As each chromosome of the pre-evolution group is prepared for testing, it is initialised with the kind of starting conditions we require for the experiment. In the examples provided this consists of three things. First the spacecraft gets its fuel load. This is some percentage of the maximum allowed amount of fuel, and is allocated to the pre-evolution group chromosome by the function `set_fuel_payload_for_single_rocket`.

In the example main, it has the following parameters:

`set_fuel_payload_for_single_rocket(ea_project,i,((float)pct/THOUSAND)*ea_project->current_max_fuel)`

First, as usual, we pass the ea\_project structure, then _i_, which is the index in the pre-evolution group of the chromosome being prepared. After this we have `((float)pct/THOUSAND)*ea_project->current_max_fuel` which basically means assign some random percentage (_pct_ being a random percentage value already calculated) of the currently set maximum fuel load. I left this as being a parameter because you may not always want to be restricted by the max fuel measure.

Once this is done its time for the burn plan to be created. Burn plan handling is a bit complex, but I've tried to automate it to hide this complexity. In order to create the burn plan a call is made to `create_single_burn_plan_for_particle_in_EA_population`

this is called as follows:
`create_single_burn_plan_for_particle_in_EA_population(ea_project,i,ea_project->number_of_burns);`

All you need to do is set the number of burns. This is a simple integer, so you can use the number defined in the project (nmp) file, or set some random number.

There is no need to have all chromosomes using the same number of burns, so how many burns to use can also be subject to mutation.

Having prepared the pre-evolution group chromosome, we get ready to copy it into the environment.

Before doing this, we reset the environment particles thus

`swap_particles_starting_state_in (ea_project);`

and we copy in the pre-evolution group chromosome

`copy_particle (&(ea_project->particle_pop[i]),&(ea_project->particles[ea_project->spacecraft_array_pos]));`

This is one of the few times the basic copy function is called, all other particle copy operations make use of this function.

Ok, now its time to set the particles position. This is dependant on the location of Earth in this example, so had to wait until the chromosome was in the environment. This operation is performed by the function `set_particle_starting_state_in_random_orbital_pos_around_another_particle ` which is called as follows:

`set_particle_starting_state_in_random_orbital_pos_around_another_particle (ea_project,1,ea_project->spacecraft_array_pos,10900,LOW_EARTH_ORBIT,THOUSAND);`

This function takes the ea\_project structure, the index of the particle which will be orbited, in this case Earth at array index 1, and the location of the spacecraft in the environment population. This is specified explicitly rather then fetched inside the function (which could happen as the variable is a member of ea\_project), because I wanted to keep the option open of moving the spacecraft to another array pos, or using this function on another particle. the number 10900 is the escape velocity for a spacecraft in low earth orbit in meters per second, and the constant LOW\_EARTH\_ORBIT is set as one possible altitude for low earth orbit. You can choose another if you like. the constant THOUSAND is for a divisor parameter that isn't used any more and will be removed from this function in the next release.

Ok, the pre-evolution group chromosome has been created, prepped, and is ready to go, so we save a copy of it into the pre-evolution group array with a call to `copy_particle_from_environment_to_pre_evolution_group`, and run the n-Body model with a call to run\_n-Body\_environment.

A call to run\_n-Body\_environment  starts the n-body model running. Whilst it is iterated for the required number of states and steps, run\_n-Body\_environment applies the burn sequence, checks for collisions (if this option is on), and records the performance data for the spacecraft and its target/s.

Once the n-Body model exits the chromosome has to be tested to see how well it performed. Assuming for the moment that there was just one target,  the function `scan_distances_array_of_pair_one_for_closest_pass (ea_project);` looks through the recorded distances array and locates the point at which the spacecraft and the target were closest to each other.

This is the point at which, if you wanted a more complex measure of fitness or scoring, you would add code.

Once the closest pass is found, we record that as the score for this pre-evolution group particle as follows:

`ea_project->pre_evolution_scores[j].single_score = ea_project->minimum_distance_record_for_pair_one.distance;`


Rinse and repeat for the entire pre-evolution group for the current particle, then the best member is found with a call to the function

`winner = select_best_from_pre_evolution_group (ea_project);`

With the winner getting to be the population member for the current population index.


Once this is completed for the entire population, its time to move on to the EA itself.

## Running the EA ##

The first thing that happens is we enter the for loop which actually performs the EA for however many iterations are requested in the nmp file.

The first thing that then happens is that a check is made to see if its time to write a progress report. It won't be yet, but once the proegress recording interval limit is reached, a progress report will be written to file, so we will describe it now.

### Progress reporting ###
Basically a progress report is a complete report such as would be written when the EA finally exits. It writes a plain text report of the performance of the EA, and a complete nbo file that can be played back in nbview. This serves three purposes.

Firstly, and most obviously, you get to check up on how the EA is doing.

Secondly if the computer you are using crashes or something, you at least have something to see what the state of the EA was the last time a progress report was written.

Finally, if you discover that the EA has achieved the required goal, but would still run for a long time yet, you can close down the program there and then.

The progress report is really just my idea of what you would need to know, and almost certainly wont suit someone else's experiment. Therefore I suggest you add or remove information that is added to the report file as you wish.

Remember that the progress report file is the same as the final report, so you have to edit this as well. I should have the progress reporting code as a separate re-used function, but haven't done this yet. You may find it easier to do so, or you can nag me and I'll do it.

### Chromosome selection and mutation ###

In the EA example I use tournament selection. This is acheived by a call to ` n_ary_tournament_selection` as follows

`selected_particle = n_ary_tournament_selection (ea_project);`

You can use whatever tournament size you want, by setting it in the nmp file or changing it using the relevent runtime reset function (which functions allow you to change most perameters set in the nmp file).

Remember, all mutation operations happen on the temp particle, so we copy the selected particle into the temp particle thus:

`copy_particle_from_population_to_temp_particle (ea_project,selected_particle);`

And then we mutate it using one of the five provided mutation functions. read ea\_main for the calling code, but the functions are

`mutation_alter_spacecraft_fuel_load (ea_project);`

`mutation_shuffle_thusts_within_one_axis (ea_project);`

`mutation_shuffle_thusts_between_axis (ea_project);`

`mutation_shuffle_fuel_allocation_between_burns(ea_project);`

`mutation_change_a_burns_timing (ea_project);`


These are examples of mutation, and do not represent all possible mutation operations. Actually looking at them I see I have spelt thrusts wrong, darn. I'll fix this in the next release.


### Using the n-Body model to test fitness ###

Actually the n-Body model doesn't test fitness, it corllects the data on which a judgement of fitness will be made. The only time it explicitally rejects a particle is if a collision occurs.

So, once again the environment particles are reset to their starting state

`swap_particles_starting_state_in (ea_project);`

And the spacecraft is copied in

`copy_temp_particle_to_environment (ea_project,ea_project->spacecraft_array_pos);`

Now the variable used to indicate a collision is reset to a no collision state

`ret_nb = NO_COLLISION;`

And the n-Body model is started, with its integer return being stored in the collision check variable.

`ret_nb = run_n-Body_environment (ea_project);`

If no collision was detected then the chromsomes fitness is evaluated in exactly the same was as occurred during pre-evolution group testing, so we won't repeat that here.

If an improvement has been found, this is reported to the screen (if writing to stdout is enabled in the nmp file), then the chromosome is saved.

To do this, the current worst chromosome in the ea population is located (the one with the largest closest pass value in this case), and over-written with the chromosome we just tested. The score of this chromosome is also recorded in the scores array, again over-writing the score of the worst chromosome.

If you are altering the fitness function in your experiment, the fitness evaluation (scanning functions), code needs to be changed to suit your criteria. The fitness evaluation method provided is pretty basic, and meant only as an example.


### Locating the final best chromosome ###


This is simply a matter of scanning the EA population once evolution has finished to find the chromosome with the lowest closest pass distance.

This is specific to the example EA provided, and may not suit your own fitness criteria, so would need to be altered.

Recall that the pre-evolution group should be judged using the same fitness evaluation method, so change that code too.

Once the final chromosome is found, it is copied into the environment, which is run one last time to get the final nbo recording.

The final report (or the progress report) does not include the exact starting conditions of the chromosome deemed best, so you may wish to add this code in. I will add it to the next release if I remember.

Now we move on to the [data structures used by nMod-ea](http://code.google.com/p/nmod/wiki/documentation2).



---


**Links**

[nMod-ea technical details, part 2](http://code.google.com/p/nmod/wiki/documentation2)

[Students Main Page](http://code.google.com/p/nmod/wiki/students)

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com