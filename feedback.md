![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)

# User Feedback page #



---

I'm always interested to know what people think of the nMod Toolkit, and what sort of things they use it for. If you have something to say, add it as a comment on this page.



---


**Links**

[Project Homepage](http://code.google.com/p/nmod/)

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com