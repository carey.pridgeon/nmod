![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# History of the nMod Toolkit #


---


I developed the first version of the nMod Toolkit in 2002/3 as my undergraduate dissertation at Reading University, in England.

It was applied, under the direction of [Professor David Corne](http://www.macs.hw.ac.uk/~dwcorne/) (now of [Heriot Watt](http://www.hw.ac.uk/home/) University), to the problem of intercepting Earth Crossing Asteroids. We enjoyed some moderate progress on this problem at the time, but the limitations of time imposed on a dissertation, and subsequent research demands whilst completing my phd have meant that such research needed to be put on hold, and is only now being resumed.

Over the last several years I have continued to develop nMod in my somewhat limited spare time, and introduced a number of improvements on the original design. Some of these have been a direct response to the requirements of researchers who used nMod in their own work.

The work I did on nMod, and the need to conduct more complex experiments has now led me to developing a much more capable nBody Model development framework, called Moody. That can be found [here](http://moody.politespider.com/index.php).

---


**Links**

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com