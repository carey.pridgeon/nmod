![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# The Integration Methods in nMod: Part 1 #
**_Introduction_**

---


nMod currently has two integration methods available for experimentors to use, Midpoint integration, and Runge-Kutta fourth order integration.
These are provided to allow for n-Body experimentation at with varying degree's of accuracy.
Midpoint is the fastest method, but correspondingly, less exact. That said, it is still a capable method employed extensively in n-Body modelling, especially when modelling galaxies.

Runge-Kutta (RK4) offers much greater accuracy. However, this accuracy comes at the price of time taken, as this integration method is much slower than Midpoint.

## Horses for Courses: When to choose between the available methods ##

Under different circumstances, both methods have significant advantages over each other.

### When to choose Midpoint Integration ###

Midpoint is best used if you are modelling systems for which exact orbit replication is not a priority. This might be if you are modelling clusters, or while a spacecraft is traversing space a long distance from any other body. During such a time there are likely to be few sudden changes in velocity, so Midpoint integration could be used.

You could also use Midpoint integration during the early stages of an orbit optimisation process, where exactness is less important, since you will refine your result later.
### When to choose Runge-Kutta Fourth Order Integration ###

When your simulation needs to be really accurate, its time to use RK4.

The default project provided with nMod contains a partial model of the Solar System. This is a prime example of a time when accuracy is required, because orbits need to remain stable over very long periods, and we are concerned with particles remaining as close as possible to the orbits of the bodies they mimic.

You would also use RK4 during a spacecraft modelling experiment during the times that the spacecraft is close to another particle in the model, or when altering the course of the spacecraft.

If you are performing an orbit discovery/optimisation experiment, then RK4 would be applied during the latter stages of the experiment, when the orbit is being refined towards the final answer.

Note: _The nMod toolkit doesn't come ready to perform such experiments, but it has almost everything you'll need if you want to use it as a base for a new application to perform this task. I will add tutorials showing how to construct such an application from nMod soon._


### Switching between Integration methods ###

Actually, this isn't especially hard, since the project data structure that nMod uses to store everything involved with a single experiment can be switched between integration schemes without harm while the experiment is running. We'll go into how to do this later, when we cover a few examples of altering the n-Body model reference build (not actually available yet, coming soon though..).

The reference build only allows for a single integration method to be employed during an experiment, with that integration method being selected in the nmp file.

### Choosing the step size for integration ###

As you will have learned by now, the n-Body model works by moving the particles forward in time over a series of steps.

Selecting the step size to be used should be done with care.

Smaller step sizes increase the resolution of the model, but slows it down, as more work has to be done to get the particles to cover a certain distance. This means you can check the relative positions and velocities of particles over short distances, butt the computational cost is high.

Selecting a longer step size reduces the time the model takes to move the particles over a specified period of time, thus reducing the computational workload. In doing so you get a result faster, but the distance particles cover inbetween opportunities to analyse position and velocity increases.

So with both choices you gain something, and lose something. Which to use is Dependant on your needs for a given experiment.

Another important aspect of the step size choice is that nMod requires even step sizes. The step must be divisible by two, or you will not be able to obtain position information for the exact middle of the integration step. This is critical, because if you can't do this, you will get significant drift, and accuracy will be lost.

I consider the minimum step size for nMod to be six seconds. You could go to two or four seconds, but that is unlikely to be useful. Ten seconds provides very good accuracy, and sixty seconds does not appreciably reduce accuracy, so far as I have been able to tell.

---


Time to learn a little more about the integration methods themselves, starting with Midpoint integration.

[The Integration Methods in nMod: Part 2](http://code.google.com/p/nmod/wiki/int2)


---


**Links**

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com