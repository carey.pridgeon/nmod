![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# The Integration Methods in nMod: Part 2 #
**_The Midpoint Integration Method_**

---

Midpoint integration is provided in the nMod n-Body model through the function [move\_particles\_MP](http://code.google.com/p/nmod/source/browse/trunk/release/nmod/_move_particles_MP.c). You don't need to read the function to follow this section, since this is a description of how midpoint integration works, rather than its precise implementation in nMod. If you are planning on extending nMod's n-Body model, it could be useful to have the function open in another tab.

In order to explain the process, I am describing it as a series of tasks. This maps well to the approach used by nMod.


---


## Task One: Initialisation ##

We start at the beginning of the step by obtaining all the gravitational interaction between the particles. This represents the starting velocities for midpoint integration.

![http://nmod.googlecode.com/svn/trunk/images/midpoint_1.png](http://nmod.googlecode.com/svn/trunk/images/midpoint_1.png)

## Task Two: Advancing to the Mid Point ##

Using the initial velocities, we advance the particles in the model to half way through the required integration step. For example, given a step size of 60 seconds, we would advance the particles using this initial velocity for 30 seconds.

We then recalculate the gravitational interactions between the particles in the model, convert them to new velocities for each particle, and  store them for later use.

![http://nmod.googlecode.com/svn/trunk/images/midpoint_2.png](http://nmod.googlecode.com/svn/trunk/images/midpoint_2.png)


## Task Three: Moving across the entire interval ##

We return to the starting position again, discarding the positions obtained during Task two, but retaining the revised velocities calculated at the mid point.

![http://nmod.googlecode.com/svn/trunk/images/midpoint_1.png](http://nmod.googlecode.com/svn/trunk/images/midpoint_1.png)

Using these revised velocities, we now advance the particles through the entire step.

![http://nmod.googlecode.com/svn/trunk/images/midpoint_3.png](http://nmod.googlecode.com/svn/trunk/images/midpoint_3.png)

## Summary ##

After completing these three tasks, the particles have been moved using an average of the change in gravitational forces they encounter as they move through the step.

If we were to just use the gravitational forces found at the beginning of the step, the motion across the step would become Euler's Method, which is First Order and thus unsuitable for this task, as error would accumulate rapidly. The Midpoint method, and all other integration techniques, rely on Euler's Method as a component, but alone it is far too inaccurate.

The Midpoint Integration method provides reasonable accuracy, and should be considered when a reduction in particle position/velocity is acceptable in return for a reduced experiment run time.

There are time when the Midpoint Integration method isn't quite good enough, so for that nMod has another method available: Runge-Kutta Fourth Order Integration. We'll cover that next.

[The Integration Methods in nMod: Part 3](http://code.google.com/p/nmod/wiki/int3)


---


**Links**

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com