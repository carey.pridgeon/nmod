![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small_kitteh.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small_kitteh.png)


# The Integration Methods in nMod: Part 3 #
**_The Runge-Kutta Fourth Order Integration Method_**

---


The Runge-Kutta Fourth Order (RK4) Integration function is the most accurate integration method provided in nMod's n-Body model. This accuracy does come at the price of an additional computational burden when compared to the simpler Midpoint method, but it is better suited for experiments where accuracy is of greater importance than speed.

If you are interested in the actual implementation of RK4 in the nMod n-Body model, then you may want to have the function [move\_particles\_RK4](http://code.google.com/p/nmod/source/browse/trunk/release/nmod/_move_particles_RK4.c) open in another tab while you work through this section. It is not required however, so if you're unfamiliar with C code you needn't bother.

Once again I have broken down the process into a series of tasks that must be completed in order to complete the integration. These once more map well to the implementation in nMod.


---


## Task One: Initialisation ##

We start at the beginning of the step by obtaining all the gravitational interaction between the particles. This represents the starting velocities for RK4 integration.

We obtain the gravitational effects, convert them to velocity changes, and store the revised particle velocities found at this point as **V1**

![http://nmod.googlecode.com/svn/trunk/images/Rung_kutta_4th_1.png](http://nmod.googlecode.com/svn/trunk/images/Rung_kutta_4th_1.png)

## Task Two: Advancing to the First Mid Point ##

Using the velocities obtained in task one, we advance to a point mid way between the total step size. Thus for a total step size of 30 seconds, we would advance for 15 seconds, using the velocities calculated in task one.


Once at this mid point, We again obtain the gravitational effects, convert them to velocity changes, and store the revised particle velocities found at this point as **V2**

![http://nmod.googlecode.com/svn/trunk/images/Rung_kutta_4th_2.png](http://nmod.googlecode.com/svn/trunk/images/Rung_kutta_4th_2.png)

## Task Three: Returning to the Starting Position ##

We now discard the new positional data from task two, and return to the starting position. This being the positions occupied by all particles in the model during task one.

![http://nmod.googlecode.com/svn/trunk/images/Rung_kutta_4th_1.png](http://nmod.googlecode.com/svn/trunk/images/Rung_kutta_4th_1.png)

## Task Three: Advancing to the second Mid Point ##

We initialise the particles in the model with the velocities found for them in task 2, and stored in **V2**, then advance them once again until half the time of the step has elapsed.

Once at this new mid point, we again obtain the revised gravitational effects, find the revised velocities for all particles in the model, and store these in **V3**

![http://nmod.googlecode.com/svn/trunk/images/Rung_kutta_4th_3.png](http://nmod.googlecode.com/svn/trunk/images/Rung_kutta_4th_3.png)

## Task Four: Returning to the Starting Position ##

Again we discard the current positional information for all particles, and return them to the positions they occupied in task one.

![http://nmod.googlecode.com/svn/trunk/images/Rung_kutta_4th_1.png](http://nmod.googlecode.com/svn/trunk/images/Rung_kutta_4th_1.png)

## Task Five: Advancing across the whole interval ##

We now initialise the particles in the model with the velocities stored in **V3**, and advance them across the entire step.

Once there, we again obtain the revised gravitational effects, find the revised velocities for all particles in the model, and store these in **V4**

![http://nmod.googlecode.com/svn/trunk/images/Rung_kutta_4th_4.png](http://nmod.googlecode.com/svn/trunk/images/Rung_kutta_4th_4.png)

## Task Six: Returning to the Starting Position ##

For the final time, we discard the current positional information for all particles, and return them to the positions they occupied in task one.

![http://nmod.googlecode.com/svn/trunk/images/Rung_kutta_4th_1.png](http://nmod.googlecode.com/svn/trunk/images/Rung_kutta_4th_1.png)

## Task Seven: Finding the Final Position ##

Here we combine the velocities stored in **V1**, **V2**, **V3** and **V4** in order to discover the smooth curve that will allow us to find the final positions of all particles in the model.

Taking the original positions of all particle in the model as **YN**, so the revised positions at the end of the step are **YN+1**, we have

**YN+1 = YN + V1/6 + V2/3 + V3/3 + V4/6**

Which gives us the desired new position and velocity data at the end of the step.

![http://nmod.googlecode.com/svn/trunk/images/Rung_kutta_4th_5.png](http://nmod.googlecode.com/svn/trunk/images/Rung_kutta_4th_5.png)

## Summary ##

As you have seen, the RK4 method is a lot more involved than Midpoint integration. This additional complexity increases accuracy, but the greater amount of work means it is slower in comparison.

This concludes the Integration section. You might want to return to the [User Manual](http://code.google.com/p/nmod/wiki/Introduction).




---


**Links**

[The Integration Methods in nMod: Part 2](http://code.google.com/p/nmod/wiki/int2)

[The Integration Methods in nMod: Part 1](http://code.google.com/p/nmod/wiki/int1)

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com