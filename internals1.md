﻿![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# The nMod n-Body Model Technical details: Part 1 #

**_Calculating Motion: How the nMod n-Body model works_**


---


## Calculating Motion ##

We will start this tutorial by looking at how the nMod n-Body model causes the particles in the model to move in response to gravitational forces. We will leave such things as preparing a project, loading it into the model and writing the time series output until later, when you are comfortable with the internal operation of the n-Body model.

For each stage in the process I will first describe the action, then name the actual function that performs that action. In this way you will be able to relate what is said here directly to the code.

Note that this is not an exhaustive guide to the model, I will describe operations, but not the minutiae of implementation. This guided tour is an overview which will help those who wish to investigate further on their own. Making it too low level would mean it might be invalidated each time I update a function, and that would be problematic. If you are interested in the exact implementation, open and read the source files while following this tutorial.


## Task One: Finding the Pairs ##

After the particles have been loaded into the n-Body model, we need to obtain the list of unique pairings (as was was covered in [lesson 3](http://code.google.com/p/nmod/wiki/basics3) of n-Body modelling basics).

Actually, the particles themselves don't need to get involved here, since it turns out this is a pretty simple task. What we require is a set of paired indexes into the array of particles, so all we need to know in order to begin is how many particles there are.

The required set of unique pairs is obtained by nesting one for loop which is counting back  **_i_** from the number of particles-1 to 0, inside another for loop that is likewise counting down **_j_** from the number of particles-1 to 0, and recording each pair **_(i,j)_**. Provided we avoid self references (such as the pairs **(0,0)**, **(1,1)** and so on, we can generate the required list of unique pairs quickly.

This task is performed by the function [make\_set\_of\_particle\_pairs](http://code.google.com/p/nmod/source/browse/trunk/release/nmod/_make_set_of_particle_pairs.c).

## Task Two: Starting the Model ##

At this point the particles are ready to go, so we launch our driver function (I introduced the driver function in [lesson 3](http://code.google.com/p/nmod/wiki/basics3) of n-Body modelling basics). This function controls the n-Body model itself, starting and stopping it, counting how many steps have taken place, and doing any other operations on the data which are required. As a result the driver can actually be quite a complex function. In nMod's n-Body model, the driver function is called [run\_n-Body\_environment](http://code.google.com/p/nmod/source/browse/trunk/release/nmod/run_n-Body_environment.c).

As I have said before, an n-Body model, or at least the Particle-Particle n-Body model, advances just one step at a time. the job of the driver is to feed to output of the n-Body model back into it continuously until the required number of steps has been completed.  However at this time we are considering just a single run through all the functions during part of a step. This is sufficient to detail each required operation.

## Task Three: Selecting the Integration Method ##

The driver, having launched the n-Body model, now selects from one of two available integration schemes. These are Midpoint Integration (as provided by the function [move\_particles\_MP](http://code.google.com/p/nmod/source/browse/trunk/release/nmod/_move_particles_MP.c) and Runge-Kutta Fourth Order Integration [move\_particles\_RK4](http://code.google.com/p/nmod/source/browse/trunk/release/nmod/_move_particles_RK4.c). Of these, Midpoint Integration is the faster, but as a result is less accurate. Runge-Kutta is far more accurate, but computationally expensive.

Both integration schemes are interchangeable within nMod. You could, if you liked, do one step with Midpoint, then the next with Runge-Kutta, and back to Midpoint. A more likely scenario is switching between the two methods according to some event in the system such as a spacecraft or asteroid getting within a certain distance of another object. That's why the selection of which to apply is performed within the driver function.

We will be covering how each integration method works later, since each is a subject which requires its own tutorial.

## Task Four: Finding the Gravitational Effects ##

The first thing either move\_particles function needs to do is establish the current forces acting between the particles in the model. The function which oversees this process is [convert\_gravitational\_effects\_into\_velocity](http://code.google.com/p/nmod/source/browse/trunk/release/nmod/_convert_gravitational_effects_into_velocity.c).

This function performs three operations. First it blacks all the temporary buffers used to accumulate gravitational forces by calling [clear\_total\_gravitational\_force\_accumulators](http://code.google.com/p/nmod/source/browse/trunk/release/nmod/_clear_total_gravitational_force_accumulators.c).

Then, having prepared the data structures, it starts the process of obtaining the gravitational forces by handing the problem over to the function [calculate\_forces\_acting\_between\_all\_particles](http://code.google.com/p/nmod/source/browse/trunk/release/nmod/_calculate_forces_acting_between_all_particles.c)

## Task Five: Application of Gravitational Forces ##

The function [calculate\_forces\_acting\_between\_all\_particles](http://code.google.com/p/nmod/source/browse/trunk/release/nmod/_calculate_forces_acting_between_all_particles.c) is where we use the list of unique pairing obtained in Task One. These pairs are sent, one at a time to [find\_gravitational\_interaction\_between\_pair\_of\_particles](http://code.google.com/p/nmod/source/browse/trunk/release/nmod/_find_gravitational_interaction_between_pair_of_particles.c) for each pairing in the list.

[find\_gravitational\_interaction\_between\_pair\_of\_particles](http://code.google.com/p/nmod/source/browse/trunk/release/nmod/_find_gravitational_interaction_between_pair_of_particles.c) is the function that uses Newtons equation of gravitation to discover the amount of gravitational influence that exists between the two particles. This is then applied to each particle in such a way that the force will result in the two particles moving towards each other (see [lesson 1](http://code.google.com/p/nmod/wiki/basics1) of n-Body modelling basics to recap this). This application is in the form of either adding, or subtracting the required amount of force.

Finding the gravitational influence is not the final step though, we need to know the magnitude of the change in velocity for each particle. For this control is passed back to convert\_gravitational\_effects\_into\_velocity.

convert\_gravitational\_effects\_into\_velocity now calls the final function in the process of converting the gravitational influences within the system of particles into motion. This is the function [find\_meters\_per\_second\_velocity](http://code.google.com/p/nmod/source/browse/trunk/release/nmod/_find_meters_per_second_velocity.c).

All find\_meters\_per\_second\_velocity does is call, for each particle in the model, the function [account\_for\_inertia](http://code.google.com/p/nmod/source/browse/trunk/release/nmod/_account_for_inertia.c). The reason I use a function to perform such a simple task is that it makes the code simpler to read, and thus, simpler to edit. Never underestimate the usefulness of a simpler layout, even if it results in a few more lines of code.

This function works out the summed forces acting on a particle (remember the particle has had its gravitational interaction with every other particle in the model calculated by this time). It then works out the inertia of the particle, and applies the force, taking this inertia into account. The result is the required change in velocity for each particle.

## Task Six: Moving the Particles ##

Now that the revised velocities for all particles are known, convert\_gravitational\_effects\_into\_velocity exits, returning control to the move\_particles function in use.

The system of particles is then iterated for a specified number of seconds using the velocity found, resulting in a revised model state, ready to have gravitational influences calculated anew. The job of turning this basic scheme into one which provides the required positional accuracy is performed by the integrator selected.

## Summary ##

We've now covered a single pass through the n-Body model. We haven't covered a complete step, because to do so requires more detail on the integration methods in use. A complete step involves a repetition of the process described above, along with some integration scheme specific manipulations of the results. We'll cover that in a later tutorial.

For now though, it's time to cover getting particle data loaded into the model.

[Technical Details, Part Two](http://code.google.com/p/nmod/wiki/internals2)


---


**Links**

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com