![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# The nMod n-Body Model Technical details: Part 2 #

**_Preparation and Loading of Projects into nMod: the nmp file_**

---


# The NMP File #

Nmp files are project files for nMod. The nmp file is plain text, but has an nmp file extension simply to differentiate the nmp files from other text files. Thus you can edit an nmp file in any standard text editor.

Nmp files have are divided into two sections. The first is concerned with n-Body model initialisation, the second with providing the data for the particles that are to be loaded into the n-Body model.

While the nmp file is being explained, we'll be covering some of the conventions used by nMod, so it's a good idea to read this, or you might not understand something important later.

Nmp files are read into nMod's n-Body model via the function [load\_project\_file\_into\_nmod](http://code.google.com/p/nmod/source/browse/trunk/release/nmod/load_project_file_into_nmod.c), which itself calls the worker function [read\_single\_particle](http://code.google.com/p/nmod/source/browse/trunk/release/nmod/read_single_particle.c). If you are intending to start re-coding nMod soon, it might be helpful to have those open while you read this section, but its not at all required if you're just trying to understand how nMod's n-Body model works.


---

## NMP file n-Body Model Initialisation Data ##

The first section of the nmp file is where you provide the information that nMod needs  in order to perform the requested experiment.

Some entries in the nmp file are essential, other specific to the reference build. These will be labelled as **required** or **build specific**. You might find that if you use nMod to create your own n-Body model, many of the build specific variables are not required.

nMod's file parsing code is rather simple. It allows, for each entry, two descriptive words and one data item, for example:

**_state interval 86400_**

Ok then, lets go over the contents of a typical nmp file

### The number of particles in this project (required) ###

This is a simple integer that tells nMod how many particles are in the project file.

**_particle population 29_**

### Setting the number of states, steps, and the step size (required) ###

A _state_ is a term used by nMod to mean 'The condition of the particles in the model after a fixed period of time has elapsed'. For example, an nmp file set up to get the n-Body model to simulate 100 days months of planatery motion might have 100 states specified. The resulting time series record would contain 100 snapshots of particle positions, one each from the 100 states recorded.

A state consists of a set number of steps, with steps in turn consisting of a set number of seconds of integration. Therefore all three entries, states, steps, and step size must be set to reproduce the correct time period.

Here they have been set to reproduce 5000 24 hour days of planetary motion, with a single step consisting of sixty seconds.

**_states required 5000_**

**_step count 1440_**

**_step size 60_**


### Specifying the integration method to be used (build specific) ###

Here we tell nMod which integration scheme to be used, Runge Kutta or Midpoint. This is specific to the reference build because nMod is designed to allow for the integration scheme to be swapped during an experiment. The code to perform such a swap would have to be written by the experimentor in question for the moment, though some additional code to assist in this will be available in the next stable build of nMod.

nMod has two possible integration schemes at present.

To Choose Runge Kutta fourth order integration you use:

**_integration method RK4_**

To use the simpler (and faster) Midpoint integration, you use:

**_integration method Midpoint_**

(In fact the 'p' in midpoint is capitalised, but that gets messed up in the wiki page)


### The Gravitational Constant used by nMod (required) ###

The measure for the gravitational constant (G) is intermittently improved through research. In fact while I was developing nMod initially there were three competing measures, which baffled me somewhat. When selecting from among the competing measures, you should be guided by the measure which appears to result in the most accurate reproduction fof the required orbits within the n-Body model.

This measure is left as a value that must be provided because there may be improvements to the value of G, and this approach means trying a new measure out is trivial. I strongly recommend you do not hard code the value for G into nMod.

**_gravitational constant 6.674040E-11_**

### Setting the Collapsor (build specific) ###

We covered the reason for, and usefulness of, collapsors previously (see [lesson 2](http://code.google.com/p/nmod/wiki/basics2) of n-Body modelling basics).  nMod allows for one fixed collapsor in the reference build. This is either for Solar System Modelling, or simply for some point of trivial mass which we can use as a fixed point of reference later when playing back the time series output of the n-Body model.
To set a collapsor, you provide the index of the collapsor (remember that arrays in C start at 0, so the first particle has the index 0, not 1).

If you decide you don't want a collapsor, set the collapsor to array index -1. Since that is an invalid index, the reference build will allow all particles to move.

In the reference builds default project we are setting  Sol (particle 0) as the collapsor.

**_collapsor defined 0_**

### How many items of information exist for each particle (required) ###

This value refers to the number of individual elements of data that a single particle contains. This is important, because this number is written to the time series result file, and used when the time series file (called an nbo file, we'll get to that next) is played back in the viewer.  If you want to record more information on a particle in the time series output, you will need to change this value _and_ alter the output file format, so it's non trivial.

**_particle info\_size 14_**

### The name for the file that the time series data from an experiment will be written to (build specific) ###

This must be a single string with no gaps, so use underscores as spaces. We include the nbo file extension here. In fact there's no reason to use the nbo file extension other than to make these result files stand out.

The reference build needs this name to be specified here, but you could equally well specify it at runtime if you edited nMod. The next stable version of nMod will make setting and changing the nbo filename much simpler.

**_output filename time\_series.nbo_**

### Can nMod write to stdout? (build specific) ###

This option allows you to prevent nMod from writing any output to the console window. This is useful if you are using screen, or nohup, or you wish to do without the computational expense of calls to printf.

This entry accepts one of two values TRUE or FALSE (caps required). By default, nmod is allowed to access stdout.

**_use stdout TRUE_**

### Creation of a new project file, based on the final state of an experiment (build specific) ###

It can be useful to have a new nmp file which represents the point at which your original nmp file left off. This feature can be accessed by setting the following entry to either TRUE or FALSE.

**_write project TRUE_**

And if you do want a new project file to be generated, you provide its filename here, minus the nmp extension:

**_final\_conditions filename final\_state\_conditions_**

Note however that this option does not write a complete nmp file. There are too many possible variations of nmp initialization data to make this worthwhile at present. Therefore the number of particles, and the current information for each particle is recorded. All other information needs to be added manually.


---

## NMP file particle data ##

In the nmp file, this section begins with the string 

&lt;particles&gt;

. That tells read\_project that it's time to stop looking for initialisation data, and time to start loading the particles in.

Particle data is pretty straightforward. Again we use the convention of having two descriptive words, followed by the data item.

All particles use the same format, so we just have to look at one, Venus.

The first entry is the identifier of the particle. This is _venus_ in this case, but it can be any combination of letter,s numbers and underscores or other characters. Provided the end result can be printed to screen as a valid single string with no spaces, there's no problem.

**_particle ident Venus_**

Now we have to tell the n-Body model what class our particle is. Actually the n-Body model  doesn't use this information itself. What it does is pass the information on through to the viewer application, where it is used to choose which if a set number of particle sizes is to be applied.

The list of available particle sizes are as follows:

  * massive\_3 - Stellar Mass objects
  * massive\_2 - Gas Giant: Jupiter size
  * massive\_1 - Gas Giant: Smaller than Jupiter
  * major\_2 - Large planet: Earth,Venus,Mars
  * major\_1 - Small planet: Mercury
  * minor\_2 - Planetesimals: Our Moon
  * minor\_1 - Very small objects: Asteroid,Comet, Spaceship


**_particle type major\_1_**

We also need to know the colour that the viewer is to use to render the particle. Once again this information is passed on without alteration to the viewer, but the colour of a particle can be altered to indicate some change. Doing this requires alteration of the reference build.

**_particle red 230_**

**_particle green 189_**

**_particle blue 123_**


We now have a piece of free text that is to be passed to the viewer unchanged. This is unused by either the n-Body model or the viewer in the reference build. It is included solely as an additional means to pass data from the n-Body model to the viewer about a particle in custom experiments.

**_free text empty_**

Next we have the data about the particle that the n-Body model requires itself.

First, the mass.

**_particle mass 48.685E+23_**

Next, the radius of the particle. Actually the n-Body model doesn't use this in the reference build, but it can if re-coded. This would be essential for checking the validity of spacecraft orbits.

**_particle radius 6.0518E+06_**

Next is the X Y Z position of the particle in the World Co-ordinate System. Position data is provided in metres.

**_X position -3.492492560045657E+10_**

**_Y position 1.020150650723204E+011_**

**_Z position 3.413644752166130E+09_**


Lastly, we have the X Y Z velocity of the particle. This is a measure in metres per second.


**_X velocity -3.319646094915657E+04_**

**_Y velocity -1.171273191124313E+04_**

**_Z velocity 1.755210603793870E+03_**



---


Now we move onto describing the output file format.

[Technical Details, Part Three](http://code.google.com/p/nmod/wiki/internals3)


---


**Links**

[Technical Details, Part One](http://code.google.com/p/nmod/wiki/internals1)

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com