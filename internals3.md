![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# The nMod n-Body Model Technical details: Part 3 #
**_Recording of n-Body time series data: the nbo file_**

---

The NBO file format is used to record the time series output of projects which have been run in the n-Body model. This file is then played back in the nMod Toolkit viewer nBview, which we are covering soon.

Unlike the nmp file format, the nbo file is not human readable without processing. There are a number of utilities for editing nbo files which we will cover later on.

This description is divided into two sections. The first describes how nMod's n-Body model creates and writes to, the nbo file, and the second details the format of an nbo file.

Those of you who are working through the tutorial to understand how nMod works need only read the first part of this page, as the second is provided solely for those people who wish to make changes to the nbo file format.


---


## How nbo files are created ##

To  initialise an nbo file, the function [initialise\_nbo\_file](http://code.google.com/p/nmod/source/browse/trunk/release/nmod/initialise_nbo_file.c) is called, which creates the nbo file and writes the preamble to it. The only parameter it requires is the project data structure. This data structure contains everything that was loaded from the nmp file, and we'll be covering that later.

Once the n-Body models driver function has been called, all we need to do is dump the current state of the particles in the model to the nbo file whenever requested to do so by the driver function. To do this we make a call to the [write\_current\_state](http://code.google.com/p/nmod/source/browse/trunk/release/nmod/write_current_state.c) function.

The nbo file remains open during the run of the n-Body model, and is closed by the driver once the specified number of states has been reached.

Because the number of states that will be recorded in the nbo file is written to the file when it is initialised, halting the n-Body model before the specified number of states has elapsed will break the nbo file. There is now a utility to solve this problem, called nbofix, which is included in the toolkit.


---


## The NBO file format in greater detail ##

### 1: The Preamble ###
the nbo file format begins with a preamble section. This section lists the total number of states in the nbo file, the number of particles, and the state interval used. It also records how many items of data to expect per particle.

The preamble begins with the character ‘#’, then separates the values it contains with ‘:’, before closing the preamble with another ‘#’

Here then we have a preamble for an nMod output file with 100 states recorded,  19 particles in the system, with a state interval of 86400 seconds, and 14 elements present in the data for a single particle.

**#100:19:86400:14#**

Records of individual states are enclosed in square brackets ‘[‘ ‘]’. Each state has its own minor preamble, with entries separated by a bang ‘!’. Here then is a state preamble for the first state recorded in a time series

**[s1!19!.......]**

This means 'State 1, 19 Particles'. The number of particles is restated because it can reduce as the model progresses. Not increase yet alas, I haven't got that far with the viewer.

Within state records, there are the data for the individual particles. All individual particle records are encloses by brackets ‘(’ ‘)’

**[s2!19!( data for particle1)(data for particle 2)(…)]**

If you look at the function that parses all this you may wonder why I bothered, since all of the separators are just stripped out when the file is read. The answer is that I'll  be adding more capabilities to nbview, such as streaming, because larger nbo files eat memory like it was lemon cake (mmmm, cake). The separators are also required for  nbo file editing utilities.

The format for a single particle within a state is as follows:

each element separated by bar ‘|’:
1.	Particle plain text identifier

2.	Particle Class

3.	Particle Mass

4.	Particle Radius

5.	Particle Red Green and Blue display color values (for viewer)

6.	A free text field (single word for the moment)

7.	Position on the X Axis

8.	Position on the Y Axis

9.	Position on the Z Axis

10.	Velocity on the X Axis

11.	Velocity on the Y Axis

12.	Velocity on the Z Axis

with each element of particle data separated by a bar '|'. The only exception being the RGB values, which are all within the same two bars, and comma separated, e.g. |23,45,67|

This file format isn’t meant to be easily human readable, and thus is a departure from previous versions. However human readability had to be abandoned in favour of decreased file size.


---


Now for the last tutorial in this series about the internal working of the nMod n-Body model.

[Technical Details, Part Four](http://code.google.com/p/nmod/wiki/internals4)

---


**Links**

[Technical Details, Part Two](http://code.google.com/p/nmod/wiki/internals2)

[Technical Details, Part One](http://code.google.com/p/nmod/wiki/internals1)

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com