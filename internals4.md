![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# The nMod n-Body Model Technical details: Part 4 #

**_Additional Properties of the nMod n-Body Model_**


---


the n-Body Model implemented in the nMod toolkit, as you should now know. uses a Particle Particle method to model the interactions of particles. Hopefully by this stage you have a reasonable grasp of how the nMod n-Body mode works.

The nMod n-Body Model has another couple of properties that I haven't mentioned yet, but which are also quite important, so we'll cover those now.

---

## The nMod n-Body Model is Deterministic ##

Deterministic means that the same starting conditions for any system of particles fed into the n-Body model will always produce the same result, unless some random element is introduced, no matter how many times you re-run the model.

This may not seem that important, but if you are using nMod as a means to evolve, for example, an orbit for a spacecraft, you can be confident that the only source of variation between runs of the model are due to the variations you introduce. This statement will be revisited when the tutorials on using the nMod Toolkit to perform experiments are completed.



---

## The nMod n-Body Model is a Collisionless system ##

Unless you explicitly add in code to check for collisions, the n-Body model will ignore them. This is the case with the reference build of the n-Body model.

This lack of collision detection/handling in the reference build is deliberate. If the n-Body model were to check for collisions all the time, the runtime for a given time series would be greatly increased. Collisions are quite rare, especially when you are modelling a structure like the Solar System, or even the Globular cluster collision scenario we covered earlier.

Collisions are useful if you are wanting particles to accrete into larger bodies. nMod does not have this capability, so this would be an addition you would need to make yourself, at least for the moment.

The other time when collisions would be useful is when you are using the n-Body model to evolve flight plans for spacecraft. Any spacecraft that passed within the recorded radius of another particle in the system during its speculative orbit would most likely be following an invalid orbit.

That is, it would be invalid unless the collision was intended (you might be specifically trying for an impact event), because spacecraft should not as a rule fly through solid objects on their way somewhere. The experimentation version of the nMod Toolkit, nMod-ea, has this functionality included.



---


**Links**

[Technical Details, Part Three](http://code.google.com/p/nmod/wiki/internals3)

[Technical Details, Part Two](http://code.google.com/p/nmod/wiki/internals2)

[Technical Details, Part One](http://code.google.com/p/nmod/wiki/internals1)

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com