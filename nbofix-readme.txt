nbofix 1.0

     if you have an NBO file from an incomplete run, as might be the case if nmod crashed during an experiment, this utility will 
     write a new, valid NBO file from the damaged one.
     The recovered nbo file is written to stdout, unless redirected to a file
     The original damaged file is left intact   
     usage
     Linux
     ./nbofix nbo_file.nbo > [repaired_filename.nbo]
     Windows
     nbofix nbo_file.nbo > [repaired_filename.nbo]
