nbojoin 1.0

     This utility will merge together two nbo files.
     
     This would be for use when you need to present a series of experiments together, such as showing 
     an optimisation process in action.
          
     The merged nbo file is written to stdout unless redirected to a file
     
     usage
     
     Linux:
     
     ./nbojoin nbo_file1.nbo nbo_file2.nbo > [output filename]
     
     Windows:

     nbojoin nbo_file1.nbo nbo_file2.nbo > [output filename]
     
