nbo2txt

This utility takes an nbo file and extracts a portion of it, writing
the result as a new nbo file to stdout, unless redirected to a file

Usage
-----
 
Linux: 
./nbosplit <nbo_file_to_split> start_state _end_state 

Windows: 
nbosplit <nbo_file_to_split> start_state _end_state 

Indexing starts at 1, not 0, so to start at the first state in the file, start state should be 1.