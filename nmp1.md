![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# Creating new NMP files #
**_Using the JPL Horizons database to build models of the real Solar System_**

---


The [JPL Horizons web site](http://ssd.jpl.nasa.gov/?horizons) is an on-line solar system data and ephemeris computation service. Through its web interface you can obtain data on many objects in the Solar System. In the context of nMod it is  invaluable for producing project files which reflect the motion of bodies in the real Solar system.

It would help when reading this if you also have the [HORIZONS](http://ssd.jpl.nasa.gov/horizons.cgi) web interface open in another tab/window, since that's what I am going to use here. There are also telnet and email interfaces to the horizons service, but we will stick to the web interface, since its the easiest to explain.

I've found that the web interface can be a bit unreliable in Firefox, so I'd suggest you use IE for now.


---


In this tutorial we will be adding the planetesimal Pluto to the solar system model provided with the nMod toolkit (default.nmp).

The web interface has a number of options you have to set

**Ephemeris Type**

**Target Body**

**Observer Location**

**Time Span**

**Table Settings**

**Display/Output**

Exactly how you do this I will leave to the tutorial on the Horizons site, because they have some very good ones. What I will cover are the settings you need for nmod.

### Ephemeris Type ###

Set this to **VECTORS**

### Target Body ###

by using the search function, find Pluto in their list and add it.

### Observer Location ###

Set this to **Solar System Barycenter** (@0 in the search box)

### Time Span ###

Set this to **discrete output times form** and choose the date you want If you want to keep the position of Pluto consistent with the rest of the default project, use the date it lists.

### Table Settings ###

This is a tad more complex. You need to open the table setting page and set quite a few things.

Output units : set to 'km & km/sec'

quantities code: state vector {x,y,z,vx,vy,vz}

labels : -- enable labelling of each vector component

CSV format : -- output data in Comma-Separated-Variables (CSV) format

### Display/Output ###

set this to html, unless you want a text file, it's up to you.


### How to convert the data ###

The part we are interested in is between the labels **$$SOE** and **$$EOE**

$$SOE

2454214.500000000, A.D. 2007-Apr-24 00:00:00.0000, -2.268943146888790E+08, -4.638380283374466E+09,  5.619670388509338E+08,  5.545686514294367E+00, -1.261288041409800E+00, -1.474365754204501E+00,

$$EOE


The first two values _2454214.500000000, A.D. 2007-Apr-24 00:00:00.0000_ we can ignore, we don't need them.

Note that I selected labels, but the values aren't labelled. This happens sometimes, it's not a problem mind.

After disregarding the first two values we have:

-2.268943146888790E+08, -4.638380283374466E+09,  5.619670388509338E+08,  5.545686514294367E+00, -1.261288041409800E+00, -1.474365754204501E+00,

The first three are X Y and Z location in kilometres

**X** -2.268943146888790E+08

**Y** -4.638380283374466E+09

**Z** 5.619670388509338E+08


The final three are velocity on X Y and Z in kilometres per second

**VX** 5.545686514294367E+00

**VY** -1.261288041409800E+00

**VZ** -1.474365754204501E+00


These need to be converted to meters for the position values, and meters per second for the velocity. This is a simple matter of multiplying each value by 1000.

The result in this case is

**X** -2.268943146888790E+11

**Y** -4.638380283374466E+12

**Z** 5.619670388509338E+11

**VX** 5.545686514294367E+03

**VY** -1.261288041409800E+03

**VZ** -1.474365754204501E+03


### Other information you need ###

You also need a Mass and Radius Value. As it happens this ephemera set has them, but not all do

PHYSICAL DATA (updated 2006-Feb-27):

Mass Pluto (10<sup>22</sup> kg) =   1.314+-0.018

Radius of Pluto, Rp   = 1151 km

The radius needs also to be converted to meters.

The Mass would be re-written as 1.314E+22 Ignore the +- value for now, it won't make much of a difference with Pluto.  Ok yes, it's a pretty big deal for closer/smaller objects, but not in this case, and I haven't yet had the time to solve this problem. That is by way of being on my list..

There are plenty of sources on the internet for Mass and Radius of astronomical bodies. Wikipedia has a lot of useful data.

Using the above data, the final form for the Pluto particle is as follows:

**particle ident Pluto**

**particle type major\_1**

**particle red 230**

**particle green 189**

**particle blue 123**

**free text empty**

**particle mass 1.314E+22**

**particle radius 1.1510E+06**

**X position -2.268943146888790E+11**

**Y position -4.638380283374466E+12**

**Z position 5.619670388509338E+11**

**X velocity 5.545686514294367E+03**

**Y velocity -1.261288041409800E+03**

**Z velocity -1.474365754204501E+03**

This would be added to the list of particles in the nmp file, and the number of particles recorded would then need to be increased by one.

Now we move to generation of clusters of particles for use with nMod

[Creating new NMP files: Part 2](http://code.google.com/p/nmod/wiki/nmp2)


---

## Finding Ephemera for objects with short periods ##

Sometimes you might want to obtain the Ephemera for an object with a very small orbital Period. Examples might be the Hubble Telescope, or the moons of Mars.

In this case you need to change your time span from **discrete** to **time-span form**. Now you can select a start and stop date, and specify whether the Ephemera are to be reported for every minute, hour or day.

This can be useful for checking that the orbit remains correct over time.

I'm currently working on a version of the n-Body model hat can use the table of positions and velocities thus generated to refine an orbit. This is required because not all real world Ephemera result in a stable orbit within the model.

---



**Links**

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com