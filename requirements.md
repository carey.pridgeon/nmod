![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# Hardware and Software requirements of the nMod Toolkit #

---


## Hardware ##

A PC with the following is pretty much the minimum spec for running the n-Body model and n-Body model output viewer:
  * A Processor with a speed of at least 2Ghz
  * One Gb of Ram
  * An Opengl capable graphics card.

Anything at or above this specification will be able to run everything in the toolkit well.

Multiple cores will not make the nMod toolkit run faster, although they would allow you to run more than one instance of the n-Body model, either as seperate processes, or as seperate threads (in a custom build).

You should be aware that nMod is not like Celestia and other software of that kind, it is a research and experimentation tool. It can, and in the reference build, does reproduce the solar system, but it does so via a very precise simulation of a Newtonian gravitational system. The orbits observed in the example nbo file I provide for download are not pre-calculated, they have been derived step by step from the starting conditions provided.

This is _extremely_ processor intensive work, so don't expect to be producing galaxy simulations, or solar system time series in a few minutes. Run times for nmod to generate the time series files that can be viewed in 3d can be anything from minutes for small projects, to hours or days for more complex ones.


---

## Software ##

### Operating systems ###

Any flavour of Windows or Linux should work.

I don't have a natively compiled version for Vista yet, but my XP compiled version (as in the windows reference build), works well on Vista.

### Libraries ###

You need glut installed to be able to run the opengl viewer. Windows versions of those are provided in the installer, linux users need to install freeglut.

Compilation of nbview will require the development versions of glut for either platform. The windows version is available for download [here](http://code.google.com/p/nmod/wiki/compile2).

Adding threading to the n-Body model requires pthreads. most Linux distributions already have pthreads installed, you may need to install pthreads-devel though.

Windows users who want to add threading need the windows build of pthreads, which is [here](http://sourceware.org/pthreads-win32/).

Note that by add, I mean add yourself. at present the n-Body model provided does not utilise threading, although it is designed to make threading easier. The next version of the nMod toolkit will hopefully include a threading option.


---


**Links**

[nMod Toolkit Homepage](http://code.google.com/p/nmod/)


[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


![http://code.google.com/p/nmod/source/browse/images/Rung_kutta_4th_1.png](http://code.google.com/p/nmod/source/browse/images/Rung_kutta_4th_1.png)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com