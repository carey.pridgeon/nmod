![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)

# Scratchpad page #


---


# Parallel RK4 Integrator design speculations #


## Speculative method 1 ##

RK4, or any other n-body integration scheme, works on each Axis independantly. Therefore it is possible to split the integrator into three parts, one each for X Y and Z, and have those handled by threads.

Using this aproach there is an issue of thread creation/destruction overhead that I want to avoid if possible. Since the new nMod still integrates for single steps only, there will be numStates\*numSteps\*3 thread creation and destruction events. Fewer than in speculative method 2 below, but still excessive. Simple thread joins should make sure they all exit before program flow continues.

The solution I favour is persistant worker threads, whereby a thread created at program start waits to be allowed to perform an integration step, does the step, then waits again while whatever external processing required is performed, whereupon it can integrate for another step, and so on.

The problem here is that the method which requests an integration step needs to be able to signal the worker threads that they are safe to start another integration step. Then it must wait for them all to finish the required single step before itself exiting, having also caused the integration worker threads to pause (wait on a mutex), ready for the next time they are called upon.



## Speculative method 2 ##

Within each stage of Runge Kutta there are multiple pairwise calculations, whereby each possible pairing of particles has the gravitational influence calculated.

Such threads would be short lived, and rely on mutexes to prevent issues during the many parallel accumulator updates required.

Advantage: Would be suitable for GPU programming.

Disadvantage: This seems that it will be likely to have a large processing penalty in terms of thread creation/destruction and locks/releases unless a lock free method could be devised. I don't plan to try this any time soon.


---


**Links**

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)

---

Dr Carey Pridgeon 2009 - carey.pridgeon@gmail.com