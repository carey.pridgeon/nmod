![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)

# nMod source code repository page #

The nMod n-Body Modelling Toolkit's source code repository on google project hosting is primarily used for development, not archiving of fully working/completed software.

As a result it is often unlikely that the versions of programs held there are functional.

All releases of the nMod Toolkit and nMod-ea come with the complete source code for those releases, so you would be better off referring to those as a base for further work.


If you wish to browse the SVN repository, you will find it here:

[SVN repository](http://code.google.com/p/nmod/source/browse/)


---

**links**

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com