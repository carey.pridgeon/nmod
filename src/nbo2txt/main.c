/********************************************************************
*
* Function : main
* Date of first writing : 2007
* Author : Carey Pridgeon
* EMail  : carey.pridgeon@gmail.com
* Description: This program converts the content of an nbo file to plain text, 
*              sending the converted data to stdout
* Licence:     GPL 3 .0 or later.
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/  

/* This program converts the content of an nbo file to plain text, sending
* the converted data to stdout
*/

#include "header.h"
int main(int argc, char **argv)
{
    int i, j, k;
    if (argc == 1) {
            printf ("> nbo2txt, the nMod Toolkit nbo to plain text conversio utility.\n");
            printf ("> This utility will re-write nbo files so they can be more easily read.\n");
            printf ("> The result is written to stdout, redirect with > to a new file.\n");
            printf ("> Usage:\n");
            printf ("> nbo2txt <nbo_file.nbo>\n");
            exit (0);
        } 
        loaded_state = read_statefile(argv[1]);

        /* push to stdout */
        printf("Plain Text version of nbo file: %s\n", argv[1]);
        printf("# Preamble information\n");
        printf("Total States %d\n", loaded_state->total_states);
        printf("Total Steps per State %d\n", loaded_state->state_interval);
        printf("State Interval %d\n", loaded_state->state_interval);
        printf("Particle Amount at File Start%d\n", loaded_state->num_particles);
        printf("Particle Data Size %d\n", loaded_state->particle_datasize);
        printf("# End of preamble information, beginning state information\n");
        for (i = 0; i < loaded_state->total_states; i++)
        {
            printf("State %d of %d\n", i, loaded_state->total_states);
            printf("State Particle Amount %d\n", loaded_state->set[i].num_particles);
            for (j = 0; j < loaded_state->set[i].num_particles; j++)
            {
                printf("\tParticle %d of State %d information\n", j, i);
                printf("\t\tParticle Identifier: %s\n", loaded_state
                ->set[i].particles[j].particle_id);

                if (loaded_state->set[i].particles[j].type == MASSIVE_PARTICLE_3)
                {
                    printf("\t\tParticle Type: Massive Particle 3\n");
                }
                if (loaded_state->set[i].particles[j].type == MASSIVE_PARTICLE_2)
                {
                    printf("\t\tParticle Type: Massive Particle 2\n");
                }
                if (loaded_state->set[i].particles[j].type == MASSIVE_PARTICLE_1)
                {
                    printf("\t\tParticle Type: Massive Particle 1\n");
                }
                if (loaded_state->set[i].particles[j].type == MAJOR_PARTICLE_2)
                {
                    printf("\t\tParticle Type: Major Particle 2\n");
                }
                if (loaded_state->set[i].particles[j].type == MAJOR_PARTICLE_1)
                {
                    printf("\t\tParticle Type: Major Particle 1\n");
                }
                if (loaded_state->set[i].particles[j].type == MINOR_PARTICLE_2)
                {
                    printf("\t\tParticle Type: Minor Particle 2\n");
                }
                if (loaded_state->set[i].particles[j].type == MINOR_PARTICLE_1)
                {
                    printf("\t\tParticle Type: Minor Particle 1\n");
                }


                printf("\t\tParticle Mass: %e\n", loaded_state->set[i].particles[j].mass);
                printf("\t\tParticle Radius: %e\n", loaded_state
                ->set[i].particles[j].mass);
                printf("\t\tParticle Free Text Field: %s\n", loaded_state
                ->set[i].particles[j].freetext);
                printf("\t\tParticle Colour Values: Red: %d Green: %d Blue: %d\n",
                loaded_state->set[i].particles[j].rgb_vals.r, loaded_state
                ->set[i].particles[j].rgb_vals.g, loaded_state
                ->set[i].particles[j].rgb_vals.b);
                printf("\t\tParticle X Axis Position: %e\n", loaded_state
                ->set[i].particles[j].xloc);
                printf("\t\tParticle Y Axis Position: %e\n", loaded_state
                ->set[i].particles[j].yloc);
                printf("\t\tParticle Z Axis Position: %e\n", loaded_state
                ->set[i].particles[j].zloc);
                printf("\t\tParticle X Axis Velocity: %e\n", loaded_state
                ->set[i].particles[j].xsp);
                printf("\t\tParticle Y Axis Velocity: %e\n", loaded_state
                ->set[i].particles[j].ysp);
                printf("\t\tParticle Z Axis Velocity: %e\n", loaded_state
                ->set[i].particles[j].zsp);
            }


        }


        return 1;
    }
