nbo2txt

This utility takes an nbo file and converts it to a human readable form.

The human readable file is passed to stdout, so you will need to redirect it to a file if you want to keep it, 
or pipe it to something like 'less' to read it.

Usage:

Linux: 
./nbo2txt yourfile.nbo
or
./nbo2txt yourfile.nbo | more
or
./nbo2txt yourfile.nbo | less
or
./nbo2txt yourfile.nbo > plaintextfile.txt

Windows:
nbo2txt yourfile.nbo
or
nbo2txt yourfile.nbo > plaintextfile.txt

