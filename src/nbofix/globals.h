/*		  Released under GNU GENERAL PUBLIC LICENSE
Version 2, June 1991 -  or later
 */
#ifndef globalsH
  #define globalsH


  /* structure that is used when representing a colour for particles in the viewer*/
  typedef struct
    {
    int r; // red colour
    int g; // green colour
    int b; // guess!
    } particlecolour;
  /* structure that is used when representing a particle in the viewer*/
  typedef struct
    {
    char particle_id[100];
    int type;
    double mass;
    double radius;
    char freetext[100];
    particlecolour rgb_vals;
    double xloc; /*position in the WCS*/
    double yloc;
    double zloc;
    double xsp; /*speed in the WCS*/
    double ysp;
    double zsp;
    } particle_data;
  /* structure that is used when representing a single state in the viewer.
   * Containing an array of particles to be found in that state*/
  typedef struct
    {
    int state_number;
    int num_particles;
    particle_data *particles;
    } single_state;
  //instance of same used for scaling and display
  single_state *current_state;
  typedef struct
    {
    int total_states;
    int state_interval;
    int num_particles;
    int particle_datasize;
    single_state *set;
    } states;
  states *loaded_state;


  FILE *inputfile;

#endif
