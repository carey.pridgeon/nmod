/********************************************************************
 *
 * Function : main
 * Date of first writing : 2007
 * Author : Carey Pridgeon
 * EMail  : carey.pridgeon@gmail.com
 * Description: This program will repair an nbo file that has been broken as a result of nmod being closed prematurely
 * Licence:     GPL 3 .0 or later.
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.

 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ********************************************************************/

 
// broken nbo file recovery program

#include "header.h"
int
main (int argc, char **argv)
{
  FILE *inputfile;
  int a,i, pcount, ppos, char_count, delim_count, final_char_count;
  int ss, es, scount, orig_num;
  char pr_str[1024], pr_v_array[4][1024], inchar;

  if (argc == 1) {
      printf ("> nbofix, the nMod Toolkit broken nbo file repair utility.\n");
      printf ("> This utility will repair nbo files broken by premeture exiting of the nBody model.\n");
      printf ("> Usage:\n");
      printf ("> nbofix <broken_nbo_file.nbo>\n");
      printf ("> The repaired file is written to stdout, redirect with > to a new file.\n");
      exit (0);
    }
    else { //should be that the second perameter is an nbo file
      inputfile = fopen (argv[1], "r");
      if (inputfile == NULL) {
	      fprintf (stderr, "> Failed to open '%s'\n", argv[1]);
	      exit (0);
      }
      // load the preamble and store it for later
      delim_count = char_count = 0;
      for (i = 0; i < 1024; i++) {
	       inchar = consume_char (inputfile);
	       char_count++;
	       if (inchar == '#') {
	           delim_count++;
	       }
	       pr_str[i] = inchar;
	       if (delim_count == 2) {
	           break;
	       }
      }
      // now process the preamble string
      // preamble is a fixed format right now, so extraction is simple
      pcount = ppos = 0;
      for (i = 0; i < char_count; i++) {
	       if (pr_str[i] == '#') {
	           pr_str[i] = ' ';
	       }
      }
      for (i = 0; i < char_count; i++) {	// store the values in the preamble
	       if (pr_str[i] == ':') {
	           pcount++;
	           ppos = 0;
	       }
	       else {
	             pr_v_array[pcount][ppos] = pr_str[i];
	             ppos++;
	      }
      }
      orig_num = atoi(pr_v_array[0]);
      // scan the remainder of the file, counting completed states as we go.
      // These are easy to find, since a state starts with [  and ends with ]
      ss = es = scount = char_count =  final_char_count = 0;
      while (!feof (inputfile)) {
	         inchar = consume_char (inputfile);
             char_count+=1;
	         if (inchar == '[') {
	             ss = 1;
	         }
	         if (inchar == ']') {
	             es = 1;
	         }
	         if ((ss = 1) && (es == 1)) {
	             ss = es = 0;
	             scount += 1;
                 final_char_count += char_count;
                 char_count = 0;
	         }
      } 
      // close and re-open the file
      fclose (inputfile);
      inputfile = fopen (argv[1], "r");
      if (inputfile == NULL) {
	      fprintf (stderr, "> Failed to re-open '%s'\n", argv[1]);
	      exit (0);
      }
      // and wind forward part the end of the preamble again.
      delim_count = 0;
      for (i = 0; i < 1024; i++) {
	       inchar = consume_char (inputfile);
	       if (inchar == '#') {
	           delim_count++;
	       }
	       if (delim_count == 2) {
	           break;
	       }
      }
      printf ("#");
      printf ("%d:", scount);
      printf ("%d:", atoi (pr_v_array[1]));
      printf ("%d:", atoi (pr_v_array[2]));
      printf ("%d#", atoi (pr_v_array[3]));
      for(a=0;a<final_char_count;a++) {
	         inchar = consume_char (inputfile);
	         printf ("%c", inchar);
      }

      fclose (inputfile);  
      return 1;
    }

  return 1;
}
