/*		  Released under GNU GENERAL PUBLIC LICENSE
Version 2, June 1991 -  or later
*/
#ifndef definesH
#define definesH
#define MASSIVE_PARTICLE_3 7
#define MASSIVE_PARTICLE_2 6
#define MAJOR_PARTICLE_2 5
#define MINOR_PARTICLE_2 4
#define MASSIVE_PARTICLE_1 3
#define MAJOR_PARTICLE_1 2
#define MINOR_PARTICLE_1 1
#define INDICATOR_SIZE 2.5
#endif
