/********************************************************************
*
* Function : main
* Date of first writing : 2007
* Author : Carey Pridgeon
* EMail  : carey.pridgeon@gmail.com
* Description: this program will merge two nbo files into one. The result is written to a fresh file.
    Both nbo files must have an identical number of particles.
* Licence:     GPL 3 .0 or later.
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/


#include "header.h"
int main (int argc, char **argv) {
    FILE *inputfile1,*inputfile2;
    int a,i, pcount, ppos, char_count, delim_count, final_char_count1, final_char_count2;
    char pr_str1[1024], pr_v_array1[4][1024],pr_str2[1024], pr_v_array2[4][1024], inchar;
    if (argc == 1) {
            printf ("> nbojoin, the nMod Toolkit utility to join two nbo files together.\n");
            printf ("> This utility writes the new combined file to stdout, redirect with > to a new file.\n");
            printf ("> Usage:\n");
            printf ("> nbojoin <nbo_file1.nbo> <nbo_file2.nbo>\n");
            exit (0);
        } 
    if (argc == 3) { 
        inputfile1 = fopen (argv[1], "r");
        if (inputfile1 == NULL) {
            fprintf (stderr, "> Failed to open '%s'\n", argv[1]);
            exit (0);
        }
        inputfile2 = fopen (argv[2], "r");
        if (inputfile2 == NULL) {
            fprintf (stderr, "> Failed to open '%s'\n", argv[2]);
            exit (0);
        }
        // load the preamble from the first nbo file and store it for later
        delim_count = char_count = 0;
        for (i = 0; i < 1024; i++) {
            inchar = consume_char (inputfile1);
            char_count++;
            if (inchar == '#') {
                delim_count++;
            }
            pr_str1[i] = inchar;
            if (delim_count == 2) {
                break;
            }
        }
        // now process the preamble string
        // preamble is a fixed format right now, so extraction is simple
        pcount = ppos = 0;
        for (i = 0; i < char_count; i++) {
            if (pr_str1[i] == '#') {
                pr_str1[i] = ' ';
            }
        }
        for (i = 0; i < char_count; i++) {	// store the values in the preamble
            if (pr_str1[i] == ':') {
                pcount++;
                ppos = 0;
            }
            else {
                pr_v_array1[pcount][ppos] = pr_str1[i];
                ppos++;
            }
        }
        // scan the remainder of the file, counting chars.
        char_count =  final_char_count1 = 0;
        while (!feof (inputfile1)) {
            inchar = consume_char (inputfile1);
            char_count+=1;
        } 
        final_char_count1 = char_count;
        // close and re-open the file
        fclose (inputfile1);
        inputfile1 = fopen (argv[1], "r");
        if (inputfile1 == NULL) {
            fprintf (stderr, "> Failed to re-open '%s'\n", argv[1]);
            exit (0);
        }
        // and wind forward part the end of the preamble again.
        delim_count = 0;
        for (i = 0; i < 1024; i++) {
            inchar = consume_char (inputfile1);
            if (inchar == '#') {
                delim_count++;
            }
            if (delim_count == 2) {
                break;
            }
        }      
        // load the preamble from the second nbo file and store it for later
        delim_count = char_count = 0;
        for (i = 0; i < 1024; i++) {
            inchar = consume_char (inputfile2);
            char_count++;
            if (inchar == '#') {
                delim_count++;
            }
            pr_str2[i] = inchar;
            if (delim_count == 2) {
                break;
            }
        }
        // now process the preamble string
        // preamble is a fixed format right now, so extraction is simple
        pcount = ppos = 0;
        for (i = 0; i < char_count; i++) {
            if (pr_str2[i] == '#') {
                pr_str2[i] = ' ';
            }
        }
        for (i = 0; i < char_count; i++) {	// store the values in the preamble
            if (pr_str2[i] == ':') {
                pcount++;
                ppos = 0;
            }
            else {
                pr_v_array2[pcount][ppos] = pr_str2[i];
                ppos++;
            }
        }
        // scan the remainder of the file, counting chars.
        char_count =  final_char_count2 = 0;
        while (!feof (inputfile2)) {
            inchar = consume_char (inputfile2);
            char_count+=1;
        } 
        final_char_count2 = char_count;
        // close and re-open the file
        fclose (inputfile2);
        inputfile2 = fopen (argv[2], "r");
        if (inputfile2 == NULL) {
            fprintf (stderr, "> Failed to re-open '%s'\n", argv[2]);
            exit (0);
        }
        // and wind forward part the end of the preamble again.
        delim_count = 0;
        for (i = 0; i < 1024; i++) {
            inchar = consume_char (inputfile2);
            if (inchar == '#') {
                delim_count++;
            }
            if (delim_count == 2) {
                break;
            }
        }      
        printf ("#");
        printf ("%d:", atoi(pr_v_array1[0])+atoi(pr_v_array2[0]));
        printf ("%d:", atoi (pr_v_array1[1]));
        printf ("%d:", atoi (pr_v_array1[2]));
        printf ("%d#", atoi (pr_v_array1[3]));
        for(a=0;a<final_char_count1;a++) {
            inchar = consume_char (inputfile1);
            printf ("%c", inchar);
        }

        for(a=0;a<final_char_count2;a++) {
            inchar = consume_char (inputfile2);
            printf ("%c", inchar);
        }
        fclose (inputfile1);
        fclose (inputfile2);
        exit (0);
    }

    return 1;
}


