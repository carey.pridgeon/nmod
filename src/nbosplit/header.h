/*		  Released under GNU GENERAL PUBLIC LICENSE
Version 2, June 1991 -  or later
 */
#ifndef HeaderH
  #define HeaderH
  #include <stdio.h>   
  #include <stdlib.h>
  #include <string.h>
  #include <time.h>  
  #include <math.h>
  #include "defines.h"
  #include "globals.h"
  void parse_line(char *input, char *word1, char *word2, char *word3);
  char consume_char(FILE *in);
  states *read_statefile(char *infile_name);
single_state *init_temp_state (int particle_num);

#endif
