/********************************************************************
*
* Function : main
* Date of first writing : 2007
* Author : Carey Pridgeon
* EMail  : carey.pridgeon@gmail.com
* Description: This program converts the content of an nbo file to plain text, 
*              sending the converted data to stdout
* Licence:     GPL 3 .0 or later.
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/  

// This program csplits out a specified portion of an nbo file, sending the result to stdout.
// very few checks, this utility assumes you know what you're doing.
// Indexing is from state 1 


#include "header.h"
int main(int argc, char **argv) {
    int i, j,start_state,end_state,new_state_num;
    if (argc != 4)
    {
                printf ("> nbosplit, the nMod Toolkit utility to extract a sequence of states from an nbo file.\n");
            printf ("> The extracted states are formed into a new nbo file, which is sent to stdout.\n");
            printf ("> Redirect the output with > to a new nbo file.\n");
            printf ("> Usage:\n");
            printf ("> nbosplit <nbo_filename.nbo> [starting state index] [ending state index]\n");
        exit(0);
    }
    loaded_state = read_statefile(argv[1]);
    start_state = atoi(argv[2]);
    end_state = atoi(argv[3]);
    if (start_state > end_state) {
        printf("> End state for split less than starting state - exiting");
    }
    new_state_num = end_state - start_state;
    /* push to stdout */
    printf("#");
    printf("%d:",new_state_num);
    printf("%d:", loaded_state->num_particles);
    printf("%d:", loaded_state->state_interval);
    printf("%d#", loaded_state->particle_datasize);
    for (i = start_state-1; i < end_state-1; i++) {
        printf("[s%d!",i);
        printf("%d!", loaded_state->set[i].num_particles);
        for (j = 0; j < loaded_state->set[i].num_particles; j++){
            printf("(%s|", loaded_state->set[i].particles[j].particle_id);
            if (loaded_state->set[i].particles[j].type == MASSIVE_PARTICLE_3)     {
                printf("ma3|");
            }
            if (loaded_state->set[i].particles[j].type == MASSIVE_PARTICLE_2)      {
                printf("ma2|");
            }
            if (loaded_state->set[i].particles[j].type == MASSIVE_PARTICLE_1)      {
                printf("ma1|");
            }
            if (loaded_state->set[i].particles[j].type == MAJOR_PARTICLE_2)      {
                printf("mj2|");
            }
            if (loaded_state->set[i].particles[j].type == MAJOR_PARTICLE_1)      {
                printf("mj1|");
            }
            if (loaded_state->set[i].particles[j].type == MINOR_PARTICLE_2)      {
                printf("mn2|");
            }
            if (loaded_state->set[i].particles[j].type == MINOR_PARTICLE_1)      {
                printf("mn1|");
            }
            printf("%e|", loaded_state->set[i].particles[j].mass);
            printf("%e|", loaded_state->set[i].particles[j].mass);
            printf("%s|", loaded_state->set[i].particles[j].freetext);
            printf("(%d,%d,%d)|",loaded_state->set[i].particles[j].rgb_vals.r, loaded_state->set[i].particles[j].rgb_vals.g, loaded_state->set[i].particles[j].rgb_vals.b);
            printf("%e|", loaded_state->set[i].particles[j].xloc);
            printf("%e|", loaded_state->set[i].particles[j].yloc);
            printf("%e|", loaded_state->set[i].particles[j].zloc);
            printf("%e|", loaded_state->set[i].particles[j].xsp);
            printf("%e|", loaded_state->set[i].particles[j].ysp);
            printf("%e)", loaded_state->set[i].particles[j].zsp);
        }
        printf("]");
    }
    return 1;
}
