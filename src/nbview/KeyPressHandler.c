/********************************************************************
*
* Function                       : KeyPressHandler
* Date of first writing  : 2006
* Author                          : Carey Pridgeon
* EMail                            : carey.pridgeon@gmail.com
* Description:  Callback function called when a normal key is pressed.
* Licence: GPL 3.0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#include "header.h"
void KeyPressHandler (unsigned char key, int x, int y) {
    switch (key) {
    case 112:
    case 76:
        // pause
        pause_var = pause_var ? 0 : 1;
        break;
    case 113:
    case 81:
    case 27:
        // Quit 
        glutDestroyWindow (Window_ID);
        exit (1);
        break;
    case 116:
        // text display
        showtext = showtext ? 0 : 1;
        break;
    case 114:
        // r              
        // reset to default view
        angle_x = 0;
        angle_y = 180;
        angle_z = 0;
        scaler = scaler_orig;
        delay_current = initial_delay;
        focus_particle = 0;
        reset_burn_event_list (events_record,size_of_events_array);
        if (show_trails) {
            reset_trails (trail_record);
        }
        break;
    case 99:

        // c          
        //  reset to default view with focus particle and zoom level unchanged
        angle_x = 0;
        angle_y = 180;
        angle_z = 0;
        if (show_trails) {
            reset_trails (trail_record);
        }
        break;
    case 103:
        // g
        if (show_trails == 0) {
            show_trails = 1;
        }
        else {
            show_trails = 0;
        }
        break;
    case 98:
        // b              
        //  begin display
        begin_display = 1;
        break;
    case 117:
        // u
        if ((scaler - zoom_step) > 0) {
            reset_burn_event_list (events_record,size_of_events_array);
            scaler -= zoom_step;
            // we don't want the scale value to become a negative value
            // and we need to clear the trails arrays, if shown
            if (show_trails) {
                reset_trails (trail_record);
            }
        }
        break;
    case 105:
        // i      
        reset_burn_event_list (events_record,size_of_events_array);
        scaler += zoom_step;
        if (show_trails) {
            reset_trails (trail_record);
        }
        break;
    case 108:
        // l
        showlines = showlines ? 0 : 1;
        break;
    case 107:
        // k
        show_trails = show_trails ? 0 : 1;
        break;
    case 91:
        // [  
        delay_current += 1000;
        break;
    case 93:
        // ] 
        if (delay_current > 1000) {
            delay_current -= 1000;
        }
        else {
            delay_current = 0;
        }
        break;
    case 102:
        // f      
        focus_particle_toggle = focus_particle_toggle ? 0 : 1;

        break;
    case 104:
        // h      
        help = help ? 0 : 1;
        break;
    case 115:
        // s      
        selected_particle_toggle = selected_particle_toggle ? 0 : 1;
        break;
    case 35:
        // #
        replay_direction = replay_direction ? 0 : 1;
        reset_burn_event_list (events_record,size_of_events_array);
        if (show_trails) {
            reset_trails (trail_record);
        }
        break;
    case 119:
        // w
        show_inds = show_inds ? 0 : 1;
        break;
    case 49:
        if (focus_particle > 0) {
            focus_particle--;
            reset_burn_event_list (events_record,size_of_events_array);
            if (show_trails) {
                reset_trails (trail_record);
            }
        }
        break;
    case 50:
        if (focus_particle < max_particles - 1) {
            focus_particle++;
            reset_burn_event_list (events_record,size_of_events_array);
            if (show_trails) {
                reset_trails (trail_record);
            }
        }
        break;
    case 51:
        if (selected_particle > 0) {
            selected_particle--;
        }
        break;
    case 52:
        if (selected_particle < max_particles - 1) {
            selected_particle++;
        }
        break;
    case 53:
        zoom_step = 1;
        break;
    case 54:
        zoom_step = 10;
        break;
    case 55:
        zoom_step = 100;
        break;
    case 56:
        zoom_step = 1000;
        break;
    case 57:
        zoom_step = 10000;
        break;
    case 120:
        cycle_display = cycle_display ? 0 : 1;
        break;
    case 141:
        if (display_toggle == 1) {
            display_toggle = 2;
            angle_x = 0;
            angle_y = 180;
            angle_z = 0;
            break;
        }
        if (display_toggle == 2) {
            display_toggle = 1;
            angle_x = -90;
            angle_y = 180;
            angle_z = 0;
            break;
        }
        break;
;
    default:
        break;
    }
}
