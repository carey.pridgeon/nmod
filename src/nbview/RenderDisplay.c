
/********************************************************************
*
* Function  : RenderDisplay
* Date of first writing  :  2006
* Author : Carey Pridgeon
* EMail  : carey.pridgeon@gmail.com
* Description:  This function draws the latest frame into the opengl context
* Licence: GPL 3 .0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#include "header.h"
#include "screen_capture.h"

void RenderDisplay (void) {
    char buf[80];			// For strings.
    int i, j,k;
    char fname[100];

    if (delay_count < delay_current) {
        delay_count++;
    }
    else {
        delay_count = 0;
        
        // reset the burns indicator array
        if (current_pos ==0) {
            reset_burn_event_list (events_record,size_of_events_array);
        }
        //enable things
        glEnable (GL_LIGHTING);
        glEnable (GL_DEPTH_TEST);
        // count up for trail data storage
        trail_counteriser_of_d00m++;
        // Need to manipulate the ModelView matrix to move our model around.
        glMatrixMode (GL_MODELVIEW);
        // Reset to 0,0,0; no rotation, no scaling.
        glLoadIdentity ();
        // translate to screen centre
        // a lot of transforms are going to be done, so we pop this original matrix
        // onto the stack   
        glPushMatrix ();
        /*
    rotation vars
    */
        /*
    ensure wrap-around works before anything gets used 
    */
        /*
    positive 
    */
        if (angle_x >= 360) {
            angle_x = 0;
        }
        if (angle_y >= 360) {
            angle_y = 0;
        }
        if (angle_z >= 360) {
            angle_z = 0;
        }
        /*
    negative 
    */
        if (angle_x <= -360) {
            angle_x = 0;
        }
        if (angle_y <= -360) {
            angle_y = 0;
        }
        if (angle_z <= -360) {
            angle_z = 0;
        }
        //apply current rotation to x 
        glRotatef (angle_x, 1.0, 0.0, 0.0);
        //apply current rotation to y 
        glRotatef (angle_y, 0.0, 1.0, 0.0);
        //apply current rotation to z 
        glRotatef (angle_z, 0.0, 0.0, 1.0);
        // Clear the color and depth buffers.
        glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        //scale, but with a little extra legroom
        scale_state (&(loaded_state->set[current_pos]), current_state, scaler,max_distance_from_origin*2);
        // note the scaled state has only position data. For
        // all other fields, use the loaded state
        // display (set up to allow for variable numbers of
        // particles per state. They should only decrease with this method
        // so it needs to be improved)
        
        check_for_burn_event (&(loaded_state->set[current_pos]), current_state,events_record,event_word,size_of_events_array,focus_particle);
        for (i = 0; i < loaded_state->set[current_pos].num_particles; i++) {
            if (show_trails && (trail_counteriser_of_d00m >= trail_delay)) {
                // store/update the trail
                update_trails (trail_record, current_state, focus_particle);
                trail_counteriser_of_d00m = 0;
            }
            // save the current matrix to the stack
            // (currently sat at origin)
            glPushMatrix ();
            glLoadIdentity ();
            //apply current rotation to x 
            glRotatef (angle_x, 1.0, 0.0, 0.0);
            //apply current rotation to y 
            glRotatef (angle_y, 0.0, 1.0, 0.0);
            //apply current rotation to z 
            glRotatef (angle_z, 0.0, 0.0, 1.0);
            // colour this sphere
            glColor3ub (loaded_state->set[current_pos].particles[i].rgb_vals.r,
            loaded_state->set[current_pos].particles[i].rgb_vals.g,
            loaded_state->set[current_pos].particles[i].rgb_vals.b);
            //translate out to position in the wcs, relative to the current focus particle
            glTranslatef ((current_state->particles[i].xloc) -
            (current_state->particles[focus_particle].xloc),
            (current_state->particles[i].yloc) -
            (current_state->particles[focus_particle].yloc),
            (current_state->particles[i].zloc) -
            (current_state->particles[focus_particle].zloc));
            if (loaded_state->set[current_pos].particles[i].type ==
                    MINOR_PARTICLE_1) {
                glutSolidSphere (sphere_size_minor_1, 30.0f, 30.0f);
            }
            if (loaded_state->set[current_pos].particles[i].type ==
                    MINOR_PARTICLE_2) {
                glutSolidSphere (sphere_size_minor_2, 30.0f, 30.0f);
            }
            if (loaded_state->set[current_pos].particles[i].type ==
                    MAJOR_PARTICLE_1) {
                glutSolidSphere (sphere_size_major_1, 30.0f, 30.0f);
            }
            if (loaded_state->set[current_pos].particles[i].type ==
                    MAJOR_PARTICLE_2) {
                glutSolidSphere (sphere_size_major_2, 30.0f, 30.0f);
            }
            if (loaded_state->set[current_pos].particles[i].type ==
                    MASSIVE_PARTICLE_1) {
                glutSolidSphere (sphere_size_massive_1, 30.0f, 30.0f);
            }
            if (loaded_state->set[current_pos].particles[i].type ==
                    MASSIVE_PARTICLE_2) {
                glutSolidSphere (sphere_size_massive_2, 30.0f, 30.0f);
            }
            if (loaded_state->set[current_pos].particles[i].type ==
                    MASSIVE_PARTICLE_3) {
                glutSolidSphere (sphere_size_massive_3, 30.0f, 30.0f);
            }
        }
        // Display the burns recorded thus far.
        // If no burns exist, this will just run through and move on
        glDisable (GL_LIGHTING); // no fnacy lighting, we want to see these burns clearly
        for (k=0;k<size_of_events_array;k++) {
            // save the current matrix to the stack
            if(events_record[k].has_position_data == TRUE) {

                glPushMatrix ();
                glLoadIdentity ();
                //apply current rotation to x 
                glRotatef (angle_x, 1.0, 0.0, 0.0);
                //apply current rotation to y 
                glRotatef (angle_y, 0.0, 1.0, 0.0);
                //apply current rotation to z 
                glRotatef (angle_z, 0.0, 0.0, 1.0);   
                glColor3ub (events_record[k].rgb_vals.r,events_record[k].rgb_vals.g,events_record[k].rgb_vals.b); 
                glPushMatrix ();
                glLoadIdentity ();
                //apply current rotation to x 
                glRotatef (angle_x, 1.0, 0.0, 0.0);
                //apply current rotation to y 
                glRotatef (angle_y, 0.0, 1.0, 0.0);
                //apply current rotation to z 
                glRotatef (angle_z, 0.0, 0.0, 1.0);                   
                glTranslatef (events_record[k].xloc,events_record[k].yloc,events_record[k].zloc);
                glutSolidCube (INDICATOR_SIZE);
                glPopMatrix ();
                // colour this burn event (will be the same as the particle whose burn is is
                //printf(" > burn event %d displayed x %f y %f z %f\n",k,events_record[k].xloc,events_record[k].yloc,events_record[k].zloc);
                glBegin (GL_LINES);
                glVertex3f (events_record[k].xloc, events_record[k].yloc-5, events_record[k].zloc);
                glVertex3f (events_record[k].xloc, events_record[k].yloc+5, events_record[k].zloc);
                glVertex3f (events_record[k].xloc+5, events_record[k].yloc, events_record[k].zloc);
                glVertex3f (events_record[k].xloc-5, events_record[k].yloc, events_record[k].zloc);
                glVertex3f (events_record[k].xloc, events_record[k].yloc,events_record[k].zloc-5);
                glVertex3f (events_record[k].xloc, events_record[k].yloc, events_record[k].zloc+5);
                glEnd ();                
                glPopMatrix ();
            }
        }
        glEnable (GL_LIGHTING);
        if (show_trails) {
            // user has selected to display particle trails
            glDisable (GL_LIGHTING);
            for (i = 0; i < loaded_state->set[current_pos].num_particles; i++) {
                // save the current matrix to the stack
                glPushMatrix ();
                glLoadIdentity ();
                //apply current rotation to x 
                glRotatef (angle_x, 1.0, 0.0, 0.0);
                //apply current rotation to y 
                glRotatef (angle_y, 0.0, 1.0, 0.0);
                //apply current rotation to z 
                glRotatef (angle_z, 0.0, 0.0, 1.0);
                glBegin (GL_POINTS);
                // colour this trail
                glColor3ub (loaded_state->set[current_pos].particles[i].rgb_vals.r,
                loaded_state->set[current_pos].particles[i].rgb_vals.g,
                loaded_state->set[current_pos].particles[i].rgb_vals.b);
                for (j = 0; j < trail_record->trail_size; j++) {
                    glVertex3f (trail_record->trail[i].xloc[j],
                    trail_record->trail[i].yloc[j],
                    trail_record->trail[i].zloc[j]);
                }
                glEnd ();
                glPopMatrix ();
            }
            glEnable (GL_LIGHTING);
        }
        // if selected particle indicator is switched on, display it
        if (show_inds) {
            glPushMatrix ();
            glLoadIdentity ();
            // colour this indicator to match the selected particle's colour
            glColor3ub (loaded_state->set[current_pos].particles[selected_particle].
            rgb_vals.r,
            loaded_state->set[current_pos].particles[selected_particle].
            rgb_vals.g,
            loaded_state->set[current_pos].particles[selected_particle].
            rgb_vals.b);
            //apply current rotation to x 
            glRotatef (angle_x, 1.0, 0.0, 0.0);
            //apply current rotation to y 
            glRotatef (angle_y, 0.0, 1.0, 0.0);
            //apply current rotation to z 
            glRotatef (angle_z, 0.0, 0.0, 1.0);
            glTranslatef (((current_state->particles[selected_particle].xloc) -
            (current_state->particles[focus_particle].xloc)) + 20,
            ((current_state->particles[selected_particle].yloc) -
            (current_state->particles[focus_particle].yloc)),
            ((current_state->particles[selected_particle].zloc) -
            (current_state->particles[focus_particle].zloc)));
            glBegin (GL_LINES);
            glColor4f (0.0f, 0.0f, 0.0f, .10);
            //black line again, awlays with the black....
            glVertex3f (0, 0, 0);
            glVertex3f (-20, 0, 0);
            glEnd ();
            glPushMatrix ();
            //apply current rotation to x 
            glRotatef (selrot, 1.0, 0.0, 0.0);
            glutSolidCube (INDICATOR_SIZE);
            // colour this indicator to match the selected particle's colour
            glColor3ub (loaded_state->set[current_pos].particles[selected_particle].
            rgb_vals.r,
            loaded_state->set[current_pos].particles[selected_particle].
            rgb_vals.g,
            loaded_state->set[current_pos].particles[selected_particle].
            rgb_vals.b);
            glutSolidTorus (INDICATOR_SIZE / 4, INDICATOR_SIZE, 8, 16);
            glPopMatrix ();
            if (selrot < 360) {
                selrot++;
            }
            else {
                selrot = 0;
            }
            // return to origin
            glPopMatrix ();
        }
        if (!pause_var) {

            // if paused, just re-display the current state, otherwise, increment/decrement away.   
            if (replay_direction) {
                // current playback direction is forward
                if (current_pos < loaded_state->total_states - 1) {
                    current_pos++;
                    activate_screen_capture = 1;
                }
                else {
                    if (cycle_display) {
                        // is the flag to cycle the display set?
                        current_pos = 0;
                        reset_trails (trail_record);
                    }
                }
            }
            else {
                // current playback direction is backwards
                if (current_pos > 0) {
                    current_pos--;
                    activate_screen_capture = 1;
                }
                else {
                    if (cycle_display) {
                        // is the flag to cycle the display set?
                        current_pos = loaded_state->total_states - 1;
                        reset_trails (trail_record);
                    }
                }
            }
        }
        if (showlines) {
            // draw in some lines to show the Axis
            glPushMatrix ();
            glEnable (GL_LINE_STIPPLE);
            glLineStipple (factor, pattern);
            //The lines look terrible lit, so don't
            glDisable (GL_LIGHTING);
            //apply current rotation to x 
            glRotatef (angle_x, 1.0, 0.0, 0.0);
            //apply current rotation to y 
            glRotatef (angle_y, 0.0, 1.0, 0.0);
            //apply current rotation to z 
            glRotatef (angle_z, 0.0, 0.0, 1.0);
            glBegin (GL_LINES);
            glColor4f (0.0f, 0.0f, 0.0f, .10);	//black
            glVertex3f (0, -250, 0);
            glVertex3f (0, 250, 0);
            glVertex3f (250, 0, 0);
            glVertex3f (-250, 0, 0);
            glEnd ();
            glPopMatrix ();
        }
        if (showtext) {
            glLoadIdentity ();
            // We need to change the projection matrix for the text rendering.  
            glMatrixMode (GL_PROJECTION);
            // But we like our current view too; so we save it here.
            glPushMatrix ();
            // Now we set up a new projection for the text.
            glLoadIdentity ();
            glOrtho (0, Window_Width, 0, Window_Height, -1, 1);
            // Lit or textured text looks awful.
            glDisable (GL_TEXTURE_2D);
            glDisable (GL_LIGHTING);
            glDisable (GL_DEPTH_TEST);
            if (help) {
                glColor3f (0.9, 0.2, 0.2);
                sprintf (buf, "Exit nbview: 'q'");
                glRasterPos2i (2, 2);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Pause Model: 'p'");
                glRasterPos2i (2, 18);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Show/Hide This Key Guide: 'h'");
                glRasterPos2i (2, 34);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Show/Hide All Text: 't'");
                glRasterPos2i (2, 50);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Speed Controls: slower '['  faster: ']'");
                glRasterPos2i (2, 66);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Camara Distance: closer: 'u' farther: 'i'");
                glRasterPos2i (2, 80);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Camara Distance Step   1x:     '5'");
                glRasterPos2i (2, 96);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Camara Distance Step   10x:   '6'");
                glRasterPos2i (2, 112);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Camara Distance Step  100x:  '7'");
                glRasterPos2i (2, 128);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Camara Distance Step 1000x: '8'");
                glRasterPos2i (2, 144);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Camara Distance Step 10000x: '9'");
                glRasterPos2i (2, 160);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Rotate X and Y Axis: Cursor Keys");
                glRasterPos2i (2,176 );
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Rotate Z Axis: PageUp/PageDown");
                glRasterPos2i (2, 192);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Toggle Axis Lines: 'l'");
                glRasterPos2i (2, 208);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Reset Display to Default View: 'r'");
                glRasterPos2i (2, 224);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Reset Display to Default View -  Angles only: c");
                glRasterPos2i (2, 240);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Shift Focus Particle :      '1' - '2'");
                glRasterPos2i (2,256 );
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Shift Selected Particle : '3' - '4'");
                glRasterPos2i (2, 272);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Toggle Selected particle Indicator: 'w'");
                glRasterPos2i (2, 288);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Toggle Cyclic Display: 'x'");
                glRasterPos2i (2, 304);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Toggle XY YZ View: 'a'");
                glRasterPos2i (2,320);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Toggle nbo file playback direction: '#'");
                glRasterPos2i (2,336);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Toggle Show Focus Body Data: 'f'");
                glRasterPos2i (2, 352);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Toggle Show Selected Body Data: 's'");
                glRasterPos2i (2,368 );
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Toggle Particle Trails: 'k'");
                glRasterPos2i (2,  384);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Toggle Screen Capture: 'F1'");
                glRasterPos2i (2,400);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "Take Single Screenshot: 'F2'");
                glRasterPos2i (2,416 );
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
            }
            // show that the model has been paused 
            if (pause_var) {
                glColor3f (0.9, 0.2, 0.2);
                sprintf (buf,
                "State: %d of %d (Paused)     Xa: %2.0f Ya: %2.0f Za: %2.0f",
                loaded_state->set[current_pos].state_number,
                loaded_state->total_states, angle_x, angle_y, angle_z);
                glRasterPos2i (2, Window_Height - 20);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
            }
            else {
                // show current state
                //always want to show the current state (top right), regardless of whether text is being shown
                glColor3f (0.9, 0.2, 0.2);
                sprintf (buf,
                "State: %d of %d              Xa: %2.0f Ya: %2.0f Za: %2.0f",
                loaded_state->set[current_pos].state_number,
                loaded_state->total_states, angle_x, angle_y, angle_z);
                glRasterPos2i (2, Window_Height - 20);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
            }
            // Report the Focus Particles info if focus_particle_toggle is on
            if (focus_particle_toggle) {
                glColor3f (0.9, 0.2, 0.2);
                sprintf (buf, "Focus Particle: %s (%d)",
                loaded_state->set[current_pos].particles[focus_particle].
                particle_id, focus_particle);
                glRasterPos2i (Window_Width - 300, Window_Height - 20);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "%s X Location %2.5e",
                loaded_state->set[current_pos].particles[focus_particle].
                particle_id,
                loaded_state->set[current_pos].particles[focus_particle].
                xloc);
                glRasterPos2i (Window_Width - 300, Window_Height - 36);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "%s Y Location %2.5e",
                loaded_state->set[current_pos].particles[focus_particle].
                particle_id,
                loaded_state->set[current_pos].particles[focus_particle].
                yloc);
                glRasterPos2i (Window_Width - 300, Window_Height - 52);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "%s Z Location %2.5e",
                loaded_state->set[current_pos].particles[focus_particle].
                particle_id,
                loaded_state->set[current_pos].particles[focus_particle].
                zloc);
                glRasterPos2i (Window_Width - 300, Window_Height - 68);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "%s X Velocity %2.5e",
                loaded_state->set[current_pos].particles[focus_particle].
                particle_id,
                loaded_state->set[current_pos].particles[focus_particle].
                xsp);
                glRasterPos2i (Window_Width - 300, Window_Height - 84);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "%s Y Velocity %2.5e",
                loaded_state->set[current_pos].particles[focus_particle].
                particle_id,
                loaded_state->set[current_pos].particles[focus_particle].
                ysp);
                glRasterPos2i (Window_Width - 300, Window_Height - 100);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "%s Z Velocity %2.5e",
                loaded_state->set[current_pos].particles[focus_particle].
                particle_id,
                loaded_state->set[current_pos].particles[focus_particle].
                zsp);
                glRasterPos2i (Window_Width - 300, Window_Height - 116);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
            }
            // Report the Selected Particles info if selected_particle_toggle on
            if (selected_particle_toggle) {
                glColor3f (0.9, 0.2, 0.2);
                sprintf (buf, "Selected Particle: %s (%d)",
                loaded_state->set[current_pos].particles[selected_particle].
                particle_id, selected_particle);
                glRasterPos2i (Window_Width - 300, 108);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "%s X Location %2.5e",
                loaded_state->set[current_pos].particles[selected_particle].
                particle_id,
                loaded_state->set[current_pos].particles[selected_particle].
                xloc);
                glRasterPos2i (Window_Width - 300, 92);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "%s Y Location %2.5e",
                loaded_state->set[current_pos].particles[selected_particle].
                particle_id,
                loaded_state->set[current_pos].particles[selected_particle].
                yloc);
                glRasterPos2i (Window_Width - 300, 76);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "%s Z Location %2.5e",
                loaded_state->set[current_pos].particles[selected_particle].
                particle_id,
                loaded_state->set[current_pos].particles[selected_particle].
                zloc);
                glRasterPos2i (Window_Width - 300, 60);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "%s X Velocity %2.5e",
                loaded_state->set[current_pos].particles[selected_particle].
                particle_id,
                loaded_state->set[current_pos].particles[selected_particle].
                xsp);
                glRasterPos2i (Window_Width - 300, 44);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "%s Y Velocity %2.5e",
                loaded_state->set[current_pos].particles[selected_particle].
                particle_id,
                loaded_state->set[current_pos].particles[selected_particle].
                ysp);
                glRasterPos2i (Window_Width - 300, 28);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
                sprintf (buf, "%s Z Velocity %2.5e",
                loaded_state->set[current_pos].particles[selected_particle].
                particle_id,
                loaded_state->set[current_pos].particles[selected_particle].
                zsp);
                glRasterPos2i (Window_Width - 300, 12);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
            }
            // return to origin
            glPopMatrix ();
        }
        else {
            //always want to show the current state (top right), regardless of whether text is being shown
            // Move back to the origin (for the text, below).
            glLoadIdentity ();
            // We need to change the projection matrix for the text rendering.  
            glMatrixMode (GL_PROJECTION);
            // But we like our current view too; so we save it here.
            glPushMatrix ();
            // Now we set up a new projection for the text.
            glLoadIdentity ();
            glOrtho (0, Window_Width, 0, Window_Height, -1.0, 1.0);
            // Lit or textured text looks awful.
            glDisable (GL_TEXTURE_2D);
            glDisable (GL_LIGHTING);
            glDisable (GL_DEPTH_TEST);
            // show that the model has been paused 
            if (pause_var) {
                glColor3f (0.9, 0.2, 0.2);
                sprintf (buf,
                "State: %d of %d (Paused)     Xa: %2.0f Ya: %2.0f Za: %2.0f",
                loaded_state->set[current_pos].state_number,
                loaded_state->total_states, angle_x, angle_y, angle_z);
                glRasterPos2i (2, Window_Height - 20);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
            }
            else {
                // show current state
                //always want to show the current state (top right), regardless of whether text is being shown
                glColor4f (0.9, 0.2, 0.2, .75);
                sprintf (buf,
                "State: %d of %d              Xa: %2.0f Ya: %2.0f Za: %2.0f",
                loaded_state->set[current_pos].state_number,
                loaded_state->total_states, angle_x, angle_y, angle_z);
                glRasterPos2i (2, Window_Height - 20);
                PrintString (GLUT_BITMAP_HELVETICA_12, buf);
            }
            // return to origin
            glPopMatrix ();
        }
        //Time to return to the original view
        glPopMatrix ();
        glutSwapBuffers ();
        if (screen_capture_enabled) {
            if (activate_screen_capture) {
                sprintf(fname,"images/state_%d.bmp",loaded_state->set[current_pos].state_number);
                take_screenshot_bmp(fname,0, 0, Window_Width, Window_Height);
                activate_screen_capture = 0;
            }
        }
        if (take_screenshot) {
                sprintf(fname,"images/screenshot_state_%d.bmp",loaded_state->set[current_pos].state_number);
                take_screenshot_bmp(fname,0, 0, Window_Width, Window_Height);
                take_screenshot = 0;
       }

    }
}
