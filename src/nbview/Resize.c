/********************************************************************
*
* Function  : Resize
* Date of first writing  :  2007
* Author : Carey Pridgeon
* EMail  : carey.pridgeon@gmail.com
* Description:  Callback routine executed when the opengl context is resized.  
* Licence: GPL 3 .0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/

#include "header.h"
// ------

void Resize (int Width, int Height) {
    // Let's not core dump, no matter what.
    if (Height == 0) {
        Height = 1;
    }
    glViewport (0, 0, Width, Height);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    //gluPerspective (45.0f, (GLfloat) Width / (GLfloat) Height, 0.1f, 100.0f);
    /*
    make it a 3d cube 
*/
    glOrtho (-250.0F, 250.0F, -250.0F, 250.0F, -1000.0F, 1000.0F);
    glMatrixMode (GL_MODELVIEW);
    Window_Width = Width;
    Window_Height = Height;
}
