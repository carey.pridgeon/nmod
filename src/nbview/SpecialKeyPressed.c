/********************************************************************
*
* Function  : SpecialKeyPressed
* Date of first writing   :  2006
* Author : Carey Pridgeon
* EMail  : carey.pridgeon@gmail.com
* Description:  Callback Function called when a special key is pressed. Yup, what an amazingly informative description....
* Licence: GPL 3 .0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#include "header.h"

void SpecialKeyPressed (int key, int x, int y) {
    switch (key) {
        // here we are controlling the X Y Z rotation
    case GLUT_KEY_UP:
        // for x axis
        angle_x -= 1;
        break;
    case GLUT_KEY_DOWN:
        angle_x += 1;
        break;
    case GLUT_KEY_LEFT:
        // and y
        angle_y -= 1;
        break;
    case GLUT_KEY_RIGHT:
        angle_y += 1;
        break;
    case GLUT_KEY_PAGE_UP:
        // you *must* get it by now....
        angle_z += 1;
        break;
    case GLUT_KEY_PAGE_DOWN:
        angle_z -= 1;
        break;
   case GLUT_KEY_F1:
        screen_capture_enabled = screen_capture_enabled ? 0 : 1;
        break;
   case GLUT_KEY_F2:
        take_screenshot = 1;
        break;        
        
    default:
        break;
    }
}
