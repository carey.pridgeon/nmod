/********************************************************************
*
* Function  : update_trails
* Date of first writing : 01/2007
* Author : Carey Pridgeon
* EMail  : carey.pridgeon@gmail.com
* Description:  This function  shifts the trails down the arrays
            and adds the latest trails value to the head of the arrays.
* Licence:     GPL 3 .0 or later.
*  This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*  any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#include "header.h"
void check_for_burn_event (single_state *local_state, single_state *scaled_current_state, single_event *local_events_record, char *local_event_string, int num_burns,int local_focus_particle) {
    int i, j;
    //read through the list of burns we stored when the nbo file was loaded. Is there a burn in the current state?
    for (i = 0; i < num_burns; i++) {
        if(local_events_record[i].state_occuring == local_state->state_number) { // yarr, thar be burn here.
            for (j=0;j<local_state->num_particles;j++) { // read through the particles and find which has the burn event
                if (strcmp(local_state->particles[j].freetext,local_event_string)==0) {
                    local_events_record[i].xloc = scaled_current_state->particles[j].xloc - scaled_current_state->particles[focus_particle].xloc;
                    local_events_record[i].yloc = scaled_current_state->particles[j].yloc - scaled_current_state->particles[focus_particle].yloc;
                    local_events_record[i].zloc = scaled_current_state->particles[j].zloc - scaled_current_state->particles[focus_particle].zloc;
                    local_events_record[i].rgb_vals.r = local_state->particles[j].rgb_vals.r;
                    local_events_record[i].rgb_vals.g = local_state->particles[j].rgb_vals.g;
                    local_events_record[i].rgb_vals.b = local_state->particles[j].rgb_vals.b;
                    local_events_record[i].has_position_data = TRUE;
                }
            }
        }
    }
}

