/********************************************************************
*
* File                      : defines.h
* Date of first writing  : 2007
* Author                          : Carey Pridgeon
* EMail                            : carey.pridgeon@gmail.com
* Description:  This file is where all defines that nBview uses are declared
* Licence: GPL 3.0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#ifndef definesH
#define definesH
#define PROGRAM_TITLE "nBview - OpenGL nMod output file viewer"
#define TRUE 1
#define FALSE 0
#define MASSIVE_PARTICLE_3 7
#define MASSIVE_PARTICLE_2 6
#define MAJOR_PARTICLE_2 5
#define MINOR_PARTICLE_2 4
#define MASSIVE_PARTICLE_1 3
#define MAJOR_PARTICLE_1 2
#define MINOR_PARTICLE_1 1
#define INDICATOR_SIZE 2.5
#define sqr(x) ((x)*(x))
#endif
