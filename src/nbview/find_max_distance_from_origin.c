/********************************************************************
*
* Function                       : find_max_distance_from_origin
* Date of first writing  : 2006
* Author                          : Carey Pridgeon
* EMail                            : carey.pridgeon@gmail.com
* Description:  Discovers which of the particles in the current state is the furthest away,
*                        This value is what scale_state would use.
* Licence: GPL 3.0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#include "header.h"
double find_max_distance_from_origin (states * local_states) {
    double max, tmp1, tmp2, tmp3;
    int j;
    max = 0;
    for (j = 0; j < local_states->set[0].num_particles; j++) {
        tmp1 = sqrt (sqr (local_states->set[0].particles[j].xloc));
        tmp2 = sqrt (sqr (local_states->set[0].particles[j].yloc));
        tmp3 = sqrt (sqr (local_states->set[0].particles[j].zloc));
        if (tmp1 > max) {
            max = tmp1;
        }
        if (tmp2 > max) {
            max = tmp2;
        }
        if (tmp3 > max) {
            max = tmp3;
        }
    }
    return max;
}
