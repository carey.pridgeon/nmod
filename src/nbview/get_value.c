/********************************************************************
*
* Function                       : get_value
* Date of first writing  : 2006
* Author                          : Carey Pridgeon
* EMail                            : carey.pridgeon@gmail.com
* Description:  Takes the array of tokens from the parse_line function ,  discover what perameter the line describes and assign the value
* found to the correct variable
* Licence: GPL 3.0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#include "header.h"
void parse_line (char *input, char *word1, char *word2, char *word3);

int get_value (char *fetched) {
    char *first_word, *second_word, *value;
    first_word = (char *) calloc (32, sizeof (char));
    second_word = (char *) calloc (32, sizeof (char));
    value = (char *) calloc (32, sizeof (char));
    if (fetched[0] == '#') {//this line is a comment, ignore it 
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }				

    // the line isn't a comment,  so it should contain a value descriptor and a value
    // if it doesn't, it may be an empty line, so check for that here and return with 
    // -1, pretending the line is a comment

    parse_line (fetched, first_word, second_word, value);
    if ((first_word == NULL) || (second_word == NULL) || (value == NULL)) {
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("massive", first_word) == 0) && (strcmp ("particle_3",second_word) == 0)) {
        sphere_size_massive_3 = (atof (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("screen", first_word) == 0) && (strcmp ("capture",second_word) == 0)) {
        if (strcmp ("TRUE", value) == 0) { 
            screen_capture_enabled = 1;
        }
        if (strcmp ("FALSE", value) == 0) { 
            screen_capture_enabled = 0;
        }
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("massive", first_word) == 0) && (strcmp ("particle_2", second_word) == 0)) {
        sphere_size_massive_2 = (atof (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("massive", first_word) == 0) && (strcmp ("particle_1", second_word) == 0)) {
        sphere_size_massive_1 = (atof (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("major", first_word) == 0) && (strcmp ("particle_1", second_word) == 0)) {
        sphere_size_major_1 = (atof (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("major", first_word) == 0) && (strcmp ("particle_2", second_word) == 0)) {
        sphere_size_major_2 = (atof (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("minor", first_word) == 0) && (strcmp ("particle_1", second_word) == 0)) {
        sphere_size_minor_1 = (atof (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("minor", first_word) == 0) && (strcmp ("particle_2", second_word) == 0)) {
        sphere_size_minor_2 = (atof (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("focus", first_word) == 0) && (strcmp ("particle", second_word) == 0)) {
        focus_particle = (atoi (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("show", first_word) == 0) && (strcmp ("focus_info", second_word) == 0)) {
        focus_particle_toggle = (atoi (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("selected", first_word) == 0) && (strcmp ("particle", second_word) == 0)) {
        selected_particle = (atoi (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("show", first_word) == 0) && (strcmp ("sel_info", second_word) == 0)) {
        selected_particle_toggle = (atoi (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("show", first_word) == 0) && (strcmp ("text", second_word) == 0)) {
        showtext = (atoi (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("show", first_word) == 0) && (strcmp ("help", second_word) == 0)) {
        help = (atoi (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("trail", first_word) == 0) && (strcmp ("length", second_word) == 0)) {
        trail_size = (atoi (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("trail", first_word) == 0) && (strcmp ("delay", second_word) == 0)) {
        trail_delay = (atoi (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("trails", first_word) == 0) && (strcmp ("toggle", second_word) == 0)) {
        show_trails = (atoi (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("initial", first_word) == 0) && (strcmp ("delay", second_word) == 0)) {
        initial_delay = (atoi (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("start", first_word) == 0) && (strcmp ("paused", second_word) == 0)) {
        pause_var = (atoi (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("cycle", first_word) == 0) && (strcmp ("display", second_word) == 0)) {
        cycle_display = (atoi (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("start", first_word) == 0) && (strcmp ("state", second_word) == 0)) {
        current_pos = (atoi (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("show", first_word) == 0)&& (strcmp ("indicators", second_word)== 0)) {
        show_inds = (atoi (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("window", first_word) == 0) && (strcmp ("width", second_word) == 0) ) {
        Window_Width = (atoi (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("window", first_word) == 0)&& (strcmp ("height", second_word) == 0)) {
        Window_Height = (atoi (value));
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }
    if ((strcmp ("state", first_word) == 0)&& (strcmp ("scaler", second_word) == 0)) {
        scaler = atof (value);
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }

    if ((strcmp ("event", first_word) == 0) && (strcmp ("word", second_word) == 0)) {
        strcpy (event_word, value);
        free (first_word);
        free (second_word);
        free (value);
        return 1;
    }  
    free (first_word);
    free (second_word);
    free (value);  
    return 1;        
}
