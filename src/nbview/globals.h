/********************************************************************
*
* File                     : header.h
* Date of first writing  : 2006
* Author                          : Carey Pridgeon
* EMail                            : carey.pridgeon@gmail.com
* Description:  'Globals, get yer globals here, nice and hot, cooked em meself'
* Licence: GPL 3.0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#ifndef globalsH
#define globalsH
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
// state progress reporting vars
int StateNow;
int StateMax;
// title thing
char title_and_fname[100];
// Some global variables.
// Window and texture IDs, window width and height.
int Texture_ID;
int Window_ID;
int Window_Width;
int Window_Height;
// image recording control, vars
int screen_capture_enabled;
int activate_screen_capture;
int take_screenshot;
// display mode settings.
int Light_On;
int Blend_On;
int Texture_On;
int Filtering_On;
int Alpha_Add;
int Curr_TexMode;
// rotation of the selected particle indicator (around X)
GLfloat selrot;
// Object and scene global variables.
float *Light_Ambient;
float *Light_Diffuse;
float *Light_Position;
char **TexModesStr;
GLint *TexModes;
// ------
// Frames per second (FPS) statistic variables.
// Some global variables.
/*
    structure that is used when representing a colour for particles in the viewer
*/
typedef struct
{
    int r;			// red colour
    int g;			// green colour
    int b;			// guess!
} particlecolour;
/*
    structure that is used when representing a particle in the viewer
*/
typedef struct
{
    char particle_id[100];
    GLint type;
    GLfloat mass;
    GLfloat radius;
    char freetext[100];
    particlecolour rgb_vals;
    GLfloat xloc;			/*position in the WCS */
    GLfloat yloc;
    GLfloat zloc;
    GLfloat xsp;			/*speed in the WCS */
    GLfloat ysp;
    GLfloat zsp;
} particle_data;
/*
    structure that is used when representing a single state in the viewer.
    * Containing an array of particles to be found in that state
*/
typedef struct
{
    int state_number;
    int num_particles;
    particle_data *particles;
} single_state;
//instance of same used for scaling and display
single_state *current_state;
typedef struct
{
    int total_states;
    int state_interval;
    int num_particles;
    int particle_datasize;
    single_state *set;
} states;
states *loaded_state;

/*
    struct used for a single particle trail 
*/
typedef struct
{
    float *xloc;
    float *yloc;
    float *zloc;
} single_trail;
/*
    struct used for storing particle trails 
*/
typedef struct
{
    int trail_size;		// will be used to size the x y z trail storage arrays
    int total_particles;
    single_trail *trail;
} trail_set;
trail_set *trail_record;
// var used when setting the interval between trail recording instances
int trail_delay, trail_counteriser_of_d00m;

// we want to display events as reported in the nbo file, and this struct is to handle that.

typedef struct
{
    int state_occuring;
    int has_position_data; // boolean, using the defines TRUE or FALSE
    GLfloat xloc;		   //position in the WCS 
    GLfloat yloc;
    GLfloat zloc;
    particlecolour rgb_vals; // colours for this event (are taken from the particle whose burn it is)
} single_event;

// the max possible size for this array is set in the config file
// if the number of events exceeds this value, they won't be displayed.
single_event *events_record;
// the size of the array is found in this var, that is set in the config file
int size_of_events_array;
// the number of events found as the nbo is iterated is stored here
int event_count;
// and the word that indicates an event is stored here (defined in config file)
char event_word[100];
/*
    vars used when looping the display through the array of particles
*/
int poscount;
int current_pos;
FILE *inputfile;
GLint main_window;		/* Main Window handle         */
GLint num_particles;	/* the number of particles represented by this file */
GLint samplesize;		/* the sample rate in the given file */
GLint sizecount;		/* used to store the number of the time series sets in the  given file */
GLint done_conf;		/* used to say the file reading has moved past the first two  entries */
GLfloat temp;			/* used when interating through file to get the file size */
char p_type_check[100];
GLint trail_size;
GLint showlabels;
GLint show_trails;
GLint current_pos;
// the current position in the list of states being displayed
GLint begin_display;
/*
    particle data filename 
*/
char particle_filename[50];
double max_distance_from_origin;
GLfloat half_window;
/*
    magnification 
*/
GLfloat scaler;
GLfloat scaler_orig;
/*
    sizes for the spheres representing the particles
*/
GLfloat sphere_size_major_1;
GLfloat sphere_size_minor_1;
GLfloat sphere_size_massive_1;
GLfloat sphere_size_major_2;
GLfloat sphere_size_minor_2;
GLfloat sphere_size_massive_2;
GLfloat sphere_size_massive_3;
//focus body
GLint focus_particle, focus_particle_toggle;
//var for direction of file playback
GLint replay_direction;
//display angle toggle
GLint display_toggle;
// steps per zoom inrement
GLint zoom_step;
// pause var
int initial_delay, delay_count, delay_current;
FILE *inputfile;		/* the file to read */
FILE *conf;			/* the config file for viewer */
GLint read_position;		/* location in the file (in time seris slices) */
char filename[30];		/* used to store the filename for opening */
char text[100];			/* Storage for current string   */
/*
    vars used for rotation angles
*/
GLfloat angle_z;
GLfloat angle_x;
GLfloat angle_y;
/*
    vars used for rotation animation
*/
GLint spin_z;
GLint spin_x;
GLint spin_y;
/*
    line stippling 
*/
GLint factor;
GLushort pattern;
/*
    show the line box bool
*/
GLint showlines;
/*
    show the polar lines bool
*/
GLint showpoles;
/*
    show the text bool
*/
GLint showtext;
GLint pause_var;
// Window and texture IDs, window width and height.
int Texture_ID;
int Window_ID;
int Window_Width;
int Window_Height;
// display mode settings.
int Light_On;
int Blend_On;
int Texture_On;
int Filtering_On;
int Alpha_Add;
int Curr_TexMode;
int showtext;
// Object and scene global variables.
// position and rotation speed variables.
float X_Rot;
float Y_Rot;
float X_Speed;
float Y_Speed;
float Z_Off;
// Settings for light.
float *Light_Ambient;
float *Light_Diffuse;
float *Light_Position;
GLfloat *gray;
GLfloat *specular;
GLfloat *specref;
GLint cenlight;
/*
    line stippling 
*/
GLint factor;
GLushort pattern;
//var for focus particle data toggle
GLint verbose;
//var for help info toggle
GLint help;
// var for display cycling
GLint cycle_display;
char **TexModesStr;
GLint *TexModes;
// hack to obtain the number of particles
// for the specialkeypress handling function
int max_particles;
// selected particle var
int selected_particle, selected_particle_toggle;
// toggle for selected/focus particle indicators
GLint show_inds;

#endif
