/********************************************************************
*
* File                     : header.h
* Date of first writing  : 2006
* Author                          : Carey Pridgeon
* EMail                            : carey.pridgeon@gmail.com
* Description:  function declaration file
* Licence: GPL 3.0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#ifndef HeaderH
#define HeaderH
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <unistd.h>
#include "defines.h"
#include "globals.h"
void init_globals ();
void PrintString (void *font, char *str);
void RenderDisplay (void);
void SpecialKeyPressed (int key, int x, int y);
void KeyPressHandler (unsigned char key, int x, int y);
void GLInit (int Width, int Height);
void Resize (int Width, int Height);
int get_value (char *fetched);
void parse_line (char *input, char *word1, char *word2, char *word3);
char consume_char (FILE * in);
states *read_statefile (char *infile_name);
void readconfig (char *filename);
int scale_state (single_state * local_state, single_state * temp_state,
float wp, double md);
trail_set *init_trails (int p_num, int t_size);
single_state *init_temp_state (int particle_num);
double find_max_distance_from_origin (states * local_states);
void init_GL (int Width, int Height);
void mouseclickHandler (int button, int state, int x, int y);
void mousemotionHandler (int x, int y);
void update_trails (trail_set * local_trail, single_state * local_state, int local_focus_particle);
void reset_trails (trail_set * local_trail);
void var_sanity_check (states *local_state);
void check_for_burn_event (single_state *local_state, single_state *scaled_current_state, single_event *local_events_record, char *local_event_string, int num_burns,int local_focus_particle);
void reset_burn_event_list (single_event *local_events_record,int num_burns);
#endif
