/********************************************************************
*
* Function                       : init_GL
* Date of first writing  : 2006
* Author                          : Carey Pridgeon
* EMail                            : carey.pridgeon@gmail.com
* Description: Prepare yourself for a shock, but this function initialises GL...
* Licence: GPL 3.0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#include "header.h"
void init_GL (int Width, int Height) {
    // Color to clear color buffer to.
    glClearColor (255, 255, 255, 255);
    /*
    enable hidden surface removal
*/
    glEnable (GL_DEPTH_TEST);
    /*
    enable culling
*/
    glEnable (GL_CULL_FACE);
    // Depth to clear depth buffer to; type of test.
    glClearDepth (1.0);
    glDepthFunc (GL_LESS);
    // Enables Smooth Color Shading; try GL_FLAT for (lack of) fun.
    glShadeModel (GL_SMOOTH);
    // Load up the correct perspective matrix; using a callback directly.
    Resize (Width, Height);
    // Set up a light, turn it on.
    glEnable (GL_LIGHTING);
    glLightfv (GL_LIGHT1, GL_POSITION, Light_Position);
    glLightfv (GL_LIGHT1, GL_AMBIENT, Light_Ambient);
    glLightfv (GL_LIGHT1, GL_DIFFUSE, Light_Diffuse);
    glLightfv (GL_LIGHT1, GL_SPECULAR, specular);
    glEnable (GL_LIGHT1);
    glEnable (GL_COLOR_MATERIAL);
    // need more lights to illuminate the focus body..
    // have surface material mirror the color.
    glColorMaterial (GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable (GL_COLOR_MATERIAL);
    glMaterialfv (GL_FRONT, GL_SPECULAR, specref);
    glMateriali (GL_FRONT, GL_SHININESS, 128);
}
