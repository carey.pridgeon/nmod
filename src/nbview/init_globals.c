/********************************************************************
*
* Function                       :  init_globals
* Date of first writing  :  2006
* Author                          : Carey Pridgeon
* EMail                            : carey.pridgeon@gmail.com
* Description:  This function initialises all the variables that opengl needs for setting up the context, 
                        as well as a few used during display  
* Licence: GPL 3.0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#include "header.h"
// init global variables.
void init_globals () {
    trail_counteriser_of_d00m = 0;
    // Window and texture IDs, window width and height. Now loaded from 
    // the config file.
    int i;
    //Window_Width = 500;
    //Window_Height = 500;
    half_window = Window_Height / 2;
    // turn on screen capture for first frame, if continuous screen capture is on
    if (screen_capture_enabled) {
        activate_screen_capture = 1;
    }
    take_screenshot = 0;
    Curr_TexMode = 0;
    // Object and scene global variables.
    // position and rotation speed variables.
    X_Rot = 0.9f;
    Y_Rot = 0.0f;
    X_Speed = 0.0f;
    Y_Speed = 0.5f;
    Z_Off = -5.0f;
    // selected particle indicator rotation var
    selrot = 0;
    Light_Ambient = calloc (4, sizeof (float));
    Light_Ambient[0] = 0.5f;
    Light_Ambient[1] = 0.5f;
    Light_Ambient[2] = 0.5f;
    Light_Ambient[3] = 1.0f;
    Light_Diffuse = calloc (4, sizeof (float));
    Light_Diffuse[0] = 1.2f;
    Light_Diffuse[1] = 1.2f;
    Light_Diffuse[2] = 1.2f;
    Light_Diffuse[3] = 1.0f;
    Light_Position = calloc (4, sizeof (float));
    Light_Position[0] = 0;
    Light_Position[1] = 0;
    Light_Position[2] = 0.0f;
    Light_Position[3] = 1.0f;
    gray = calloc (4, sizeof (float));
    gray[0] = 0.75f;
    gray[1] = 0.75f;
    gray[2] = 0.75f;
    gray[3] = 1.0f;
    specular = calloc (4, sizeof (float));
    specular[0] = 1.0f;
    specular[1] = 1.0f;
    specular[2] = 1.0f;
    specular[3] = 1.0f;                
    specref = calloc (4, sizeof (float));
    specref[0] = 1.0f;
    specref[1] = 1.0f;
    specref[2] = 1.0f;
    specref[3] = 1.0f;
    cenlight = 1;

    TexModesStr = calloc (4, sizeof (char *));
    for (i = 0; i < 4; i++) {
        TexModesStr[i] = calloc (20, sizeof (char));
    }
    strcpy (TexModesStr[0], "GL_DECAL");
    strcpy (TexModesStr[1], "GL_MODULATE");
    strcpy (TexModesStr[2], "GL_BLEND");
    strcpy (TexModesStr[3], "GL_REPLACE");
    TexModes = calloc (4, sizeof (GLint));
    TexModes[0] = GL_DECAL;
    TexModes[1] = GL_MODULATE;
    TexModes[2] = GL_BLEND;
    TexModes[3] = GL_REPLACE;
    /*
    vars used for rotation angles
*/
    angle_x = 0;
    angle_y = 180;
    angle_z = 0;
    /*
    vars used for rotation animation
*/
    spin_z = 0;
    spin_x = 0;
    spin_y = 0;
    /*
    line stippling 
*/
    factor = 1;
    pattern = 0x9999;
    /*
    show the line box bool
*/
    showlines = 1;
    // delay factor
    delay_current = initial_delay;
    // var used for counting when delaying screen updates
    delay_count = 0;
    // initial zoom increment is 1
    zoom_step = 1;
    // toggle for view angle presets
    display_toggle = 1;
    //var to state which direction the loaded data will be played in
    // starts at 1, for forward direction
    replay_direction = 1;
    // set up the state scaler
    scaler_orig =Window_Height/scaler;
    scaler = scaler_orig; // and store this for resets, since scaler will be often reset
    // event records found var initial value
    event_count = 0;

}
