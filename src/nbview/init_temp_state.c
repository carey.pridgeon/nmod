/********************************************************************
*
* Function                       : init_temp_state
* Date of first writing  : 2007
* Author                          : Carey Pridgeon
* EMail                            : carey.pridgeon@gmail.com
* Description:  Function to read in the data of a single state.
*                       Perameters:
*                                           1: the file stream currently being parsed
*                                           2: the particle array entry to be filled
* Licence: GPL 3.0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#include "header.h"
single_state * init_temp_state (int particle_num) {
    single_state *temp;
    temp = malloc (sizeof (single_state));
    temp->num_particles = particle_num;
    temp->particles = calloc (temp->num_particles, sizeof (particle_data));
    return temp;
}
