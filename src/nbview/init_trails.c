/********************************************************************
*
* Function                       : init_trails
* Date of first writing  : 2006
* Author                          : Carey Pridgeon
* EMail                            : carey.pridgeon@gmail.com
* Description:  function prepare the trails array
* Licence: GPL 3.0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#include "header.h"
trail_set * init_trails (int p_num, int t_size) {
    int i;
    trail_set *temp_set;
    temp_set = malloc (sizeof (trail_set));
    temp_set->trail = calloc (p_num, sizeof (single_trail));
    temp_set->trail_size = t_size;
    temp_set->total_particles = p_num;
    for (i = 0; i < p_num; i++) {
        temp_set->trail[i].xloc = calloc (t_size, sizeof (float));
        temp_set->trail[i].yloc = calloc (t_size, sizeof (float));
        temp_set->trail[i].zloc = calloc (t_size, sizeof (float));
    }
    return temp_set;
}
