/********************************************************************
*
* Function  : mouseclickHandler
* Date of first writing  :  2006
* Author : Carey Pridgeon
* EMail  : carey.pridgeon@gmail.com
* Description:  Callback function called when mouse buttons clicked. I haven't really got anywhere with this, it's unfinished and unused right now.
* Licence: GPL 3 .0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#include "header.h"

void mouseclickHandler (int button, int state, int x, int y) {
    //int i;
    //float tmpx,tmpy,tmpxbest,tmpybest,tmp,tmpbest,winner;
    if (state == GLUT_DOWN) {
        // left button
        if (button == GLUT_LEFT_BUTTON) {
            /*
        // this button handles selection of particles
        // to reveal their info (ignores z axis, obviously)
        // again with the using max_particles.
        // This is a bad idea long term, because
        // the number of particles may change
        x -= Window_Width/2;
        y -= Window_Width/2;
        winner = 0;
        tmpxbest = x - current_state->particles[0].xloc;
        tmpybest = y - current_state->particles[0].yloc;
        tmpbest =  tmpxbest -  tmpybest;
        for (i=0;i<max_particles;i++) {
        tmpx = x - current_state->particles[i].xloc;
        tmpy = y - current_state->particles[i].yloc;
        tmp =  tmpx -  tmpy;
        printf("> x %d\n",x);
        printf("> y %d\n",y); 
        printf("> xloc %f\n",current_state->particles[i].xloc); 
        printf("> yloc %f\n",current_state->particles[i].yloc); 
        printf("> tmp %f\n",tmp); 
        if((tmp<tmpbest)) {
        winner = i;
        tmpbest = tmp;
        }
        }
        selected_particle = winner; 
        printf("> %d\n\n\n",selected_particle);  
    */
        }
        // middle button
        else if (button == GLUT_MIDDLE_BUTTON) {
        }
        // right button
        else {
            // this button handles selection of particles
            // to be the focus body
        }
    }
}
