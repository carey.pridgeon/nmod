
/********************************************************************
*
* Function  : mousemotionHandler
* Date of first writing :  2006
* Author : Carey Pridgeon
* EMail  : carey.pridgeon@gmail.com
* Description:  Callback function called when mouse moves, blank for now
* Licence: GPL 3 .0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#include "header.h"

void mousemotionHandler (int x, int y) {
    if (x < 0) {
    }
    else if (x >= 0) {
    }
    else {
    }
    if (y < 0) {
    }
    else if (y >= 0) {
    }
    else {
    }
}
