
/********************************************************************
*
* Function  :  main.c
* Date of first writing  :  2006
* Author : Carey Pridgeon
* EMail  : carey.pridgeon@gmail.com
* Description:  The main function into nBview. 
                        Configs are parsed, data are loaded, and opengl context are contexted in a very opengly kind of way.
* Licence: GPL 3 .0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#include "header.h"
int main (int argc, char **argv) {
    /*
    working on the assumption that the required filename has 
    * indeed been supplied by the config file, read it in 
*/
    system ("cls");
    printf ("##################################################\n");
    printf ("# nBview - the nMod nBody Toolkit OpenGl Viewer  #\n");
    printf ("# Version 3.1 Reference Build                    #\n");
    printf ("# Carey Pridgeon 2006                            #\n");
    printf ("# Email: carey.pridgeon@gmail.com                #\n");
    printf ("##################################################\n");
    if (argc == 1) {
        printf(">\n> Incorrect number of perameters, you need to specify an nbo file. Feel free to try again...\n");
        exit (0);

    }
    if (argc == 2) {

        // open the config file
        //and then open the nbo file specified at the command line
        chdir ("cfg");
        printf ("##################################################\n");
        readconfig ("nb_conf.cfg");
        chdir ("..");
        loaded_state = read_statefile (argv[1]);
    }

    // hack to obtain the number of particles
    // for the specialkeypress handling function
    max_particles = loaded_state->set[0].num_particles;
    system ("cls");
    printf ("##################################################\n");
    printf ("# nBview - the nMod nBody Toolkit OpenGl Viewer  #\n");
    printf ("# Version 3.1 Reference Build                    #\n");
    printf ("# Carey Pridgeon 2006                            #\n");
    printf ("# Email: carey.pridgeon@gmail.com                #\n");
    printf ("##################################################\n");
    printf (">\n> Finished parsing %s, beginning display mode\n",particle_filename);

    init_globals ();
    // get the largest value to be found in the first state
    max_distance_from_origin = find_max_distance_from_origin (loaded_state);
    


    //set up trails array, regardless of whether it is to be used, because it
    //can be toggled on later.
    //Might change this later so if it's turned off in the config file it can't ever be used, dunno, depends on memory usage.

    trail_record = init_trails (max_particles, trail_size);
    // init various other globals
    glutInit (&argc, argv);
    // To see OpenGL drawing, take out the GLUT_DOUBLE request.
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowPosition (100, 100);
    glutInitWindowSize (Window_Width, Window_Height);
    // Open a window, after setting up the windows title
    sprintf (title_and_fname, "%s - %s", PROGRAM_TITLE, particle_filename);
    Window_ID = glutCreateWindow (title_and_fname);
    // Register the callback function to do the drawing. 
    glutDisplayFunc (&RenderDisplay);
    //glutFullScreen();
    // If there's nothing to do, draw.
    glutIdleFunc (&RenderDisplay);
    // It's a good idea to know when our window's resized.
    glutReshapeFunc (&Resize);
    // And let's get some keyboard input.
    glutKeyboardFunc (&KeyPressHandler);
    glutSpecialFunc (&SpecialKeyPressed);
    //glutMouseFunc(&mouseclickHandler);
    init_GL (Window_Width, Window_Height);
    // Pass off control to OpenGL.
    // Above functions are called as appropriate.
    glutMainLoop ();
    printf ("> Finished\n");
    return 1;
}
