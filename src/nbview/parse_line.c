/********************************************************************
*
* Function  : parse_line
* Date of first writing  :  2003
* Author : Carey Pridgeon
* EMail  : carey.pridgeon@gmail.com
* Description:  This function  tokenises a single line of characters  and stores those tokens in a 2d array of char.
                        Currently the line being parsed can only have a strict form:
                                                                                                                            a line must not start with a space.
                                                                                                                            only one space is allowed between words
                            
                                                                                                                            
* Licence: GPL 3 .0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/

#include "header.h"
void parse_line (char *input, char *word1, char *word2, char *word3) {
    int a, pos;
    a = pos = 0;
    /*
    read the line till all words (max 3) have been read 
*/
    while (1) {
        while ((input[pos] != '\0') && (input[pos] != ' ')
        && (input[pos] != '\n')) {
            word1[a] = input[pos];
            a++;
            pos++;
        }
        if (input[pos] == '\0') {
            break;
        }
        a = 0;
        pos++;
        while ((input[pos] != '\0') && (input[pos] != ' ')
        && (input[pos] != '\n')) {
            word2[a] = input[pos];
            a++;
            pos++;
        }
        if (input[pos] == '\0') {
            break;
        }
        a = 0;
        pos++;
        while ((input[pos] != '\0') && (input[pos] != ' ')
        && (input[pos] != '\n')) {
            word3[a] = input[pos];
            a++;
            pos++;
        }
        if (input[pos] == '\0') {
            break;
        }
    }
}
