
/********************************************************************
*
* Function  : read_statefile
* Date of first writing  :  2007
* Author : Carey Pridgeon
* EMail  : carey.pridgeon@gmail.com
* Description:  Function to read in the stat data for an entire time series.
                        Very rough and non adaptive, stuffed full of magic numbers 
            and, well, its shit. A better version will be produced when 
                            time permits.			 
* Licence: GPL 3 .0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/

#include "header.h"

states * read_statefile (char *infile_name) {
    int i, j, k, l, m, pcount, ppos;
    char  inchar;
    char preamble_str[1024], preamble_val_array[4][1024],
    single_state_preamble[2][1024], particle_val_array[14][1024];
    states *temp_states;
    int delim_count, char_count, event_counter;
    char particle_str[1024];
    event_counter = 0;
    // open the input file (like you could do anything else here, peh....)
    inputfile = fopen (infile_name, "r");
    if (inputfile == NULL) {
        fprintf (stderr, "> Failed to open %s\n", infile_name);
        exit (0);
    }
    // retreive and parse the preamble
    delim_count = char_count = 0;
    // fetch the preamble
    for (i = 0; i < 1024; i++) {
        inchar = consume_char (inputfile);
        char_count++;
        if (inchar == '#') {
            delim_count++;
        }
        preamble_str[i] = inchar;
        if (delim_count == 2) {
            break;
        }				// done with preamble
    }
    // now process the preamble string
    // preamble is a fixed format right now, so extraction is simple
    pcount = ppos = 0;
    for (i = 0; i < char_count; i++) {
        if (preamble_str[i] == '#') {
            preamble_str[i] = ' ';
        };				// replace the '#' with whitespace
    }
    for (i = 0; i < char_count; i++) {
        if (preamble_str[i] == ':') {
            pcount++;
            ppos = 0;
        }
        else {
            preamble_val_array[pcount][ppos] = preamble_str[i];
            ppos++;
        }
    }

    // now a simple convert and copy process to get the preamble into the state header
    temp_states = malloc (sizeof (states));
    temp_states->total_states = atoi (preamble_val_array[0]);
    temp_states->num_particles = atoi (preamble_val_array[1]);
    temp_states->state_interval = atoi (preamble_val_array[2]);
    temp_states->particle_datasize = atoi (preamble_val_array[3]);
    temp_states->set =
    calloc (temp_states->total_states, sizeof (single_state));
    // now we have the preamble, and the state dataset has been readied, start the process of reading in the states
    for (i = 0; i < temp_states->total_states; i++) {
        printf ("> Now loading State %d of %d\n", i, temp_states->total_states);
        for (k = 0; k < 1024; k++) {
            single_state_preamble[0][k] = ' ';
            single_state_preamble[1][k] = ' ';
        }
        // read the minor preamble of this indidual state
        delim_count = char_count = pcount = ppos = 0;
        while (1) {
            inchar = consume_char (inputfile);
            if (inchar == '!') {
                delim_count++;
                pcount++;
                ppos = 0;
            }
            if (delim_count == 2) {
                break;
            }				// assuming here the format is unchanged
            if ((inchar != '!') && (inchar != '[') && (inchar != 's')) {
                single_state_preamble[pcount][ppos] = inchar;
                ppos++;
            }
        }
        temp_states->set[i].state_number = atoi (single_state_preamble[0]);
        temp_states->set[i].num_particles = atoi (single_state_preamble[1]);
        // now read each of the particle blocks within the states.
        // note that for now the maximum allowed size for a single particles data is 1024 chars, go over that and this already 
        // shaky code will die a horrible death..)
        temp_states->set[i].particles =
        calloc (temp_states->set[i].num_particles, sizeof (particle_data));
        for (j = 0; j < temp_states->set[i].num_particles; j++) {
            delim_count = ppos = 0;
            for (k = 0; k < 1024; k++) {
                particle_str[k] = '\0';
            }
            while (1) {
                inchar = consume_char (inputfile);
                if (inchar == ')') {
                    break;
                }
                if ((inchar == '(') || (inchar == '|') || (inchar == ',')) {
                    particle_str[ppos] = ' ';
                    ppos++;
                }
                else {
                    particle_str[ppos] = inchar;
                    ppos++;
                }
            }
            // now split the string out to fill the particle data structure
            // oh gods, the hAXXing, make it stop!!!!!111one
            for (l = 0; l < temp_states->particle_datasize; l++) {
                for (m = 0; m < 1024; m++) {
                    particle_val_array[l][m] = '\0';
                }
            }
            delim_count = char_count = pcount = ppos = 0;
            for (l = 1; l < 1024; l++) {

                // we ignore the first char for now, it's a space anyway, so who cares about it, loser...
                if (particle_str[l] == ' ') {
                    pcount++;
                    ppos = 0;
                    if (pcount > temp_states->particle_datasize) {
                        break;
                    }
                }
                else {
                    particle_val_array[pcount][ppos] = particle_str[l];
                    ppos++;
                }
            }
            for (l = 0; l < 1024; l++) {
                if (particle_str[l] == ' ') {
                    pcount++;
                    ppos = 0;
                    if (pcount > temp_states->particle_datasize) {
                        break;
                    }
                }
                else {
                    particle_val_array[pcount][ppos] = particle_str[l];
                    ppos++;
                }
            }
            // values extracted and stored as strings, so time for the +4 boots of type convertings
            strcpy (temp_states->set[i].particles[j].particle_id, particle_val_array[0]);
            
            if ((strcmp ("ma3", particle_val_array[1]) == 0)) {
                temp_states->set[i].particles[j].type = MASSIVE_PARTICLE_3;
            }
            //#######
            // code to cope with older nbo files that still use sm for stars
            if ((strcmp ("sm", particle_val_array[1]) == 0)) {
                temp_states->set[i].particles[j].type = MASSIVE_PARTICLE_3;
            }
            // #######
            if ((strcmp ("ma2", particle_val_array[1]) == 0)) {
                temp_states->set[i].particles[j].type = MASSIVE_PARTICLE_2;
            }
            if ((strcmp ("ma1", particle_val_array[1]) == 0)) {
                temp_states->set[i].particles[j].type = MASSIVE_PARTICLE_1;
            }
            if ((strcmp ("mj2", particle_val_array[1]) == 0)) {
                temp_states->set[i].particles[j].type = MAJOR_PARTICLE_2;
            }
            if ((strcmp ("mj1", particle_val_array[1]) == 0)) {
                temp_states->set[i].particles[j].type = MAJOR_PARTICLE_1;
            }
            if ((strcmp ("mn2", particle_val_array[1]) == 0)) {
                temp_states->set[i].particles[j].type = MINOR_PARTICLE_2;
            }
            if ((strcmp ("mn1", particle_val_array[1]) == 0)) {
                temp_states->set[i].particles[j].type = MINOR_PARTICLE_1;
            }
            
            temp_states->set[i].particles[j].mass = atof (particle_val_array[2]);
            temp_states->set[i].particles[j].radius = atof (particle_val_array[3]);

            temp_states->set[i].particles[j].rgb_vals.r = atoi (particle_val_array[4]);
            temp_states->set[i].particles[j].rgb_vals.g = atoi (particle_val_array[5]);
            temp_states->set[i].particles[j].rgb_vals.b = atoi (particle_val_array[6]);
            strcpy (temp_states->set[i].particles[j].freetext,particle_val_array[7]);
            if (strcmp(temp_states->set[i].particles[j].freetext,event_word) ==0) {
                event_counter++;
            }
            temp_states->set[i].particles[j].xloc = atof (particle_val_array[8]);
            temp_states->set[i].particles[j].yloc = atof (particle_val_array[9]);
            temp_states->set[i].particles[j].zloc = atof (particle_val_array[10]);
            temp_states->set[i].particles[j].xsp = atof (particle_val_array[11]);
            temp_states->set[i].particles[j].ysp = atof (particle_val_array[12]);
            temp_states->set[i].particles[j].zsp = atof (particle_val_array[13]);
        }
        // grab end of state ']'
        inchar = consume_char (inputfile);
    }
    //calloc the state used for storage during display here
    current_state = init_temp_state (temp_states->num_particles);
    // allocate memory for the events list
    size_of_events_array = event_counter;
    event_counter = 0; // we need to re-use this now
    events_record =calloc(size_of_events_array,sizeof(single_event));
    // now populate the event array. Just with the state in which a burn occurs though, not the position
    for (i=0;i<temp_states->total_states;i++) {
        for (j=0;j<temp_states->num_particles;j++) {
            if (strcmp(temp_states->set[i].particles[j].freetext,event_word) ==0) {
                events_record[event_counter].state_occuring = i; 
                // no position information for this burn is available yet, so say this
                events_record[event_counter].has_position_data = FALSE;
                event_counter++;
            }
        }  
    } 
    // quick check to make sure important vars aren't out of bounds
    var_sanity_check (temp_states);
    return temp_states;
}
