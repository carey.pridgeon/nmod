
/********************************************************************
*
* Function  : readconfig
* Date of first writing :  2006
* Author : Carey Pridgeon
* EMail  : carey.pridgeon@gmail.com
* Description:  Well, I know you'll be amazed, but this function read the config file for nbview.
* Licence: GPL 3 .0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#include "header.h"
void readconfig (char *filename) {
    FILE *inputfile;
    char *inputline;
    int i;
    inputline = (char *) calloc (100, (sizeof (char)));
    if (inputline == NULL) {
        printf ("malloc faliure in readconfig\n");
        exit (0);
    }
    inputfile = fopen (filename, "r");
    if (inputfile == NULL) {
        fprintf (stderr,"> +++ FTB error +++ replace fluffy teddy bear to continue +++\n");
        fprintf (stderr,"> (in other words, the config file either can't be found or can't be opened)\n");
        exit (0);
    }
    i=0;
    while ((fgets (inputline, 100, inputfile)) != NULL) {
            get_value (inputline);
    }
    if ((fclose (inputfile)) != 0) {
        printf ("> config file %s not closed!\n", filename);
    }
}
