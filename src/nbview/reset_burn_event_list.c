/********************************************************************
*
* Function  : update_trails
* Date of first writing : 01/2007
* Author : Carey Pridgeon
* EMail  : carey.pridgeon@gmail.com
* Description:  This function  shifts the trails down the arrays
            and adds the latest trails value to the head of the arrays.
* Licence:     GPL 3 .0 or later.
*  This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*  any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#include "header.h"
void reset_burn_event_list (single_event *local_events_record,int num_burns) {
    int i;
    for (i = 0; i < num_burns; i++) {
        local_events_record[i].xloc = 0;
        local_events_record[i].yloc = 0;
        local_events_record[i].zloc = 0;
        local_events_record[i].has_position_data = FALSE;
    }
}

