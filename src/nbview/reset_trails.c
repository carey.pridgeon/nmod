/********************************************************************
*
* Function  : reset_trails
* Date of first writing  :  2006
* Author : Carey Pridgeon
* EMail  : carey.pridgeon@gmail.com
* Description:  Function that clears the trails arrays. Used when zooming in or out.
* Licence: GPL 3 .0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#include "header.h"
// ------
// function that clears the trails arrays, 
// used when zooming in or out.
void reset_trails (trail_set * local_trail) {
    int i, j;
    for (i = 0; i < local_trail->total_particles; i++) {
        for (j = 0; j < local_trail->trail_size; j++) {
            local_trail->trail[i].xloc[j] = 0.0;
            local_trail->trail[i].yloc[j] = 0.0;
            local_trail->trail[i].zloc[j] = 0.0;
        }
    }
}
