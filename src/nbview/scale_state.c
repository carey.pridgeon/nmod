/********************************************************************
*
* Function  :  Scale_state
* Date of first writing  :  2007
* Author : Carey Pridgeon
* EMail  : carey.pridgeon@gmail.com
* Description:  This function takes a single state and scales the location variables of the particles so all particles in the 
*                       state will fit in the viewing window.  
*                       This does not mean the particles will not leave the viewable area when the nbo file is played, just that 
*                       they start scaled so you can see them all to begin with.
*                       Scaling is by window height
* Licence: GPL 3 .0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/


/*		  Released under GNU GENERAL PUBLIC LICENSE
Version 2, June 1991 -  or later
*/
/* function to scale a state, returning the result to a temporary state, to be used for display
* scaling is by window height* the scaler  supplied in the config file
*/
#include "header.h"
int scale_state (single_state * local_state, single_state * temp_state, float wp,double md) {
    float m1, m2, m3;
    int j;
    // copy the number of particles over
    temp_state->num_particles = local_state->num_particles;
    // copy over scaled versions of each location entry
    for (j = 0; j < temp_state->num_particles; j++) {
        // discover what percentage of the largest first frame value these 
        // current values are
        m1 = (sqrt (sqr (local_state->particles[j].xloc)) / md);
        m2 = (sqrt (sqr (local_state->particles[j].yloc)) / md);
        m3 = (sqrt (sqr (local_state->particles[j].zloc)) / md);
        if (local_state->particles[j].xloc <= 0) {
            temp_state->particles[j].xloc = (m1 * (-wp));
        }
        else {
            temp_state->particles[j].xloc = m1 * wp;
        }
        if (local_state->particles[j].yloc <= 0) {
            temp_state->particles[j].yloc = (m2 * (-wp));
        }
        else {
            temp_state->particles[j].yloc = m2 * wp;
        }
        if (local_state->particles[j].zloc <= 0) {
            temp_state->particles[j].zloc = (m3 * (-wp));
        }
        else {
            temp_state->particles[j].zloc = m3 * wp;
        }
    }
    return 0;
}
