#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include "screen_capture.h"
int write_tga(char *filename, int x, int y, int comp, void *data) {
    int has_alpha = !(comp & 1);
    return outfile(filename, -1,-1, x, y, comp, data,0, 0,
    "111 221 2222 11", 0,0,2, 0,0,0, 0,0,x,y, 24+8*has_alpha, 8*has_alpha);
}

int write_bmp(char *filename, int x, int y, int comp, void *data)
{
    int pad = (-x*3) & 3;
    return outfile(filename,-1,-1,x,y,comp,data,0,pad,
    "11 4 22 4" "4 44 22 444444",
    'B', 'M', 14+40+(x*3+pad)*y, 0,0, 14+40,  // file header
    40, x,y, 1,24, 0,0,0,0,0,0);             // bitmap header
}

int outfile(char const *filename, int rgb_dir, int vdir, int x, int y, int comp, void *data, int alpha, int pad, char *fmt, ...) {
    FILE *f = fopen(filename, "wb");
    if (f) {
        va_list v;
        va_start(v, fmt);
        writefv(f, fmt, v);
        va_end(v);
        write_pixels(f,rgb_dir,vdir,x,y,comp,data,alpha,pad);
        fclose(f);
    }
    return f != NULL;
}

void write_pixels(FILE *f, int rgb_dir, int vdir, int x, int y, int comp, void *data, int write_alpha, int scanline_pad) {
    unsigned char bg[3] = { 255, 0, 255}, px[3];
    unsigned int zero = 0;
    int i,j,k, j_end;

    if (vdir < 0)
    j_end = -1, j = y-1;
    else
    j_end =  y, j = 0;

    for (; j != j_end; j += vdir) {
        for (i=0; i < x; ++i) {
            unsigned char *d = (unsigned char *) data + (j*x+i)*comp;
            if (write_alpha < 0)
            fwrite(&d[comp-1], 1, 1, f);
            switch (comp) {
            case 1:
            case 2: writef(f, "111", d[0],d[0],d[0]);
                break;
            case 3:
                writef(f, "111", d[1-rgb_dir],d[1],d[1+rgb_dir]);
                break;
            case 4:
                if (!write_alpha) {
                    for (k=0; k < 3; ++k)
                    px[k] = bg[k] + ((d[k] - bg[k]) * d[3])/255;
                    writef(f, "111", px[1-rgb_dir],px[1],px[1+rgb_dir]);
                    break;
                }
            }
            if (write_alpha > 0)
            fwrite(&d[comp-1], 1, 1, f);
        }
        fwrite(&zero,scanline_pad,1,f);
    }
}


void write8(FILE *f, int x) { unsigned char z = (unsigned char) x; fwrite(&z,1,1,f);}


void writefv(FILE *f, char *fmt, va_list v) {
    while (*fmt) {
        switch (*fmt++) {
        case ' ': break;
        case '1': { unsigned char x = va_arg(v, int); write8(f,x); break; }
        case '2': { signed short x = va_arg(v, int); write8(f,x); write8(f,x>>8); break; }
        case '4': { signed int x = va_arg(v, int); write8(f,x); write8(f,x>>8); write8(f,x>>16); write8(f,x>>24); break; }
        default:
            assert(0);
            va_end(v);
            return;
        }
    }
}

void writef(FILE *f, char *fmt, ...) {
    va_list v;
    va_start(v, fmt);
    writefv(f,fmt,v);
    va_end(v);
}

int take_screenshot_tga(const char *filename,int x, int y,int width, int height) {
    unsigned char *pixel_data;
    int i, j;

    if( (width < 1) || (height < 1) )
    {
        fprintf(stderr,"Invalid screenshot dimensions");
        exit(0);
    }
    if( (x < 0) || (y < 0) )
    {
        fprintf(stderr,"Invalid screenshot location");
        exit(0);
    }
    if( filename == NULL )
    {
        fprintf(stderr,"Invalid screenshot filename");
        exit(0);
    }

    pixel_data = (unsigned char*)malloc( 3*width*height );
    glReadPixels (x, y, width, height, GL_RGB, GL_UNSIGNED_BYTE, pixel_data);

    for( j = 0; j*2 < height; ++j )
    {
        int index1 = j * width * 3;
        int index2 = (height - 1 - j) * width * 3;
        for( i = width * 3; i > 0; --i )
        {
            unsigned char temp = pixel_data[index1];
            pixel_data[index1] = pixel_data[index2];
            pixel_data[index2] = temp;
            ++index1;
            ++index2;
        }
    }

    write_tga((char*)filename,width,height,3,pixel_data);

    free( (void*)pixel_data );
    return 0;
}

int take_screenshot_bmp(const char *filename,int x, int y,int width, int height) {
    unsigned char *pixel_data;
    int i, j;

    if( (width < 1) || (height < 1) )
    {
        fprintf(stderr,"Invalid screenshot dimensions");
        exit(0);
    }
    if( (x < 0) || (y < 0) )
    {
        fprintf(stderr,"Invalid screenshot location");
        exit(0);
    }
    if( filename == NULL )
    {
        fprintf(stderr,"Invalid screenshot filename");
        exit(0);
    }

    pixel_data = (unsigned char*)malloc( 3*width*height );
    glReadPixels (x, y, width, height, GL_RGB, GL_UNSIGNED_BYTE, pixel_data);

    for( j = 0; j*2 < height; ++j )
    {
        int index1 = j * width * 3;
        int index2 = (height - 1 - j) * width * 3;
        for( i = width * 3; i > 0; --i )
        {
            unsigned char temp = pixel_data[index1];
            pixel_data[index1] = pixel_data[index2];
            pixel_data[index2] = temp;
            ++index1;
            ++index2;
        }
    }

    write_bmp((char*)filename,width,height,3,pixel_data);

    free( (void*)pixel_data );
    return 0;
}

