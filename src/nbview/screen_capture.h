#ifndef screen_captureH
#define screen_captureH
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#include <GL/glu.h>
#include <GL/glut.h>
int write_tga(char *filename, int x, int y, int comp, void *data);

int write_bmp(char *filename, int x, int y, int comp, void *data);

int outfile(char const *filename, int rgb_dir, int vdir, int x, int y, int comp, void *data, int alpha, int pad, char *fmt, ...);

void write_pixels(FILE *f, int rgb_dir, int vdir, int x, int y, int comp, void *data, int write_alpha, int scanline_pad);

void write8(FILE *f, int x);

void writefv(FILE *f, char *fmt, va_list v);

void writef(FILE *f, char *fmt, ...);

int take_screenshot_tga(const char *filename,int x, int y,int width, int height);
int take_screenshot_bmp(const char *filename,int x, int y,int width, int height);
#endif

