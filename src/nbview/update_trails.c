/********************************************************************
*
* Function  : update_trails
* Date of first writing : 01/2007
* Author : Carey Pridgeon
* EMail  : carey.pridgeon@gmail.com
* Description:  This function  shifts the trails down the arrays
            and adds the latest trails value to the head of the arrays.
* Licence:     GPL 3 .0 or later.
*  This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*  any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/
#include "header.h"
void update_trails (trail_set * local_trail, single_state * local_state, int local_focus_particle) {
    int i, j, k;
    for (i = 0; i < local_trail->total_particles; i++) {
        k = local_trail->trail_size - 1;
        for (j = 0; j < local_trail->trail_size; j++) {
            local_trail->trail[i].xloc[k] = local_trail->trail[i].xloc[k - 1];
            local_trail->trail[i].yloc[k] = local_trail->trail[i].yloc[k - 1];
            local_trail->trail[i].zloc[k] = local_trail->trail[i].zloc[k - 1];
            k--;
        }
    }
    for (i = 0; i < local_trail->total_particles; i++) {
        if (i != local_focus_particle) {

            // we don't want to be subtracting the focus particle from itself now, do we
            local_trail->trail[i].xloc[0] = current_state->particles[i].xloc -
            current_state->particles[local_focus_particle].xloc;
            local_trail->trail[i].yloc[0] = current_state->particles[i].yloc -
            current_state->particles[local_focus_particle].yloc;
            local_trail->trail[i].zloc[0] = current_state->particles[i].zloc -
            current_state->particles[local_focus_particle].zloc;
        }
    }
}
