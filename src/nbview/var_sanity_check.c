/********************************************************************
*
* Function  : var_sanity_check
* Date of first writing  :  2007
* Author : Carey Pridgeon
* EMail  : carey.pridgeon@gmail.com
* Description:  Function that checks we don't have variables out of range for the current nbo file contents
* examples might be selected particle being initially set to greater then the number of particles.
* Licence: GPL 3 .0 or later.
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.

*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.
********************************************************************/

#include "header.h"

void var_sanity_check (states *local_state) {
    //  a simple collection of if statements. Add more if a new variable requires a test

    if ((focus_particle >= local_state->num_particles)||(focus_particle < 0)) {
        focus_particle = 0;

    }
    if ((selected_particle >= local_state->num_particles)||(selected_particle < 0)) {
        selected_particle = 0;
    }

}
