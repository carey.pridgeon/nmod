![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# nMod-ea #

---


[download](http://nmod.googlecode.com/files/nMod-ea_pack_version_1.i.zip)

Current version 1.j


---


nMod-ea is an extension of the nMod Toolkit that enables the use of evolutionary algorithms to investigate spacecraft flight plan creation and optimisation.

![http://nmod.googlecode.com/svn/trunk/images/burn_events.png](http://nmod.googlecode.com/svn/trunk/images/burn_events.png)

The nMod-ea pack includes Windows and Linux source code for example steady state EA and stochastic hillclimber versions of nMod-ea, plus nBview (see the nMod Toolkit user manual for detail on this visualisation tool). Additionally there are sample project (nmp) files which contain demonstration Near Earth Asteroid interception experiments.


Here is an example nbo file showing the result of a basic experiment to get a spacecraft to leave Low Earth Orbit and fly past the asteroids Apophis and Eros.

[Download example nbo file](http://nmod.googlecode.com/files/nmod-ea_example_result_1.zip)

Its not an optimal result, but suffices to demonstrate the spacecraft particle in action.

I am able to offer some advice to researchers, students and educators if you wish to use nMod-ea.


---


## Documentation ##

This documentation is minimal, as most guidance on using the code was provided in tutorial sessions, and it will remain so, as I am no longer developing this branch. Should you wish to use this version of nMod-ea, contact me, as I am quite happy to provide guidance.

[nMod-ea Documentation](http://code.google.com/p/nmod/wiki/documentation1) .



---


**Links**

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)



---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com