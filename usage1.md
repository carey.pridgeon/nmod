![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# Using the nMod Toolkit: Part 1 #
**_Using the nMod Toolkit n-Body model under Linux_**

---


The Linux release of the nMod Toolkit includes both compiled binaries for all tools and the source code for each tool. If you wish to compile your own binaries, instructions can be found [here](http://code.google.com/p/nmod/wiki/compile1).


---


The n-Body model in the nMod toolkit is called **nmod**. You need to open a console and navigate to the folder that contains this binary before continuing.

nMod comes with a project file, this is a file with the extension _nmp_, which we covered in an [earlier tutorial](http://code.google.com/p/nmod/wiki/internals2). You  definitely need to understand nmp files before using nMod, so I strongly recommend that you read the nmp file explanation before moving on.

## Running nMod ##

at the terminal, type:

_**./nmod projectfile.nmp**_

(where projectfile.nmp is any valid nmp filename).

To use the included example nmp file, type:

**_./nmod reference\_project.nmp_**

If you don't want to keep the terminal open, you can use nohup or screen.

If you use nohup, change 'use stdout' in the nmp file to FALSE, because there's no point in wasting clock cycles writing to stdout if its going to get piped to nohup.out.

Then type:

**_nohup ./nmod projectfile.nmp &_**

Screen is well documented elsewhere, I suggest you consult the man page for it.

## Waiting for nMod to finish generating the nbo file ##

Running even a small project using nmod is going to take a while. n-Body modelling isn't a quick business. I've made the n-Body model as fast as I can, but there's a lot of number crunching going on, and that takes time.

If you're wanting to model globular clusters, you could be looking at a run time of several days to a week, although that's an extreme example. It's not unusual for the n-Body model to take a few hours to complete, dependant on how many particles there are, how small the step size is, and how long you want the time series to be.

As a result, it's probably not a good plan to run the n-Body model on a machine you need to use frequently, unless that machine has two cores, in which case the n-Body model will sit on one core, leaving you free to do other things without having to deal with a sluggish machine.

Sometimes it can be a good idea to run nMod overnight, or when you know you don't need to use your computer for a few hours.

At the moment nMod can't be paused and resumed.

### What you get after running nMod ###

Once nMod has completed, which might take a while, you will find two files, a new [nmp](http://code.google.com/p/nmod/wiki/internals2) file based on the final state of the model (if you requested one in the nmp file), and an [nbo](http://code.google.com/p/nmod/wiki/internals3) file which contains a time series recording of the model you just ran.

You will find these files in a sub folder 'result'.

Once you have the nbo file, it's time to use nBview to have a look at it, so we will cover that now.

[Using nBview](http://code.google.com/p/nmod/wiki/usage3)


---


**Links**

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com