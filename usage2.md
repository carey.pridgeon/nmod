![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# Using the nMod Toolkit: Part 2 #
**_Using the nMod Toolkit n-Body model under Windows_**

---


I am assuming that at this point you have either installed or [compiled](http://code.google.com/p/nmod/wiki/compile2)  the latest version of the  toolkit. For most users, using the installer is probably easiest, since that also provides you with the source code. Again you should make sure you have the latest version.

The n-Body model in the nMod toolkit is called **nmod**. You need to open a command line window and navigate to the folder that contains this binary before continuing. If you used the installer, you already have a shortcut in your start menu to open a console window for you.

nMod comes with a default project file, with the extension nmp, which we covered in an [earlier tutorial](http://code.google.com/p/nmod/wiki/internals2). Before we go any further, you need to learn about these. You  definitely need to understand nmp files before using nMod, so I strongly recommend that you read the nmp file explanation before moving on.


## Running nMod ##

at the command line, type:

_**nmod projectfile.nmp**_

(where projectfile.nmp is any valid nmp filename)

To use the included example nmp file, type:

**_nmod reference\_project.nmp_**

Windows has no equivalent to nohup or screen that I'm aware of, so you'll have to keep the console open.


## Waiting for nMod to finish generating the nbo file ##

Running even a small project using nmod is going to take a while. n-Body modelling isn't a quick business. I've made the n-Body model as fast as I can, but there's a lot of number crunching going on, and that takes time.

If you're wanting to model globular clusters, you could be looking at a run time of several days to a week, although that's an extreme example. It's not unusual for the n-Body model to take a few hours to complete, dependant on how many particles there are, how small the step size is, and how long you want the time series to be.

As a result, it's probably not a good plan to run the n-Body model on a machine you need to use frequently, unless that machine has two cores, in which case the n-Body model will sit on one core, leaving you free to do other things without having to deal with a sluggish machine.

Sometimes it can be a good idea to run nMod overnight, or when you know you don't need to use your computer for a few hours.

At the moment nMod can't be paused and resumed. That functionality will appear, because it would be handy, but not until the next major release.


### What you get after running nMod ###

Once nMod has completed, which might take a while, you will find two files, a new [nmp](http://code.google.com/p/nmod/wiki/internals2) file based on the final state of the model (if you requested one in the nmp file), and an [nbo](http://code.google.com/p/nmod/wiki/internals3) file which contains a time series recording of the model you just ran, in a sub folder of the main nmod toolkit folder, called 'result/'.

Once you have the nbo file, it's time to use nBview to have a look at it, so we will cover that now.

[Using nBview](http://code.google.com/p/nmod/wiki/usage4)


---


**Links**

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com