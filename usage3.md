![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# Using the nMod Toolkit: Part 4 #

**_Using the nMod Toolkit nbo viewer nBview under Linux_**


---

nBview is an OpenGL implementation of a basic viewer that loads and displays the nbo files the nMod n-Body model. It is meant to be used as a base for more complex viewer's and output analysers., but requires no additional coding to function as a viewer of nbo files.

The Linux version of nBview requires freeglut. To compile nBview in Linux requires freeglut-devel.

If you don't want to leave nmod running for a while generating a time series file before you follow this tutorial, you can download a completed [Solar System time series](http://nmod.googlecode.com/files/reference_project_2.7-pack.zip). It has 5,000 days of solar system motion, so it's a nice file to use for playing around with nBview. If you fancy something a bit more spectacular, you can download the
[globular cluster impact](http://nmod.googlecode.com/files/globuler_cluster_disruption_scenario_time_series_example-1.zip) scenario I used in the basic n-Body modelling introduction. The included nbvconf file is no longer required.

---


The configuration file for nBview: **nb\_conf.cfg**, which can be found in the cfg/ sub-folder of the nMod Toolkit, is well documented, so I won't describe every setting in it, but there are a few things I need to cover.

While it contains setting required for OpenGL, you will find it also allows you to change the way nBview behaves when you load an nbo file into it. For instance, you can have it cycling through an nbo file forever, or tell it to run through once, then stop. You can also say whether it is to start paused, how much delay there should be between frames, the length of particle trails, and many other things. You can do much of this while nBview is running, as I will explain below, but it can be useful to have everything set up before nBview even loads.

I suggest you read through nv\_conf.cfg yourself, and try changing some of the settings. Many are simple '1' for on and '0' for off options. Make sure you make a backup of the file first!


---


Any nbo file you want to view with nBview either has to be in the same directory, or you need to specify the full path to the file when calling nBview, as follows:


**_./nbview time\_series.nbo_**

__./nbview /path\_to_/time\_series.nbo_

Its usually simpler to write batch files to allow for loading the right nbo file into nBview.


To start with nbview will be parsing the nbo file, you'll see something like this


![http://nmod.googlecode.com/svn/trunk/images/loading_screen.png](http://nmod.googlecode.com/svn/trunk/images/loading_screen.png)

You should then be rewarded with a loaded viewer displaying the first frame of your nbo file. If you used the provided default nmp file to generate the nbo file, or downloaded the premade nbo file you should see this:

![http://nmod.googlecode.com/svn/trunk/images/finished_loading.png](http://nmod.googlecode.com/svn/trunk/images/finished_loading.png)

The viewer will scale any particle set it is given so that the outermost particles can be seen on screen. In this case the Voyager 2 probe is the outermost particle.

nBview has a lot of command options, all of which are accessed via single key presses. Pressing ‘h’ with the viewer in focus will bring up a list of these keys, also listed at the bottom of this page.

![http://nmod.googlecode.com/svn/trunk/images/help_text.png](http://nmod.googlecode.com/svn/trunk/images/help_text.png)

Pressing 'h' again will dismiss the help text. Now press ‘7’. This moves you to a decent rate of zoom increment.

The press ‘i’ for a while until you can see the inner solar system clearly.

![http://nmod.googlecode.com/svn/trunk/images/zoom_out2.png](http://nmod.googlecode.com/svn/trunk/images/zoom_out2.png)

See that particle with the dongle sort of thing attached to it? That's the 'selected' particle. Pressing '3' and '4' moves through the set of particles, selecting the next of previous particle. If you have chosen to display the selected particle text you will see its velocity and position displayed (kind of pointless using the selected particle feature if you don't have this on..). Whatever particle is currently centred on the display is the 'focus' particle, for which you also get current information displayed, if you have chosen that option. Pressing '1' and '2' will move back and forth through the particles, making each the focus in turn.

Then press ‘p’ to start playback.

![http://nmod.googlecode.com/svn/trunk/images/playing_text.png](http://nmod.googlecode.com/svn/trunk/images/playing_text.png)

Now try using the cursor/pageup/down keys to move the display about a bit. You will notice that trails also rotate around with the particles. They only get reset if you zoom in or out, and they can be dismissed/brought back by pressing 'k'.

![http://nmod.googlecode.com/svn/trunk/images/zoom_angle.png](http://nmod.googlecode.com/svn/trunk/images/zoom_angle.png)

To re-centre the view without losing magnification, press ‘c’. If you don't want any text cluttering things up, press 't' to get rid of it all, or refer to the help list to get rid of particular blocks of text.

nBview has no means of performing analysis of the particles it displays. This was intentionally left out as this visualiser is primarily intended to be used as a base for other more complete display and analysis programs.


---

### recording images during playback ###

nBview now has the capability to record either single, or successive screen shots during playback.

To take a single screenshot, press 'F2' at any time. A screen shot of the currently displayed state will be recorded in the images sub folder.

To record a sequence of states, as one might do to create an animation of a time series, press 'F1' to start recording, and 'F1' again to stop. Successively numbered bmp files will be saved to the images sub folder.

All images are recorded as bmp files, each 1.4 Mb, so you will need to convert them down to something more manageable, like png files, to create an animation. I suggest [Bulk Image Converter](http://www.softpedia.com/get/Multimedia/Graphic/Image-Convertors/Bulk-Image-Converter.shtml), as it is available under the GPL and works extremely well.

Note that the screen capture code included can write tga files as well, but bulk image converter does not convert those.



---


## Keypress options guide ##


  * Take a screenshot: 'F2'

  * Record successive states to bmp files during playback (toggle): 'F1'

  * Pause Model: 'p'

  * Show/Hide This Key Guide: 'h'

  * Show/Hide All Text: 't'

  * Speed Controls: slower '['  faster: ']'

  * Camara Distance: closer: 'u' farther: 'i'

  * Camara Distance Step   1x:     '5'

  * Camara Distance Step   10x:   '6'

  * Camara Distance Step  100x:  '7'

  * Camara Distance Step 1000x: '8'

  * Rotate X and Y Axis: Cursor Keys

  * Rotate Z Axis: Page Up/Page Down

  * Toggle Axis Lines: 'l'

  * Reset Display to Default View: 'r'

  * Reset Display to Default View -  Angles only: c

  * Shift Focus Particle :      '1' - '2'

  * Shift Selected Particle : '3' - '4'

  * Toggle Selected particle Indicator: 'w'

  * Toggle Cyclic Display: 'x'

  * Toggle XY YZ View: 'a'

  * Toggle nbo file playback direction: '#'

  * Toggle Show Focus Body Data: 'f'

  * Toggle Show Selected Body Data: 's'

  * Toggle Particle Trails: 'k'

  * Swap Selected/Focus Particles with each other:  '0'

  * Toggle Cyclic Display: 'x'

  * Toggle XY YZ View: 'a'

  * Toggle Show Focus Body Data: 'f'

  * Toggle Show Selected Body Data: 's'

  * Toggle Particle Trails: 'k'



---


**Links**

[Using the nMod Toolkit n-Body model under Windows](http://code.google.com/p/nmod/wiki/usage2)

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com