![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# Using the nMod Toolkit: Part 5 #
**_nbo2txt - making nbo files human readable_**

---


This utility will convert an nbo file into a human readable form.

nbo2txt writes the converted nbo file to stdout, so you'll need to pipe it to a file if you want to retain the converted file.

This tool can be useful in visually inspecting nbo files to discover if any invalid data has been recorded. This may in turn reveal a problem in a custom build of the n-Body model, or errors in the nmp file that haven't been spotted.

nbo2txt is a very simple utility. It would serve as an excellent base for writing your own tools to analyse nbo files.


---

## Usage ##

### Linux: ###

> ./nbo2txt yourfile.nbo

or

> ./nbo2txt yourfile.nbo | more

or

> ./nbo2txt yourfile.nbo | less

or

> ./nbo2txt yourfile.nbo > plaintextfile.txt


### Windows: ###

> nbo2txt yourfile.nbo

or

> nbo2txt yourfile.nbo > plaintextfile.txt



---


**Links**

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com