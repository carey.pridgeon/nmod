![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# Using the nMod Toolkit: Part 6 #
**_nbojoin -  combining existing nbo files_**

---


This utility will join two nbo files together.

You might want to do this when you need to demonstrate the progression of a series of experiments by having them play in the same time series.

The merged nbo file is written to stdout, so you will need to pipe it to a file if you want to keep it.

This utility does not alter the state numbering, so when it reaches the end of the first original file, and begins the next, it will go back to saying it is playing state 1, indicating the new file has started. I left this in because this makes it easier to show that the current state being displayed is from a particular point in the experiment.


---


## usage instructions ##

### Linux: ###

> ./nbojoin nbo\_file1.nbo nbo\_file2.nbo

### Windows: ###

> nbojoin nbo\_file1.nbo nbo\_file2.nbo



---


**Links**

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com