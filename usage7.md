![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# Using the nMod Toolkit: Part 7 #
**_nbofix, repairing broken nbo files_**

---


If an experiment gets unexpectedly interrupted, the nbo file will not open properly in nbview. This is because the number of states that nbview expects to find in an nbo file is set when the file is first created, before any states are written to it. If the specified and actual number of steps don't match, it will fail to play back.

This utility will create a new, valid NBO file from the damaged one.
The recovered nbo file is written to stdout, unless redirected to a file

## usage ##

> ### Linux ###

> ./nbofix nbo\_file.nbo > [repaired\_filename.nbo]

> ### Windows ###

> nbofix nbo\_file.nbo > [repaired\_filename.nbo]


---


**Links**

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com