![http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png](http://nmod.googlecode.com/svn/trunk/images/nmod_logo_small.png)


# Using the nMod Toolkit: Part 8 #
**_nbosplit, extracting states from nbo files_**

---


This utility takes an nbo file and extracts a portion of it, writing
the result as a new nbo file to stdout, unless redirected to a file

## Usage ##


### Linux: ###

> ./nbosplit 

<nbo\_file\_to\_split>

 start\_state end\_state

### Windows: ###

> nbosplit 

<nbo\_file\_to\_split>

 start\_state end\_state


Indexing starts at 1, not 0, so to start at the first state in the file, the start state should be specified as 1. To extract from the beginning to the tenth state of the nbo file  would be:

> nbosplit time\_series.nbo 1 10





---


**Links**

[User Manual](http://code.google.com/p/nmod/wiki/Introduction)


---

Dr Carey Pridgeon 2008 - carey.pridgeon@gmail.com